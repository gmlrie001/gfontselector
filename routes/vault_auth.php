<?php

/*
|--------------------------------------------------------------------------
| Vault Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Login Routes */
Route::get('vault/login', 'LoginController@index');
Route::post('vault/login', 'LoginController@login');

/* Logout Routes */
Route::get('vault/logout', 'LoginController@logout');
