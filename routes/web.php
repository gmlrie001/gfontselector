<?php

use App\Helpers\GoogleFonts\GoogleFontsHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

class Cart
{
    public $product_id, $product;


}


/* ============================================================================================ */

Route::get('/',function(){
    $this->data = [];
    // dd( $this );
    $this->data['site_settings'] = ( new \App\Models\Site )::all()->first();
    $this->data['page']          = ( new \App\Models\Page );
    $this->data['page']->title   = "Vault MT Header+Footer Templates";
    $this->data['banners']       = null;

    $this->data['cart_total']    = (int) 0;
    $this->data['cart_products'] = collect([]); // ( new \App\Models\OrderProduct );
    $this->data['cart_product']  = $this->data['cart_products'];
    /* ====================================================== */
    $quick_links[0] = (new \App\Models\QuickLink );
    $quick_links[0]->link = 'tel:27877401800';
    $quick_links[0]->title = 'NATIONAL HOTLINE';
    $quick_links[0]->description = '087 740 1800';
    
    $quick_links[1] = (new \App\Models\QuickLink );
    $quick_links[1]->link = '/store-locator';
    $quick_links[1]->title = 'STORE LOCATOR';
    $quick_links[1]->description = 'click here to find a store';
    
    $quick_links[2] = (new \App\Models\QuickLink );
    $quick_links[2]->link = '/decofurn-credit-application';
    $quick_links[2]->title = 'OPEN YOUR ACCOUNT TODAY';
    $quick_links[2]->description = 'apply for Decofurn Credit';

    $this->data['quick_links']   = $quick_links;
    /* ====================================================== */

    /* ====================================================== */
    if ( count( (new App\Models\GoogleFont)::all() ) === 0 ) {
        Redirect::to('/vault/site_fonts/update', 301);
    }
    /* ====================================================== */

    /* ====================================================== */
    Session::put('dont_show_cart', true);
    Session::put('user.id', 1);
    Session::put('user.name', 'Lance');
    Session::put('user.email', 'production@monzamedia.com');
    Session::forget('user');
    /* ====================================================== */

    // return view('welcome', $this->data);
    return view( 'includes.pages.header.decofurnsa_header', $this->data );
})->name('home');

/* ============================================================================================ */
/* Listing */
// 'vault/parse_google_fonts_file',
// Route::get(
//     '/site_fonts',
//     'Vault\GoogleFontController'
// )->name('fonts_listing');

/* ============================================================================================ */
/* Update */
// 'vault/parse_google_fonts_file/update',
Route::get(
    '/site_fonts/update',
    'Vault\GoogleFontController@update'
)->name('fonts_update');

/* ============================================================================================ */
/* Search */
// 'vault/parse_google_fonts_file/search',
Route::any(
    '/site_fonts/search',
    'Vault\GoogleFontController@search'
)->name('fonts_search');

/* ============================================================================================ */
/* Filter */
// 'vault/parse_google_fonts_file/filter',
Route::any(
    '/site_fonts/filter',
    'Vault\GoogleFontController@filter'
)->name('fonts_filter');

/* ============================================================================================ */
/* Filter Ordering: LAST-MODIFIED and VERSION */
// 'vault/parse_google_fonts_file/orderBy/{col}/{dir}',
Route::any(
    '/site_fonts/orderBy/{col}/{dir}',
    'Vault\GoogleFontController@ordering'
)->name('fonts_ordering');

/* ============================================================================================ */
