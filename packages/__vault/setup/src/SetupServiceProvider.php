<?php namespace Vault\Setup;

use Illuminate\Support\ServiceProvider;

class SetupServiceProvider extends ServiceProvider
{
    protected $view_dest = '/resources/views/includes/setup';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';

        $this->publishViews($this->view_dest); //resources/views/vault/monzamedia/vault-filemanager' );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // View(s)
        $this->loadViewsFrom(__DIR__ . '/resources/includes/setup', 'setup');
    }

    /**
     * Publish Package Views
     */
    private function publishViews($publish_to)
    {
        if (is_dir(__DIR__ . '/resources/includes/setup')) {
            $this->publishes([
              __DIR__ . '/resources/includes/setup' => base_path($publish_to)
            ], 'views');
        }
        return;
    }
}
