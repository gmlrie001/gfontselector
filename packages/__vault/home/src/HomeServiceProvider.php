<?php namespace Vault\Home;

use Illuminate\Support\ServiceProvider;

class HomeServiceProvider extends ServiceProvider
{
    protected $view_dest = '/resources/views/pages/home';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // include __DIR__ . '/routes.php';
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->publishViews($this->view_dest);

        // $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Route(s)
        // $this->loadRoutesFrom(__DIR__ . '/routes.php');

        // View(s)
        $this->loadViewsFrom(__DIR__ . '/resources/views/pages/home', 'home');
    }

    /**
     * Publish Package Views
     */
    private function publishViews($publish_to)
    {
        if (is_dir(__DIR__ . '/resources/views/pages/home')) {
            $this->publishes([
              __DIR__ . '/resources/views/pages/home' => base_path($publish_to)
            ], 'views');
        }
        return;
    }
}
