<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\HomeSetting;
use App\Models\HomeArticle;
use App\Models\HomeFeature;
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;

/* Custom Helper Functions and Traits */
use App\Helpers\Strings\StringHelper;
use App\Helpers\DateTime\DateHelper;

use App\Traits\SEO\SEOsTraits;
use App\Helpers\MiscHelpers;

class HomeController extends PageController
{
    use SEOsTraits;

    public function __construct()
    {
        parent::__construct();

        $this->data['page'] = Page::find(2);

        $this->strHelper  = new StringHelper();
        $this->dateHelper = new DateHelper();
        $this->miscHelper = new MiscHelpers();

        $this->data['soc_share'] = collect(Parent::_socshr($this->data['page']));
    }
    
    public function __invoke(Request $request)
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));
        unset($this->data['share_links'], $sm);

        $banners = PageBanner::status()->with('blocks')->where('page_id', $this->data['page']->id)->get();
        $this->data['banners']  = $banners->sortBy('order');

        $this->data['settings'] = HomeSetting::status()->first();
        $this->data['articles']  = HomeArticle::status()->ordering()->take(2)->get();

        // HomeFeature::status()->ordering()->get();
        $features = HomeFeature::status()->inRandomOrder()->take(2)->get();
        $this->data['features'] = $features;

        // HomeListing::status()->ordering()->take(18)->get();

        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        return view('pages.home.home', $this->data);
    }
}
