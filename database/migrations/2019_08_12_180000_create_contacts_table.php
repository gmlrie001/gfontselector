<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('mobile')->nullable();

            $table->longText('address')->nullable();
            $table->longText('postal_address')->nullable();

            $table->text('map_image')->nullable();
            $table->text('map_mobile_image')->nullable();
            $table->text('map_link')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1)->nullable();
        });

        Schema::create('contact_enquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();

            $table->string('company')->nullable();

            $table->string('subject')->nullable();

            $table->longText('message')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1)->nullable();
        });

        Schema::create('contact_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->text('page_banner_image')->nullable();
            $table->text('page_banner_image_mobile')->nullable();

            $table->longText('map_address')->nullable();
            $table->text('map_image')->nullable();
            $table->text('map_mobile_image')->nullable();
            $table->text('map_link')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_addresses');
        Schema::dropIfExists('contact_enquiries');
        Schema::dropIfExists('contact_settings');
    }
}
