<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Sites
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');

            $table->string('site_name')->nullable();
            $table->string('site_domain')->nullable();

            $table->longText('copyright')->nullable();

            $table->text('logo')->nullable();
            $table->text('logo_mobile')->nullable();

            // $table->text('footer_logo')->nullable();
            // $table->text('footer_logo_mobile')->nullable();

            $table->text('contact_map_image')->nullable();
            $table->text('contact_map_mobile_image')->nullable();
            $table->longText('contact_map_address')->nullable();

            $table->text('contact_map_link')->nullable();
            $table->text('contact_map_lat')->nullable();
            $table->text('contact_map_lon')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        // Login Logs
        Schema::create('login_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('ip_address')->nullable();
            $table->integer('user_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        // Page Views
        Schema::create('page_views', function (Blueprint $table) {
            $table->increments('id');
            $table->text('page_url')->nullable();
            $table->ipAddress('visitor_ip')->nullable();
            $table->text('countryCode')->nullable();
            $table->text('regionName')->nullable();
            $table->text('cityName')->nullable();
            $table->text('zipCode')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        // Users
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('surname')->nullable();

            $table->string('password')->nullable();
            $table->string('email')->unique();
            $table->string('mobile')->nullable();

            $table->integer('subscribed')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('discount_type')->nullable();

            $table->string('admin_type')->nullable();
            $table->string('user_group_id')->nullable();

            $table->rememberToken();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->datetime('status_date')->nullable();
            $table->integer('order')->nullable();
        });
        // User Groups
        Schema::create('user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('discount_type')->nullable();
            $table->decimal('discount', 10, 2)->nullable();
            $table->integer('activation_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->datetime('status_date')->nullable();
            $table->integer('order')->nullable();
        });

        // Pages
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->datetime('status_date')->nullable();
            $table->integer('order')->nullable();
        });
        // Page Sectional Decorating Banners
        Schema::create('page_section_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->text('section_banner_image')->nullable();
            $table->text('section_banner_image_mobile')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->nullable();
            $table->integer('page_id')->nullable();
        });

        // Page Banners
        Schema::create('page_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('max_columns')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->nullable();
            $table->integer('page_id')->nullable();
        });
        // Page Banner Blocks
        Schema::create('page_banner_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('column_count')->nullable();
            $table->string('link')->nullable();
            $table->string('link_target')->nullable();
            $table->text('banner_image')->nullable();
            $table->text('mobile_image')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->nullable();
            $table->integer('banner_id')->nullable();
        });

        // Share Links
        Schema::create('share_links', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title_display')->nullable();
            $table->string('platform')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->nullable();
        });
        // Social Media Links
        Schema::create('socials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('social_media_url')->nullable();
            $table->text('social_media_icon')->nullable();
            $table->text('social_media_icon_colour')->nullable();
            $table->text('social_media_icon_bg_colour')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        // Addresses, Enquiries and Signups
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('mobile')->nullable();
            $table->longText('address')->nullable();
            $table->longText('postal_address')->nullable();
            $table->text('map_image')->nullable();
            $table->text('map_mobile_image')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
        Schema::create('enquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('company')->nullable();
            $table->string('subject')->nullable();
            $table->longText('message')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sites');
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_groups');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('page_views');
        Schema::dropIfExists('page_section_banners');
        Schema::dropIfExists('page_banners');
        Schema::dropIfExists('page_banner_blocks');
        Schema::dropIfExists('login_logs');
        Schema::dropIfExists('share_links');
        Schema::dropIfExists('socials');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('enquiries');
    }
}
