<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();
            $table->text('url_title')->nullable();
            $table->text('pubdate')->nullable();

            $table->mediumText('listing_description')->nullable();
            $table->longText('description')->nullable();

            $table->text('listing_image')->nullable();
            $table->text('article_image')->nullable();

            $table->string('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });

        Schema::create('blog_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();

            $table->text('gallery_image')->nullable();

            $table->integer('blog_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->enum('status', ['PUBLISHED', 'UNPUBLISHED', 'DRAFT', 'SCHEDULED'])->default('PUBLISHED')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
        Schema::dropIfExists('blog_galleries');
    }
}
