<?php

use Illuminate\Database\Seeder;
use App\Models\Site;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = Site::where('site_name', 'like', '%' . "" . '%')->exists();

        // SETUP
        if (! $check) {
            $site = new Site();

            $copy_pre = 'Copyright ' . '&copy; ';
            $copy_pst = '&nbsp;All&nbsp;Rights&nbsp;Reserved';
            $crnt_yr  = date('Y');
            $copy_post = $crnt_yr . '. ' . $copy_pst;
            unset($copy_pst, $crnt_yr);
                
            // SITE NAME
            $site->site_name                = (env('APP_NAME') != '' || env('APP_NAME') != null) ? env('APP_NAME') : '';
            $site->site_domain              = null;

            // FOOTER Copyright Text
            $site->copyright                = $copy_pre .' '. $site->site_name .' '. $copy_post;

            // SITE Main and Footer Logos (responsive)
            $site->logo                     = null;
            $site->logo_mobile              = null;
            // $site->footer_logo              = null;
            // $site->footer_logo_mobile       = null;

            // SITE DEFAULT Physical Location Map Image <== PLEASE REMOVE
            // $site->contact_map_image        = null;
            // $site->contact_map_mobile_image = null;
            // $site->contact_map_address      = null;
            // $site->contact_map_lon          = null;
            // $site->contact_map_lat          = null;

            $site->save();
            unset($copy_pre, $crnt_post);
        }
    }
}
