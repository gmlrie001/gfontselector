<?php

use Illuminate\Database\Seeder;
use App\Models\UserGroup;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $check = UserGroup::whereId(0)->exists();
        date_default_timezone_set("Africa/Johannesburg");

        // if ($check) {
        //     DB::table('user_groups')->delete();
        // }

        // $data = [
        //     [ "id"=>1, "title"=>"super-admin", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>1 ],
        //     [ "id"=>2, "title"=>"admin", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>2 ],
        //     [ "id"=>3, "title"=>"developer", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>3 ],
        //     [ "id"=>4, "title"=>"marketing-admin", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>4 ],
        //     [ "id"=>5, "title"=>"content-editor", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>5 ],
        //     [ "id"=>6, "title"=>"post-editor", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>6 ],
        //     [ "id"=>7, "title"=>"default", "discount_type"=>0, "discount"=>0, "activation_type"=>0, "status"=>"PUBLISHED", "status_date"=>null, "order"=>7 ],
        // ];

        // if (DB::table('user_groups')->get()->count() == 0) {
        //     DB::table('user_groups')->insert($data);
        // }
        
        if (! $check) {
            $user_group = new UserGroup();
            $user_group->id              = 1;
            $user_group->title           = "super-admin";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 1;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 2;
            $user_group->title           = "admin";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 2;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 3;
            $user_group->title           = "developer";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 3;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 4;
            $user_group->title           = "marketing-admin";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 4;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 5;
            $user_group->title           = "content-editor";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 5;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 6;
            $user_group->title           = "post-editor";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 6;
            $user_group->save();

            $user_group = new UserGroup();
            $user_group->id              = 7;
            $user_group->title           = "default";
            $user_group->discount_type   = 0;
            $user_group->discount        = 0;
            $user_group->activation_type = 0;
            $user_group->status          = 'PUBLISHED';
            $user_group->status_date     = null;
            $user_group->order           = 7;
            $user_group->save();
        }
    }
}
