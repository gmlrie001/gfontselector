<?php
  /**
   *
   * Variability in the Page Banner image dimensions. Needs to be update when adding
   * new pages refernce by page_id on pages table.
   *
   */
  return [
    /* Setup defaulting values for the project just in case... */
    'default' => [
      'width'  => 425,
      'height' => 425
    ],
    /* The page_id=1 is the Site Settings, which does not produce a page banner */
    1  => [
      'width'  => null,
      'height' => null
    ],
    2  => [
      'width'  => 768,
      'height' => 600
    ],
    3  => [
      'width'  => 375,
      'height' => 470
    ],
    4  => [
      'width'  => 375,
      'height' => 470
    ],
    5  => [
      'width'  => 375,
      'height' => 470
    ],
    6  => [
      'width'  => null,
      'height' => null
    ],
    7  => [
      'width'  => null,
      'height' => null
    ],
    /* All this non-sense for 1 page banner dimensions... */
    8  => [
      'width'  => 425,
      'height' => 425
    ],
  ];
