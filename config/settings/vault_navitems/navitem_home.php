<?php

  return [

    'Home' => [

      'title' => 'Home',
      'page_id' => '2',
      'icon' => 'fa fa-home',
      'main_link_database_tables' => [
        'home_articles' => [
              'title' => 'Introduction',
              'icon' => 'fa fa-newspaper',
              'specific_id' => '',
              'sub_database_tables' => [
                  '',
              ]
         ],
        'home_settings' => [
            'title' => 'Settings',
            'icon' => 'fa fa-cogs',
            'specific_id' => '1',
            'sub_database_tables' => [
                '',
            ]
        ],
      ],
      'tab_database_tables' => [
          '',
      ]

    ],

  ]['Home'];
