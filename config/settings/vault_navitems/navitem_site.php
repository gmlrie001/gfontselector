<?php

  return [

    'Sites' => [

      'title' => 'Settings',
      'page_id' => '0',
      'icon' => 'fa fa-desktop',

      'main_link_database_tables' => [
          'sites' => [
              'title' => 'Site',
              'icon' => 'fa fa-cogs',
              'specific_id' => '1',
              'sub_database_tables' => [
                  '',
              ]
          ],
          'socials' => [
              'title' => 'Social Media',
              'icon' => 'fa fa-share-alt',
              'specific_id' => '',
              'sub_database_tables' => [
                  '',
              ]
          ],
          'share_links' => [
              'title' => 'Social Media Sharing',
              'icon' => 'fa fa-hashtag',
              'specific_id' => '',
              'sub_database_tables' => [
                  '',
              ]
          ],
      ],
      'tab_database_tables' => [
          '',
      ]

    ],

  ]['Sites'];
