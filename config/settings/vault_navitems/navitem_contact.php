<?php

  return [

    'Contacts' => [

      'title' => 'Contacts',
      'page_id' => '', // 8
      'icon' => 'fa fa-address-book',

      'main_link_database_tables' => [
        'contact_settings' => [
          'title' => 'Contact Settings',
          'icon' => 'fa fa-cogs',
          'specific_id' => '1',
          'sub_database_tables' => [
              '',
          ]
        ],
        'contact_addresses' => [
          'title' => 'Addresses',
          'icon' => 'fa fa-map',
          'specific_id' => '',
          'sub_database_tables' => [
              '',
            ]
        ],
        'contact_enquiries' => [
          'title' => 'Contact Enquiries',
          'icon' => 'fa fa-question',
          'specific_id' => '',
          'sub_database_tables' => [
              '',
          ]
        ],
      ],
      'tab_database_tables' => [
        '',
      ]

    ],

  ]['Contacts'];
