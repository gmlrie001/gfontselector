<?php

return [

  'Frontend Settings' => [

    'title' => 'Frontend Settings',
    'page_id' => '', // 8
    'icon' => 'fa fa-globe',

    'main_link_database_tables' => [
      'site_fonts' => [
        'title' => 'Frontend Font Styles',
        'icon' => 'fa fa-style',
        'specific_id' => '1',
        'sub_database_tables' => [
            '',
        ]
      ],
    ],
    'tab_database_tables' => [
      '',
    ]

  ],

]['Frontend Settings'];
