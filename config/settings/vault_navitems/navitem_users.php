<?php

  return [
    'Users' => [
      
      'title' => 'Users',
      'page_id' => '',
      'icon' => 'fa fa-user',
      'main_link_database_tables' => [
          'users' => [
              'title' => 'User Accounts',
              'icon' => 'fa fa-users',
              'specific_id' => '',
              'sub_database_tables' => [
                'group_users'
              ]
          ],
          'user_groups' => [
              'title' => 'User Groups',
              'icon' => 'fa fa-object-group',
              'specific_id' => '',
              'sub_database_tables' => [
                ''
              ]
          ],
      ],
      'tab_database_tables' => [
        'group_users'
      ]
    ],

  ]['Users'];
