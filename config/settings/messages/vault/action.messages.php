<?php

  return [

    'crud'    => [
      'add'           => 'Entry has been added successfully',
      'update'        => 'Entry has been updated successfully',
      'trash'         => 'Entry has beed trahsed successfully',
      'delete'        => 'Entry has been deleted permanently',
      'select_trash'  => 'Selection has been trashed successfully!',
      'select_delete' => 'Selection has been deleted successfully!',
      'restore'       => 'Entry has been restored successfully!',
      'no_model'      => 'Model not found',
      'no_table'      => 'Table does not exist',
    ],

    'order'   => [
      'reordering'    => 'Successfully reordered!'
    ],

    'search'  => [
      '' => '',
    ],

    'status'  => [
      '' => '',
    ],

    'image'   => [
      'remove'        => 'Successfully removed ',/* studly_case( $field _imageFileName_) */
    ],

    'login'   => [
      '' => '',
    ],

  ];
  