<?php
  /**
   *
   * Variability in the Page Banner image dimensions. Needs to be update when adding
   * new pages refernce by page_id on pages table.
   *
   */
  return [
    /* Setup defaulting values for the project just in case... */
    'default' => [
      'width'  => 768,
      'height' => 450
    ],
    /* The page_id=1 is the Site Settings, which does not produce a page banner */
    1  => [
      'width'  => 768,
      'height' => 840
    ],
    2  => [
      'width'  => 768,
      'height' => 450
    ],
    3  => [
      'width'  => 768,
      'height' => 450
    ],
    4  => [
      'width'  => 768,
      'height' => 450
    ],
    5  => [
      'width'  => 768,
      'height' => 450
    ],
    6  => [
      'width'  => 768,
      'height' => 450
    ],
    /* All this non-sense for 1 page banner dimensions... */
    7  => [
      'width'  => 768,
      'height' => 450
    ],
  ];
