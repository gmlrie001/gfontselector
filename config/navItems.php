<?php
/**
 *
 * Vault Sidebar Navigation Menu Item Configuration
 *
 * Major overhaul to use include section config files, within the folder:
 * ]> base_path()/config/settings/vault_navitems/{...}
 *
 */

return [
  'items' => [
    'Sites'             => include(base_path('config/settings/vault_navitems/navitem_site.php')),
    'Frontend Settings' => include(base_path('config/settings/vault_navitems/navitem_font_select.php')),
    'Users'             => include(base_path('config/settings/vault_navitems/navitem_users.php')),
    'Home'              => include(base_path('config/settings/vault_navitems/navitem_home.php')),
    'Collections'       => include(base_path('config/settings/vault_navitems/navitem_collections.php')),
    'Blog'              => include(base_path('config/settings/vault_navitems/navitem_blog.php')),
    'Contacts'          => include(base_path('config/settings/vault_navitems/navitem_contact.php')),
  ]
];
