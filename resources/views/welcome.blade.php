<!DOCTYPE html>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Laravel</title>

  @stack( 'templateHeader' )
  
  <!-- Font Awesome 5.8.1 SHIM via CDN or LOCAL -->
  <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" media="print" 
    onload="this.media='all';this.removeAttribute('onload');return;">
  <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css" media="print" 
    onload="this.media='all';this.removeAttribute('onload');return;">

  <link href="{{ asset('/assets/_vault/templating/_/css/footer/_footer-variables.css') }}" rel="stylesheet" 
    onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
  <link href="{{ asset('/assets/_vault/templating/_/css/footer/footer.css') }}" rel="stylesheet" 
    onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
  <link href="{{ asset('/assets/_vault/templating/_/css/footer/copyright.css') }}" rel="stylesheet" 
    onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
  <link href="{{ asset('/assets/_vault/templating/_/css/footer/navigation/navigation.css') }}" rel="stylesheet" 
    onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">

  <!-- Fonts and Reset CSS -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&swap=display" rel="stylesheet" media="print"
    onload="this.media='all';this.removeAttribute('onload');return;">

  <style>
    html,
    body {
      display: flex;
      min-height: 100vh;
      margin: 0;
      padding: 0;
      background-color: #ffffff;
      flex-direction: column;
      font-family: 'Open Sans', sans-serif;
      font-size: 16px;
    }
    .full-height {
      height: 100vh;
    }
    .flex-center {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .title {
      font-size: 84px;
    }
    figure {
      margin-block-start: 0;
      margin-block-end: 0;
      margin-inline-start: 0;
      margin-inline-end: 0;
    }
  </style>

  <!-- JQuery (slim version has NO AJAX or Effects)-->
  <script id="jQueryLibrary" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <!-- INSECURE Version: DO NOT use version < 3.4.1 -->

</head>

@php ob_flush(); @endphp

<body>

  <div class="sticky-header stick-me" role="header" style="">
    @yield( 'header' )
  </div>

  <div class="main-content" role="main"><main>
    <a title="Testing back() functionality" class="testing completed d-none collapse hidden"
      href="{{ url()->previous() }}" hidden>Gou Terug</a>

    @if( null != $banners && count( $banners ) > 0 )
    <div class="container-fluid d-none">
    @forelse( $banners as $k=>$banner )
      @forelse( $banner->blocks as $key=>$block )
      <figure class="m-0 p-0 mw-100">
        <picture>
          @isset( $block->mobile_image )
          <source srcset="/{{ ltrim( $block->mobile_image ) }}" media="only screen and (max-width: 992px)">
          @endisset
          @isset( $block->banner_image )
          <source srcset="" media="only screen and (max-width: 992px)">
          <img class="img-fluid" src="/{{ ltrim( $block->banner_image, '/' ) }}" alt="{{ ucwords( $block->title ) }}">
          @endisset
        </picture>
      </figure>
      @empty
      @endforelse
    @empty
    @endforelse
    </div>
    @endif

    <div class="flex-center position-relative full-height">
      @if (Route::has('login'))
      <div class="top-right links">
        @auth
        <a href="{{ url('/home') }}">Home</a>
        @else
        <a href="{{ route('login') }}">Login</a>
        @if (Route::has('register'))
        <a href="{{ route('register') }}">Register</a>
        @endif
        @endauth
      </div>
      @endif
      <div class="content text-center">
        <div class="title mb-md-5">
          @if( env( 'APP_NAME' ) )
          {{ env( 'APP_NAME' ) }} &ndash; Vault
          @else
          Vault
          @endif
        </div>
        <div class="links d-flex flex-row justify-content-center">
          <a class="mx-lg-auto mx-3" href="http://dev.pretty-docs.co.za">Docs</a>
          <a class="mx-lg-auto mx-3" href="/dontexist">Vault Setup</a>
          <a class="mx-lg-auto mx-3" href="{{ route( 'the_vault' ) }}">Vault</a>
        </div>
      </div>
    </div>
  </main></div>

  <div class="footer-wrapper" role="contentinfo">
    @include( 'includes.pages.footer.decofurnsa_footer' )
  </div>

  <script type="text/javascript" src="{{ asset('/assets/_vault/templating/_/js/header/header.js') }}"></script>

</body>

</html>
