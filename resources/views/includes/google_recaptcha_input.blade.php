<style>
.g-recaptcha ~ .contact-form button[type=submit] {
    margin-top: 0rem;
}
@media only screen and (max-width:1440px) {
    .row-contact-form button, .contact-form input, .contact-form textarea {
        height: 50px;
        line-height: 50px;
    }
    .contact-form input, .contact-form textarea {
        padding: 0 1rem;
    }
    .contact-form button {
        width: auto !important;
        padding: 0 12.5%;
    }
}
@media only screen and (max-width:768px) {
    form#contact-form {
        padding: 2rem 0 !important;
    }
    .contact-form button {
        width: auto !important;
        padding: 0 25%;
    }
}
</style>

<div class="form-group" id="starbrandsGR">
    <div class="g-recaptcha" id="contact-captcha|product-captcha|platinum-captcha" data-sitekey="6Lcqp8MUAAAAABBS-OtTvC4spixwmGx5QX6UjRI1"></div>
</div>


<script type="text/javascript">
(()=>{
  _gr = setInterval( ()=>{
    check_grecaptcha();
  }, 250 )
})();
</script>

<script>
document.addEventListener( 'DOMContentLoaded', ()=>{
	var btn = document.querySelector('#contact-enquiry button[type=submit]');
	btn.setAttribute( 'disabled', true );
    mm_forms( '[type=submit]', 'form' );
})

function check_grecaptcha() {
  if( grecaptcha.getResponse() != "" ) {
    var b = document.querySelector( '#contact-enquiry button[type=submit]' );
        b.removeAttribute( 'disabled' );
    clearInterval( _gr );
    return;
  }
}

function mm_forms(btn, frm) {
  var f = document.querySelector( btn );//'#contact-enquiry button[type=submit]'
  f.addEventListener( 'click', (e)=>{
    e.stopPropagation();
    e.preventDefault();
    f.innerHTML = '<i class="mr-3 fa fa-spinner fa-spin"></i>Loading';
    var t = document.querySelector( '#contact-enquiry input[name=_token]' );
    var i = document.createElement('input');
    var s = e.srcElement.parentNode.closest( frm );//.enquiry-form
    if ( ! t ) {
      i.setAttribute( 'type', 'hidden' );
      i.setAttribute( 'name', '_token' );
      i.setAttribute( 'value', csrf_token() );

      s.appendChild( i );
      s.action = {{ route( "contact_enquiry" ) }};

      setTimeout( ()=>{ s.submit(); }, 100 );
    } else {
      if (grecaptcha.getResponse() != "") {
          s.submit();
      } else {
        f.innerHTML = 'Retry Send Enquiry';
        return;
      }
    }
    return;
  }, false );
}
</script>

<script>
    let tmp, frm;
    let temp, form, err;

    tmp = document.querySelector( '.g-recaptcha' );
    do {
        tmp = tmp.parentNode;
    } while ( tmp.parentNode.closest( 'form' ) ) 
    frm = tmp;

    frm.addEventListener( 'submit', function( evt ) {
        evt.preventDefault();
        return (
            grecaptcha.getResponse() == "" ||
            grecaptcha.getResponse() == undefined ||
            grecaptcha.getResponse() == null
        ) 
        ? console.warn( "You cannot proceed without completing the ReCAPTCHA field!" )
        : evt.srcElement.submit();
    }, false );

    try {
        temp = document.querySelector( '.g-recaptcha' )
        do {
            temp = temp.parentNode;
        } while ( temp.parentNode.closest( 'form' ) ) 
        form = temp;
    } catch( err ) {
        console.warn( "\r\nElement selection error:\r\n\t" + err + "\r\n" );
    }

    try {
        if ( form.addEventListener ) {
            form.addEventListener( 'submit', formSubmitCallback, false );
        } else if ( form.attachEvent ) {
            form.attachEvent( 'onsubmit', formSubmitCallback, false );
        }
    } catch( err ) {
        console.warn( "\r\nElement selection error:\r\n\t" + err + "\r\n" );
    }

    function formSubmitCallback( event ) {
        event.preventDefault();

        if ( grecaptcha.getResponse() === "" ) {
            let g_err = document.querySelector( '.g-recaptcha' ),
            g_iframe  = document.querySelector( '.g-recaptcha iframe' ),

            gw = parseInt( g_iframe.width - 2 ),
            gh = parseInt( g_iframe.height - 2 );
    
            g_err.setAttribute( 'style', 'width: ' + gw + 'px;' + 'height: ' + gh + 'px;' + 'box-shadow: 0 0 12px 3px rgba( 255, 0, 0, 0.75);' );
    
            console.error( "\r\nYou cannot proceed without completing the ReCAPTCHA!\r\n" );
            return;
        } else {
            let g_err = document.querySelector( '.g-recaptcha' );
    
            if ( g_err.hasAttribute( 'style' ) ) g_err.removeAttribute( 'style' );
    
            return event.srcElement.submit();
        }
    }
</script>
