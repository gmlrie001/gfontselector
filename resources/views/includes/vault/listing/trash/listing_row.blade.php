<div class="col-xs-12 padding-0 listing-row">
    <div class="col-xs-12 padding-0">
        <div class="col-xs-1 col-md-1 padding-0">
            <div class="col-md-6 tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_select">
                <label>
                    <input class="mass-operation" type="checkbox" value="{{$result->id}}" name="entryIds[]" />
                </label>
            </div>
            @if($mainDropdownField != "" || $imageDropdownField != "" || $can_order)
                <a href="#" class="col-md-6 accord listing-hide fa fa-plus tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_accord"></a>
            @else
                <div class="col-md-6 listing-hide"></div>
            @endif
        </div>
        <div class="col-xs-2 col-md-2 padding-0">
            <span class="col-md-12 text-center resizeme"><?php echo $result->{$orderField}; ?></span>
        </div>
        <div class="col-xs-4 col-md-4">
            <span class="col-md-12 padding-0"><?php echo str_limit(strip_tags($result->{$titleField}), 150); ?></span>
        </div>
        <div class="col-xs-2 col-md-2">
            <span class="col-md-12 padding-0 text-center resizeme">TRASHED</span>
        </div>
        <div class="col-xs-2 col-md-2">
            <span class="col-md-12 padding-0 text-center resizeme">{{$result->updated_at}}</span>
        </div>
        <div class="col-xs-2 col-md-1 padding-0">
            <div class="col-xs-6 col-md-6 text-center">
                @if($can_order)
                    <i data-order="<?php echo $result->{$orderField}; ?>" data-id="{{$result->id}}" class="fa fa-arrows-alt tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_handles"></i>
                @endif
            </div>
            <div class="col-xs-6 col-md-6 options-open text-center">
                <a href="/vault/{{$table}}/restore/{{$result->id}}" class="fa fa-restore tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_restore">
                    <img class="img-responsive" src="/assets/images/vault/template/functions/restore-icon.png" />
                </a>
            </div>
        </div>
    </div>
    @if($mainDropdownField != "" || $imageDropdownField != "" || $can_order)
        <div class="col-xs-12 padding-0 listing-dropdown">
            <div class="col-xs-12 padding-0 drop-down-title">
                @if($mainDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-6">{{str_replace("_", " ", $mainDropdownField)}}</div>
                @endif
                @if($imageDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-4">{{str_replace("_", " ", $imageDropdownField)}}</div>
                @endif
                <div class="col-xs-12 col-sm-2 col-md-2"></div>
            </div>
            <div class="col-xs-12 padding-0 drop-down-items">
                @if($mainDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-6"><?php echo str_limit(strip_tags($result->{$mainDropdownField}), 200); ?></div>
                @endif
                @if($imageDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-4"><img class="img-responsive" src="/<?php echo $result->{$imageDropdownField}; ?>" /></div>
                @endif
                <a href="/vault/{{$table}}/delete/{{$result->id}}/permanent" class="col-xs-12 col-sm-2 col-md-2 delete-button">Delete Forever</a>
            </div>
        </div>
    @endif
</div>