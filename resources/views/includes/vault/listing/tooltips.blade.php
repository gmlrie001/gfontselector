<div  class="tooltip_templates" style="display: none;">
    <div id="tooltip_content_export">
        <p>Export items to PDF or Excel.</p>
    </div>
    <div id="tooltip_content_order">
        <p>Change the order of items displayed.</p>
    </div>
    <div id="tooltip_content_show">
        <p>Change the number of items shown on each page.</p>
    </div>
    <div id="tooltip_content_down">
        <p>Move Item down one in the list</p>
    </div>
    <div id="tooltip_content_up">
        <p>Move Item up one in the list</p>
    </div>
    <div id="tooltip_content_duplicate">
        <p>Duplicate this item</p>
    </div>
    <div id="tooltip_content_deep_duplicate">
        <p>Duplicate this item and its relationships</p>
    </div>
    <div id="tooltip_content_delete">
        <p>Delete this item</p>
    </div>
    <div id="tooltip_content_restore">
        <p>Restore this item</p>
    </div>
    <div id="tooltip_content_active">
        <p>Mark this item as inactive</p>
    </div>
    <div id="tooltip_content_inactive">
        <p>Mark this item as active</p>
    </div>
    <div id="tooltip_content_edit">
        <p>Edit item’s content</p>
    </div>
    <div id="tooltip_content_options">
        <p>Click to view options for this item</p>
    </div>
    <div id="tooltip_content_handles">
        <p>Click and hold to drag the row and reorder the list</p>
    </div>
    <div id="tooltip_content_reorder">
        <p>Swap the order of this row and the order number entered</p>
    </div>
    <div id="tooltip_content_search">
        <p>Search for an item in the list</p>
    </div>
    <div id="tooltip_content_add">
        <p>Add an entry</p>
    </div>
    <div id="tooltip_content_select">
        <p>Add this item to the mass operation selection</p>
    </div>
    <div id="tooltip_content_accord">
        <p>Open item's addional information</p>
    </div>
    <div id="tooltip_content_reset_order">
        <p>Reset the order for any ordering bugs</p>
    </div>
</div>