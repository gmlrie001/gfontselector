@php
    $excludeTables = config('excluded_listings.excluded_listings');
    $currentUrlPath = Request::path();
    $levels = config('admin_levels');
    $levels = $levels['admin_levels'];

    $userLevel = Session::get('user.admin_type');

    $userAccess = $levels[$userLevel]['access'];

    $tab_list = array_reverse($tab_list);
@endphp

@push( 'vaultPageStyles' )
<style>
/*
.mCustomScrollBox {
  display: flex !important;
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
}
#mCSB_1_scrollbar_horizontal.mCSB_scrollTools {
    min-width: 100% !important;
}
*/
/* Responsive for the above classes */
/* @media only screen and (max-width: 768px) {
  .mCustomScrollBox {}
  #mCSB_1_scrollbar_horizontal.mCSB_scrollTools {}
} */
</style>
@endpush

<div class="col-xs-12 padding-0 tab-container">
    <div class="tabs" style="display:flex;flex-direction:row;">
        @foreach($tab_list as $key => $tabInfo)
            @php
                $tabTbl = key($tabInfo);
                $tabId = $tabInfo[$tabTbl];
            @endphp
            @if($tabId == 0)
                @if(in_array($tabTbl, $userAccess))
                    @if(!in_array($tabTbl, $excludeTables))
                        @if($currentUrlPath != "vault/".$tabTbl)
                            <a href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @else
                            <a class="active" id="active_tab" href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                        @endif
                    @endif
                @endif
            @else
                @if(in_array($tabTbl, $userAccess))
                    @if(!in_array($tabTbl, $excludeTables))
                        @if($key == 0)
                            @if($currentUrlPath != "vault/".$tabTbl)
                                <a href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                            @else
                                <a class="active" id="active_tab" href="/vault/{{$tabTbl}}">{{str_replace("_", " ", $tabTbl)}}</a>
                            @endif
                        @else
                            @if($currentUrlPath != "vault/".$tabTbl."/relation/".$tabId)
                                <a href="/vault/{{$tabTbl}}/relation/{{$tabId}}">{{str_replace("_", " ", $tabTbl)}}</a>
                            @else
                                <a class="active" id="active_tab" href="/vault/{{$tabTbl}}/relation/{{$tabId}}">{{str_replace("_", " ", $tabTbl)}}</a>
                            @endif
                        @endif
                    @endif
                    <a href="/vault/{{$tabTbl}}/edit/{{$tabId}}">Edit {{str_replace("_", " ", str_singular($tabTbl))}}</a>
                @endif
            @endif
        @endforeach
        @if(!in_array($table, $excludeTables) && $tabId != 0)
            @if($currentUrlPath != "vault/".$table."/relation/".$tabId)
                <a href="/vault/{{$table}}/relation/{{$tabId}}" data-iam>{{str_replace("_", " ", $table)}}</a>
            @else
                <a class="active" id="active_tab" href="/vault/{{$table}}/relation/{{$tabId}}">{{str_replace("_", " ", $table)}}</a>
            @endif
        @endif

        @if(isset($entry))
            <div class="pull-left">
            <a href="#" class="active" id="active_tab">{{$method}} {{str_replace("_", " ", str_singular($table))}}</a>
            @if(sizeof($relationships))
                @foreach($relationships as $tbl => $relationship)
                    @if(in_array($tbl, $userAccess))
                        @if(!in_array($tbl, $excludeTables))
                            <a href="/vault/{{$tbl}}/relation/{{$entry->id}}">{{str_replace("_", " ", $relationship)}}</a>
                        @endif
                    @endif
                @endforeach
            @endif
            </div>
        @endif
    </div>
    @if(isset($entry))
      @php
        $method = "update";
        $button = "edit";
        $ahref  = "javascript:void(0)";
        $counte = (int) count( Request::segments() ) - 2;
        if( isset( $method ) && ( strtolower( $method ) != 'update' || strtolower( Request::segments()[$counte] ) != 'edit' ) ) {
            $method = "add";
            $button = $method;
            $ahref  = "add";
        }
      @endphp
      <a href={{$ahref}} class="pull-right {{$button}}-btn tooltipster" 
        data-tooltipster='{"side":"top","animation":"fade"}' 
        data-tooltip-content="#tooltip_content_{{$button}}" 
      >{{ ucfirst( $method ) }}<span class="d-none d-xl-inline"> entry</span></a>
    @else
      <a href="/vault/{{$table}}/add" class="pull-right add-btn tooltipster" 
        data-tooltipster='{"side":"top","animation":"fade"}' 
        data-tooltip-content="#tooltip_content_add" 
      ><span class="d-none d-xl-inline">Add</span><span class="d-inline d-xl-none mr-2"><i class="fa fa-plus-circle"></i></span> entry</a>
    @endif
</div>
<script type="text/javascript">
    $(window).on("load",function(){
        $(".tabs").mCustomScrollbar({
          axis:"x",
          theme:"dark-3",
          scrollButtons: {
            enable: true,
            scrollType: 'stepless'
          },
          advanced:{
            autoExpandHorizontalScroll:true //optional (remove or set to false for non-dynamic/static elements)
          }
        });
        $('.tabs').mCustomScrollbar('scrollTo', '#active_tab');
      });
</script>
<script type="text/javascript">
$(document).ready(()=>{
    var update_btn, form, err;
    update_btn = document.querySelector('.tab-container a.edit-btn');
          form = document.querySelector('form[action*=edit].form-page');
    try {
        if( update_btn.addEventListener ) {
            update_btn.addEventListener( 'click', (evt) => {
                evt.preventDefault();
                try {
                    form.submit();
                } catch( err ) {/* Error */}
                return;
            }, false );
        } else if ( update_btn.attachEvent ) {
            update_btn.attachEvent( 'onclick', (evt) => {
                evt.preventDefault();
                try {
                    form.submit();
                } catch( err ) {/* Error */}
                return;
            } );
        }
    } catch( err ) { /* Error */ }
});
</script>
