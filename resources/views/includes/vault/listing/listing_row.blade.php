<div class="col-xs-12 padding-0 listing-row">
    <div class="col-xs-12 padding-0">
        <div class="col-xs-1 col-md-1 padding-0">
            <div class="col-md-6 tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_select">
                <label>
                    <input class="mass-operation" type="checkbox" value="{{$result->id}}" name="entryIds[]" />
                </label>
            </div>
            @if($mainDropdownField != "" || $imageDropdownField != "" || $can_order)
                <a href="#" class="col-md-6 accord listing-hide fa fa-plus tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_accord"></a>
            @else
                <div class="col-md-6 listing-hide"></div>
            @endif
        </div>
        <div class="col-xs-2 col-md-2 padding-0">
            <span class="col-md-12 text-center resizeme"><?php echo $result->{$result->orderField}; ?></span>
    <!-- BUG FIX -->
    <!-- Previous had <?php // echo $result->{$orderField};?>, {{ $result->orderField }} which changes the value of the order from the order number to value of column being ordered -->
        </div>
        <div class="col-xs-4 col-md-4">
            <span class="col-md-12 padding-0"><?php echo str_limit(strip_tags($result->{$titleField}), 50); ?></span>
        </div>
        <div class="col-xs-2 col-md-2">
            <span class="col-md-12 padding-0 text-center resizeme"><?php echo $result->{$statusField}; ?></span>
        </div>
        <div class="col-xs-2 col-md-2">
            <span class="col-md-12 padding-0 text-center resizeme">{{$result->updated_at}}</span>
        </div>
        <div class="col-xs-2 col-md-1 padding-0">
            <div class="col-xs-6 col-md-6 text-center padding-0">
                @if($can_order)
                    <i data-order="<?php echo $result->{$orderField}; ?>" data-id="{{$result->id}}" class="fa fa-arrows-alt tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_handles"></i>
                @endif
            </div>
            <div class="col-xs-6 col-md-6 options-open text-center">
                <div class="options">
                    @if($can_order)
                        <a href="/vault/{{$table}}/down/{{$result->id}}" class="fa fa-chevron-down tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_down"></a>
                        <a href="/vault/{{$table}}/up/{{$result->id}}" class="fa fa-chevron-up tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_up"></a>
                    @endif
                    <a href="/vault/{{$table}}/delete/{{$result->id}}" class="fa fa-trash-o tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_delete" onclick="confirm_action( event );"></a>
                    <a href="/vault/{{$table}}/duplicate/{{$result->id}}" class="fa fa-copy tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_duplicate"></a>
                    <a href="/vault/{{$table}}/deep_duplicate/{{$result->id}}" class="fa fa-file-export tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_deep_duplicate"></a>
                    @if($has_status)
                        <a href="/vault/{{$table}}/status/change/{{$result->id}}" class="fa fa-exclamation-triangle tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_active"></a>
                    @endif
                    <a href="/vault/{{$table}}/edit/{{$result->id}}" class="fa fa-edit tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_edit"></a>
                </div>
                <i class="fa fa-gear tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_options"></i>
            </div>
        </div>
    </div>
    <div class="options mobile-options">
        @if($can_order)
            <a href="/vault/{{$table}}/down/{{$result->id}}" class="fa fa-chevron-down tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_down"></a>
            <a href="/vault/{{$table}}/up/{{$result->id}}" class="fa fa-chevron-up tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_up"></a>
        @endif
        <a href="/vault/{{$table}}/delete/{{$result->id}}" class="fa fa-trash-o tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_delete" onclick="confirm_action( event );"></a>
        <a href="/vault/{{$table}}/duplicate/{{$result->id}}" class="fa fa-copy tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_duplicate"></a>
        <a href="/vault/{{$table}}/deep_duplicate/{{$result->id}}" class="fa fa-file-export tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_deep_duplicate"></a>
        @if($has_status)
            <a href="/vault/{{$table}}/status/change/{{$result->id}}" class="fa fa-exclamation-triangle tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_active"></a>
        @endif
        <a href="/vault/{{$table}}/edit/{{$result->id}}" class="fa fa-edit tooltipster"data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_edit"></a>
        @if($mainDropdownField != "" || $imageDropdownField != "" || $can_order)
            <a class="fa fa-plus accord tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_accord"></a>
        @endif
    </div>
    @if($mainDropdownField != "" || $imageDropdownField != "" || $can_order)
        <div class="col-xs-12 padding-0 listing-dropdown">
            <div class="col-xs-12 padding-0 drop-down-title">
                @if($mainDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-6">{{str_replace("_", " ", $mainDropdownField)}}</div>
                @endif
                @if($imageDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-4">{{str_replace("_", " ", $imageDropdownField)}}</div>
                @endif
                @if($can_order)
                    <div class="col-xs-12 col-sm-2 col-md-2">Reorder</div>
                @endif
            </div>
            <div class="col-xs-12 padding-0 drop-down-items">
                @if($mainDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-6"><?php echo str_limit(strip_tags($result->{$mainDropdownField}), 200); ?></div>
                @endif
                @if($imageDropdownField != "")
                    <div class="col-xs-12 col-sm-5 col-md-4"><img class="img-responsive" src="/<?php echo $result->{$imageDropdownField}; ?>" /></div>
                @endif
                @if($can_order)
                    <div class="col-xs-12 col-sm-2 col-md-2">
                        {!!Form::open(array('url' => '/vault/'.$table.'/bulk/order/', 'method' => 'post', 'class' => 'reorder-form'))!!}
                            {!!Form::hidden('entry', $result->id)!!}
                            {!!Form::hidden('current_order', $result->{$orderField})!!}
                            <div class="col-xs-6 col-md-6">
                                {!!Form::text('new_order', null, array('class' => 'form-control', 'placeholder' => $result->{$orderField}))!!}
                            </div>
                            <button type="submit" class="col-xs-6 col-md-6 tooltipster" data-tooltipster='{"side":"bottom","animation":"fade"}' data-tooltip-content="#tooltip_content_reorder">
                                <img src="/assets/images/vault/template/functions/reorder.svg" class="img-responsive" />
                            </button>
                        {!!Form::close()!!}
                    </div>
                @endif
            </div>
        </div>
    @endif
</div>

<script>
function confirm_action( ) {
    var c;

    //console.log( event.preventDefault() );
    event.preventDefault();
    //return;

    c = confirm( 'Are you sure that you want to trash this item?' );

    if ( c === true ) {
        window.location.href = event.srcElement.href;
    } else {
        return;
    }
    return;
}
</script>

