<div class="col-xs-12 col-sm-5 selected-options">
    <div class="col-xs-12 col-sm-4 padding-0">
        <div class="dropup tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_selected">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Export
            <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="/vault/create-selected-excel/{{$table}}" class="get-selected-items" >Export Excel</a></li>
                <li><a href="/vault/create-selected-pdf/{{$table}}" class="get-selected-items">Export PDF</a></li>
            </ul>
        </div>
        <div  class="tooltip_templates" style="display: none;">
            <div id="tooltip_content_selected">
                <p>Export the selected items to PDF or Excel</p>
            </div>
        </div>
    </div>
    @if($has_status)
        <div class="col-xs-12 col-sm-4 padding-0">
            <a href="/vault/{{$table}}/change-selected-status/" class="get-selected-items">Change Status</a>
        </div>
    @endif
    <div class="col-xs-12 col-sm-4 padding-0">
        <a href="/vault/{{$table}}/delete-selected" class="get-selected-items">Delete Selected</a>
    </div>
</div>