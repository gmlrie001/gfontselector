<div class="pull-left function-button">
    <span class="tooltipster" data-tooltipster='{"side":"top","animation":"fade"}' data-tooltip-content="#tooltip_content_show">
        <img src="/assets/images/vault/template/functions/show-icon.svg" />
        <span>Show</span>
        <img src="/assets/images/vault/template/functions/dropdown-icon.svg" />
    </span>
    <div class="function-dropdown">
        <a href="/vault/limit/set/12">Show 12 per page</a>
        <a href="/vault/limit/set/24">Show 24 per page</a>
        <a href="/vault/limit/set/48">Show 48 per page</a>
        <a href="/vault/limit/set/100">Show 100 per page</a>
        <a href="/vault/limit/set/200">Show 200 per page</a>
    </div>
</div>