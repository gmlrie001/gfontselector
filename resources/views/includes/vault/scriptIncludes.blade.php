<script src="{{ asset('/assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/Parsley.js-2.1.2/dist/parsley.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/modernizr.custom.60587.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/js/vendor/foundation/foundation.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/tooltipster/dist/js/tooltipster.bundle.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('/assets/js/vendor/slick-1.5.7/slick/slick.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/paulkinzett-toolbar-5112fbf/jquery.toolbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/jquery-ui-1.12.0.custom/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/croppic-master/croppic.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('/assets/js/vendor/fontawesome-iconpicker-1.0.0/dist/js/fontawesome-iconpicker.min.js') }}" type="text/javascript"></script>
<!-- <script src="{{ asset('/assets/js/vendor/fontawesome-iconpicker-3.0.0/dist/js/fontawesome-iconpicker.min.js') }}" type="text/javascript"></script> -->

<script src="{{ asset('/assets/js/vendor/TinyColor-1.0.0/tinycolor.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/multiselect-master/js/multiselect.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/notify.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/selectize.js-master/dist/js/standalone/selectize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/icheck-1.x/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/stickyHeader/sticky-header.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/hammer.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/jquery.hammer.js-master/jquery.hammer.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/bootstrap-datetimepicker-master/src/js/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/FitText.js-master/jquery.fittext.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('/assets/js/vendor/bootstrap-colorpickersliders-master/src/bootstrap.colorpickersliders.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/js/vendor/jquery.ddslick.js') }}" type="text/javascript"></script>