@php
  $arb_sentence = "The quick brown fox jumps over the lazy dog.";
@endphp

@isset( $font )
<article class="col-lg-4 col-12 google_fonts card pb-lg-0 @if ( $key > 11 ) hidden @endif" data-font-id="{{ $font->id }}" data-family="{{ str_replace( ' ', '+', $font->family ) }}" data-category="{{ $font->category }}" data-version="{{ $font->version }}" data-variants="{{ implode(",", $font->variants) }}">

  <section class="card-body category-{{ $font->category }}" style="font-family:'Roboto',sans-serif;border:1px solid #eaeaea;padding:10px 25px;border-radius:1rem;">

    <div class="card-title mb-lg-0 d-flex flex-lg-column flex-row flex-wrap justify-content-between ellipsis">
      <div class="row">
        <div class="col-12 col-lg font-title">
          <div class="wrapper">
            <h3 class="card-title mb-0">
              {{ ucwords( $font->family ) }} 
            </h3>
            <h5>Font has {{ count( $font->variants ) ?? 0 }} variants & {{ count($font->subsets) ?? 0 }} subsets</h5>
          </div>
        </div>

        <div class="d-none collapse hidden" hidden>
          @isset($font->version)
          <div class="col-12 col-lg-auto font-version d-none collapse hidden" hidden>
            <div class="text-center versioned" style="width:25px;height:25px;padding:0;border:1px solid #000;border-radius:50%;line-height:25px;">
              <span style="font-family:fantasy!important;font-weight:300!important;font-size:13px!important;">{{ $font->version }}</span>
            </div>
          </div>
          @endisset
          @isset($font->subsets)
          <div class="col-12 col-lg-auto font-subsets d-none collapse hidden" hidden>
            <span class="text-right-lg">
              <abbr class="d-none d-lg-block" title="{{ ucwords( implode( ', ', $font->subsets ) ) }}" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(10/1000));">Subsets</abbr>
              <span class="d-block d-lg-none" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(40/1000));">{{ ucwords( implode( ', ', $font->subsets ) ) }}</span>
            </span>
          </div>
          @endisset
        </div>

      </div>
    </div>

  @if( null != $font->variants && count( $font->variants ) > 0 )
    <select class="order-last mb-lg-2 mb-3" onchange="generic( this, this.value );">
      <option value="NULL">Select Variant</option>
    @foreach( $font->variants as $key=>$variant )
      <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
    @endforeach
    </select>
  @endif

  </section>

  <section class="card-text font-text-example ellipsis" style="font-size:1rem;font-family:{{ $font->family }},{{ $font->category }};font-display:swap;">
    <div class="wrapper" style="padding:10px 0;">
      <p style="font-size:inherit;font-family:inherit;">{{ $arb_sentence }}</p>
    </div>
  </section>

  <div class="d-flex flex-row flex-wrap" style="max-height:30px;">
    <div class="col-auto mx-lg-3">
      <label for="title_font-{{ $font->id }}" class="d-flex">
        <input id="title_font-{{ $font->id }}" class="form-control col-lg-auto col" type="radio" name="title_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
        <span style="line-height:2;font-size:15px;font-weight:400;">Title</span>
      </label>
    </div>
    <div class="col-auto mx-lg-3">
      <label for="body_font-{{ $font->id }}" class="d-flex">
        <input id="body_font-{{ $font->id }}" class="form-control col-lg-auto col" type="radio" name="body_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
        <span style="line-height:2;font-size:15px;font-weight:400;">Body</span>
      </label>
    </div>
    <div class="col-auto mx-lg-3">
      <label for="cta_font-{{ $font->id }}" class="d-flex">
        <input id="cta_font-{{ $font->id }}" class="form-control col-lg-auto col" type="radio" name="cta_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
        <span style="line-height:2;font-size:15px;font-weight:400;">CTA</span>
      </label>
    </div>
  </div>

</article>
@endisset
