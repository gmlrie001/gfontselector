{!! Form::label($name, $label) !!}
{!! Form::text($name, null, array('placeholder' => $label, 'class' => 'form-control', 'data-provide' => 'datepicker', 'data-date-format' => 'yyyy/mm/dd')) !!}