{!! Form::label($name, $label) !!}
{!! Form::text($name, null, array('placeholder' => $label, 'class' => 'form-control '.$name)) !!}

<script>
$('.{{$name}}').selectize({
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});
</script>