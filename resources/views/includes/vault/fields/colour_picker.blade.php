{!! Form::label($name, $label) !!}
{!! Form::text($name, null, array('placeholder' => $label, 'class' => 'form-control '.$name)) !!}
<script>
    $(".{{$name}}").ColorPickerSliders({
        color: "{{$value}}",
        flat: true,
        sliders: false,
        swatches: false,
        hsvpanel: true
    });
</script>