@php
    $banner = \App\Models\PageBanner::find(Session::get('relation_id'));
    $bannerColumns = $banner->max_columns;
    if($bannerColumns == "3"){
        $options = ['1' => '1 Column', '2' => '2 Columns', '3' => '3 Columns'];
    }elseif($bannerColumns == "2"){
        $options = ['1' => '1 Column', '2' => '2 Columns'];
    }else{
        $options = ['1' => '1 Column'];
    }
@endphp
{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control')) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>