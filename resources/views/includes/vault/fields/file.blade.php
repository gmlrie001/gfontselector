<style id="uploaded_file">
    .attached_file [class*=fa-file-] {
        font-size: 3rem;
        line-height: 2rem;
        width: 30px;
        height: 30px;
        color: #000000;
        padding-top: 2rem;
        margin-bottom: 2rem;
    }
    .attached_file .fa-file-pdf {
        color: #ff0000;
    }
    attached_file .fa-file-word {
        color: #2b579a;
    }
</style>

<span class="small">{{$label}}</span>
<p class="small">Max File Size: 2MB</p>
@if($value != null)
  <p class="small">Current File: {{ $value }}</p>
  @php
    $file_type   = pathinfo( $value )['extension'];
    $is_word_doc = in_array( $file_type, ['doc','docx'] );
    if ( $is_word_doc ) $file_type = 'word';
  @endphp
  <span class="attached_file d-block my-5">
    <i class="fa fa-file-{{ $file_type }}"></i>
  </span>
@else
<label class="upload-button">
    <i class="fa fa-upload" aria-hidden="true"></i> Upload file
    {!! Form::file($name."_upload", ['id' => $name."_upload"]) !!}
    {!! Form::text($name, null, array('class' => 'hidden', 'id' => 'myOutput_'.$name)) !!}
</label>
@endif
@if($method == 'update' && $can_remove == true)
    <div class="col-xs-12 padding-0">
        <a href="/vault/image/remove/{{$table}}/{{$name}}/{{$entry->id}}" class="remove-image"><i class="fa fa-trash"></i> Remove file</a>
    </div>
@endif
<script>
    $('{{"#".$name."_upload"}}').bind('change', function() {
        if(this.files[0].size/1024 > 1999) {
            $('{{"#".$name."_upload"}}').replaceWith($('{{"#".$name."_upload"}}').clone());
            
            $.notify("Files cannot exceed 2MB!", {
                autoHide: false, 
                position: 'right bottom', 
                className: 'error', 
                gap: 5
            });
        }else{
            var image = this.files[0];
            var form = new FormData();
            form.append('image', image);
            form.append("_token", $('input[name="_token"]').val());
            
            $.ajax({
                url: '/vault/image/upload/{{$name}}',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response) {
                    $('img.img_preview_{{$name}}').attr('src', '/'+response.location);
                    $("{{'#myOutput_'.$name}}").val(response.location);
                }, 
                error:function(response){
                    $.notify("File could not be uploaded!", {
                        autoHide: false, 
                        position: 'right bottom', 
                        className: 'error', 
                        gap: 5
                    });
                }
            });
        }
    });
</script>