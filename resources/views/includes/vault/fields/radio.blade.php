{!! Form::label($name, $label) !!}
@foreach($options as $option)
    @if($option == $value)
        {!! Form::radio($name, $option, true, ['class' => 'radio_'.$name]); !!}
    @else
        {!! Form::radio($name, $option, false, ['class' => 'radio_'.$name]); !!}
    @endif
<script>
    $('.radio_{{$name}}').iCheck({
        checkboxClass: 'icheckbox_flat',
        radioClass: 'iradio_flat'
    });
</script>