{{-- dd( get_defined_vars(), $name, $label, $options, Request::input( $name ) ) --}}

{{-- Form::label($name, $label) --}}
<!-- <select name="{{ $name }}" class="form-control" value="{{ $name }}">
    <option value="">{{ $label }}</option>
  @foreach( $options as $key => $option )
    <option value="{{ $key }}">{{ $option }}</option>
  @endforeach
</select> -->

{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control')) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>