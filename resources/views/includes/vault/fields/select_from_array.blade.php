{!! Form::label($name, $label) !!}
{!! Form::select($name, $options, null, array('placeholder' => $label, 'class' => 'form-control')) !!}

<script>
    $('#{{$name}}').ddslick();    
</script>