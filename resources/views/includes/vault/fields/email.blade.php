{!! Form::label($name, $label) !!}
{!! Form::email($name, null, array('placeholder' => $label, 'class' => 'form-control')) !!}