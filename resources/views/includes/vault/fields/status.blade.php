{!! Form::label($name, $label) !!}
{!! Form::select($name, Config::get('enums.statuses'), null, array('class' => 'form-control')) !!}
<div class="col-xs-12 padding-0 status-date">
    {!! Form::label('status_date', 'Scheduled For') !!}
    {!! Form::text('status_date', null, array('placeholder' => 'Scheduled For', 'class' => 'form-control date-time')) !!}
</div>
<script>
    $('.date-time').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
    $("select[name='{{$name}}']").change(function(){
        if($(this).val() == "SCHEDULED"){
            $(".status-date").slideDown(500);
        }else{
            $(".status-date").slideUp(500);
        }
    });
    if($("select[name='{{$name}}']").val() == "SCHEDULED"){
        $(".status-date").slideDown(500);
    }else{
        $(".status-date").slideUp(500);
    }
</script>