@if( ! Request::is('/') )

<section class="container-fluid px-0 position-relative page-banner">
    <div class="container">
        <div class="pr-3 position-absolute banner-text">

	    @isset( $page_title_icon )
            <img class="img-fluid d-inline" src="{{ $page_title_icon }}" alt="what we do">
	    @endisset

	    @isset( $page_title )
            <h1 class="d-inline">{{ $page_title }}</h1>
	    @endisset

        </div>
    </div>
</section>

@endif