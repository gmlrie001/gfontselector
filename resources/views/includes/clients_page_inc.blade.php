<section class="container client-module-container" style="margin-top:5rem;">

    <div class="row home-title">

        <h1 class="col-12">{{$clients->module_title}}</h1>

    </div>
    <div class="col-12">
        <div class="client-slider">

            @foreach( $clients as $client )

            <div class="col">

                <img class="img-fluid" src="/{{ $client->gallery_image }}" alt="{{ $client->title }}">

            </div>

            @endforeach

        </div>
    </div>
</section>



