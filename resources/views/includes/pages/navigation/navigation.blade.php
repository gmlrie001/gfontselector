@push( 'pageStyles' )
@endpush

<!-- DESKTOP -->
<div class="container-fluid sticky-header top-full d-none d-lg-block" data-is-sticky="false" data-sticky-momentum="stationary">
  <nav class="navbar navbar-expand-xl navbar-light px-3 pt-1 d-flex flex-xl-row flex-column" id="navbarNavDesktop">
    <a class="navbar-brand px-3 pt-2" href="/">
      <picture>
        <source srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}" media="(max-width:1280px)">
        <source srcset="/{{ ltrim( $site_settings->logo, '/' ) }}">
        <img class="img-fluid desktop-logo" alt="Construkt" src="/{{ ltrim( $site_settings->logo, '/' ) }}">
      </picture>
    </a>
    <div class="navbar-collapse p-3">
      <div class="d-flex flex-row mt-0 justify-content-center ml-auto">
        <a class="nav-item nav-link @if(Request::is('/')) active @endif" href="/">Home</a>
        <a class="nav-item nav-link @if(Request::is('about*')) active @endif" href="/about">About Us</a>
        <a class="nav-item nav-link @if(Request::is('services*')) active @endif" href="/services">Services</a>
        <a class="nav-item nav-link @if(Request::is('our-process*')) active @endif" href="/our-process">Our Process</a>
        <a class="nav-item nav-link @if(Request::is('projects*')) active @endif" href="/projects">Projects</a>
        <a class="nav-item nav-link @if(Request::is('gallery*')) active @endif" href="/gallery">Gallery</a>
        <a class="nav-item nav-link @if(Request::is('news*')) active @endif" href="/news">News</a>
        <a class="nav-item nav-link @if(Request::is('referrals*')) active @endif" href="/referrals">Referrals</a>
        <a class="nav-item nav-link @if(Request::is('contact*')) active @endif" href="/contact">Contact Us</a>
      </div>
    </div>
  </nav>
</div>

<!-- MOBILE -->
<div class="container-fluid px-0 sticky-header top-full d-block d-lg-none" data-is-sticky="false" data-sticky-momentum="stationary" style="top:0!important;">
  <nav class="navbar navbar-expand-xl navbar-light px-0">
    <a class="navbar-brand px-4 pt-0" href="/">
      <picture>
        <source srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}" media="(max-width:980px)">
        <source srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
        <img class="img-fluid desktop-logo" alt="Construkt" src="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
      </picture>
    </a>
    <button class="navbar-toggler px-4 border-none" type="button" aria-expanded="false">
      <img class="img-fluid img-navicon nav-open" alt="Navigation Open" src="/assets/images/menu-nav-icon.png">
    </button>
    <div class="collapse navbar-collapse mt-n3 px-3" id="navbarNavMobile">
      <div class="d-flex flex-row justify-content-between">
        <a class="navbar-brand px-2" href="/">
          <picture>
            <source srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}" media="(max-width:980px)">
            <source srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
            <img class="img-fluid desktop-logo" alt="Construkt" src="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
          </picture>
        </a>
        <button class="navbar-toggler pt-2 flex-center border-none" type="button" aria-expanded="true">
          <img class="img-fluid img-navicon nav-close" alt="Navigation Close" src="/assets/images/menu-close-icon.png">
        </button>
      </div>
      <div class="d-flex flex-xl-row flex-column ml-auto mt-0 pt-4 pt-sm-3">
        <a class="nav-item nav-link @if(Request::is('/')) active @endif" href="/">Home</a>
        <a class="nav-item nav-link @if(Request::is('about*')) active @endif" href="/about">About Us</a>
        <a class="nav-item nav-link @if(Request::is('services*')) active @endif" href="/services">Services</a>
        <a class="nav-item nav-link @if(Request::is('our-process*')) active @endif" href="/our-process">Our Process</a>
        <a class="nav-item nav-link @if(Request::is('projects*')) active @endif" href="/projects">Projects</a>
        <a class="nav-item nav-link @if(Request::is('gallery*')) active @endif" href="/gallery">Gallery</a>
        <a class="nav-item nav-link @if(Request::is('news*')) active @endif" href="/news">News</a>
        <a class="nav-item nav-link @if(Request::is('referrals*')) active @endif" href="/referrals">Referrals</a>
        <a class="nav-item nav-link @if(Request::is('contact*')) active @endif" href="/contact">Contact Us</a>
      </div>
      <div class="col-12 col-lg-6 footer-branding-social-media my-3 d-block d-xl-none">
        <ul class="social-links">
          <li>
            <a class="icons" href="#" title="Connect via Facebook" target="_blank">
              <i class="fa fa-facebook-f"></i>
            </a>
          </li>
          <li>
            <a class="icons" href="#" title="Connect via LinkedIn" target="_blank">
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
