<style>
  .bg-black {
    background-color: #000000;
  }
  .nav-item.active {
    color: #ff6f00;
  }
  section.faqs-content-tab {
    padding-top: 0;
  }
  @media only screen and (max-width: 768px) {
    .mfp-container {
      height: 100vh !important;
    }
  }
</style>

<header class="container-fluid template-default z-top">
  <div class="container">
    <div class="row">
      <hgroup class="col-5 col-lg-3 branding d-none d-lg-block">
        <h1 class="text-hide">
          <a href="/">
            <picture>
              <source media="(min-width: 992px)" srcset="/{{ ltrim( $site_settings->logo, '/' ) }}">
              <source media="(max-width: 991px)" srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
              <img class="img-fluid bg-black" src="/{{ ltrim( $site_settings->logo, '/' ) }}" alt="{{ $site_settings->site_name }}">
            </picture>
            {{ $site_settings->site_name }}
          </a>
        </h1>
      </hgroup>
      <!-- DESKTOP Navigation Container and Navigation Items -->
      <nav class="col-7 col-lg navbar navbar-expand-sm clearfix desktop-nav d-none d-lg-block px-3">
        <ul class="navbar-nav ml-auto">
          <li class="@if( Request::is('/') ) active @endif nav-item">
            <a class="nav-link" href="/">Home</a>
          </li>
          <li class="@if( Request::is('about') || Request::is('about/*') ) active @endif nav-item">
            <a class="nav-link" href="/about">About</a>
          </li>
          <li class="@if( Request::is('blog') ) active @endif nav-item">
            <a class="nav-link" href="/blog">Blog</a>
          </li>
          <li class="@if( Request::is('faqs/*') ) active @endif nav-item">
            <a class="nav-link" href="/faqs">Faqs</a>
          </li>
          <li class="@if( Request::is('gallery') || Request::is('gallery/*') ) active @endif nav-item">
            <a class="nav-link" href="/gallery">Gallery</a>
          </li>
          <li class="@if( Request::is('rentals') || Request::is('rentals/*') ) active @endif nav-item">
            <a class="nav-link" href="/rentals">Rentals</a>
          </li>
          <li class="@if( Request::is('events') || Request::is('events/*') ) active @endif nav-item">
            <a class="nav-link" href="/events">Events</a>
          </li>
          <li class="@if( Request::is('news') || Request::is('news/*') ) active @endif nav-item">
            <a class="nav-link" href="/news">News</a>
          </li>
          <li class="@if( Request::is('tenant-directory') || Request::is('tenant-directory/*') ) active @endif nav-item">
            <a class="nav-link" href="/tenant-directory">Tenants<span class="collapse"> Directory</span></a>
          </li>
          <li class="@if( Request::is('recipes') || Request::is('recipes/*') ) active @endif nav-item">
            <a class="nav-link" href="/recipes">Recipes</a>
          </li>
          <li class="@if( Request::is('body-corporate') || Request::is('body-corporate/*') ) active @endif nav-item">
            <a class="nav-link" href="/body-corporate">Body Corp</a>
          </li>
          <li class="@if( Request::is('contact') ) active @endif nav-item">
            <a class="nav-link" href="/contact">Contact</a>
          </li>
          <li class="nav-item pr-0" style="align-self:center;">
            <a class="nav-link share" href="#">
              <img class="img-fluid share-icon" src="/assets/images/helpers/icons/share.svg" alt="Click/Tap for Social Media and Sharing" style="max-width:85%;">
            </a>
          </li>
        </ul>
      </nav>
      <!-- MOBILE Navigation Container -->
      <nav class="col-12 navbar navbar-expand-md responsive-nav justify-content-between text-center px-5">
        <img class="img-fluid share-icon" src="/assets/images/helpers/icons/share.svg" alt="Click/Tap for Social Media and Sharing">
        <hgroup class="col-5 col-lg-auto branding">
          <h1 class="text-hide">
            <a href="/">
              <picture>
                <source media="(min-width: 992px)" srcset="/{{ ltrim( $site_settings->logo, '/' ) }}">
                <source media="(max-width: 991px)" srcset="/{{ ltrim( $site_settings->logo_mobile, '/' ) }}">
                <img class="img-fluid bg-black" src="/{{ ltrim( $site_settings->logo, '/' ) }}" alt="{{ $site_settings->site_name }}">
              </picture>
              {{ $site_settings->site_name }}
            </a>
          </h1>
        </hgroup>
        <button class="navbar-toggler collapse show h-lg-100 h-auto" type="image" id="mobile-humburger">
          <img class="img-fluid" src="/assets/images/helpers/icons/navicon.svg" alt="Mobile Navigation Toggler -- Open">
        </button>
      </nav>
    </div>
  </div>
</header>

<!-- MOBILE Navigation Items (divorced from container) -->
<div class="mobile-nav-overlay collapse"></div>
<div class="mobile-navigation collapse p-3" id="navbarMobile">
  <div class="container">
    <div class="row">
      <button class="navbar-toggler collapse clearfix col-auto ml-auto" type="image" id="mobile-x">
        <img class="img-fluid nav-close" src="/assets/images/helpers/icons/close.png" alt="Mobile Navigation Close">
      </button>
    </div>
    <ul class="navbar-nav mobile-navbar-nav">
      <li class="@if( Request::is('/') ) active @endif nav-item">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="@if( Request::is('about') || Request::is('about/*') ) active @endif nav-item">
        <a class="nav-link" href="/about">About</a>
      </li>
      <li class="@if( Request::is('blog/*') ) active @endif nav-item">
        <a class="nav-link" href="/blog">Blog</a>
      </li>
      <li class="@if( Request::is('faqs/*') ) active @endif nav-item">
          <a class="nav-link" href="/faqs">Faqs</a>
      </li>
      <li class="@if( Request::is('gallery') || Request::is('gallery/*') ) active @endif nav-item">
        <a class="nav-link" href="/gallery">Gallery</a>
      </li>
      <li class="@if( Request::is('body-corporate') || Request::is('body-corporate/*') ) active @endif nav-item">
        <a class="nav-link" href="/body-corporate">Body Corp</a>
      </li>
      <li class="@if( Request::is('contact') ) active @endif nav-item">
        <a class="nav-link" href="/contact">Contact</a>
      </li>
      <li class="nav-item d-none">
        <a class="nav-link share" href="#">
          <img class="img-fluid share-icon" src="/assets/images/helpers/icons/share.svg" alt="Click/Tap for Social Media and Sharing">
        </a>
      </li>
    </ul>
    <div class="col-12 px-4 pt-3">
      <div class="row get-in-touch px-md-3">
        <div class="col-12">
          <h2>Get In Touch</h2>
        </div>
        <a href="tel:+27 (21) 510 8339" class="col-12">T: +27 (21) 510 8339</a>
        <a href="fax:+27 (21) 510 0809" class="col-12">F: +27 (21) 510 0809</a>
      </div>
    </div>
    <div class="col-12 col-lg-6 px-4 mt-md-0 mt-sm-n5">
      <div class="col-12 px-md-0 pt-md-3">
        <div class="row follow-us">
          <div class="col-auto follow facebook">
            <a href="#"><i class="fa fa-facebook-f"></i></a>
          </div>
          <div class="col-auto follow twitter">
            <a href="#"><i class="fa fa-twitter"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
