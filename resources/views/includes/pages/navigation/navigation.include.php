<style>
  .desktop-logo {
    max-width: 120px;
  }
  @media only screen and (max-width: 768px) {
    .desktop-logo,
    .mobile-logo {
      max-width: 50px;
    }
  }
</style>

<div class="container-fluid sticky-header top-full" data-is-sticky="false" data-sticky-momentum="stationary">
  <nav class="navbar navbar-expand-xl navbar-light px-3 pt-0 pt-md-3" id="navbarNavDesktop">
    <a class="navbar-brand px-3" href="/">
      <picture>
        <source srcset="/assets/images/buld-logo.png" media="(max-width:1280px)">
        <source srcset="/assets/images/buld-logo.png">
        <img class="img-fluid desktop-logo" alt="Construkt" src="/assets/images/buld-logo.png">
      </picture>
    </a>
    <button class="navbar-toggler px-3 border-none" type="button" 
      data-toggle="collapse" 
      data-target="#navbarNavMobile" 
      aria-controls="navbarNavMobile" 
      aria-expanded="false" 
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon collapse"></span>
      <img class="img-fluid img-navicon nav-open" alt="Navigation Open" src="/assets/images/menu-nav-icon.png">
      <img class="img-fluid img-navicon nav-close" alt="Navigation Close" src="/assets/images/menu-close-icon.png">
    </button>
    <div class="collapse navbar-collapse px-3">
      <div class="d-flex flex-xl-row flex-column mt-0 mt-md-3 justify-content-center ml-auto">
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/' . 'index.php'):?>active<?php endif;?>" href="/">Home <span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/about' || $_SERVER['REQUEST_URI'] == '/about' . 'index.php'):?>active<?php endif;?>" href="/about">About Us</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/services' || $_SERVER['REQUEST_URI'] == '/services' . 'index.php'):?>active<?php endif;?>" href="/services">Services</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/our-process' || $_SERVER['REQUEST_URI'] == '/our-process' . 'index.php'):?>active<?php endif;?>" href="/our-process">Our Process</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/projects' || $_SERVER['REQUEST_URI'] == '/projects' . 'index.php'):?>active<?php endif;?>" href="/projects">Projects</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/gallery' || $_SERVER['REQUEST_URI'] == '/gallery' . 'index.php'):?>active<?php endif;?>" href="/gallery">Gallery</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/news' || $_SERVER['REQUEST_URI'] == '/news' . 'index.php'):?>active<?php endif;?>" href="/news">News</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/referrals' || $_SERVER['REQUEST_URI'] == '/referrals' . 'index.php'):?>active<?php endif;?>" href="/referrals">Referrals</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/contact' || $_SERVER['REQUEST_URI'] == '/contact' . 'index.php'):?>active<?php endif;?>" href="/contact">Contact Us</a>
      </div>
      <div class="col-12 col-lg-6 footer-branding-social-media py-2 d-block d-xl-none">
        <ul class="social-links mt-2 mb-0">
          <li>
            <a class="icons" href="#" title="Connect via Facebook" target="_blank">
              <i class="fa fa-facebook-f"></i>
            </a>
          </li>
          <li>
            <a class="icons" href="#" title="Connect via LinkedIn" target="_blank">
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="d-block d-md-block debug-toggle-switch position-absolute collapse" style="top:50%;right:0;">
      <button class="pb-1 collapse" onclick="switchon_debugging();">Debug: ON</button>
      <button class="pb-1 collapse" onclick="switchoff_debugging();">Debug: OFF</button>
    </div>
  </nav>

  <nav class="navbar navbar-expand-xl navbar-light px-3 pt-0 pt-md-3 collapse" id="navbarNavMobile">
    <a class="navbar-brand px-3" href="/">
        <picture>
          <source srcset="/assets/images/buld-logo.png" media="(max-width:1280px)">
          <source srcset="/assets/images/buld-logo.png">
          <img class="img-fluid mobile-logo" alt="Construkt" src="/assets/images/buld-logo.png">
        </picture>
    </a>
    <button class="navbar-toggler px-3 border-none" type="button" 
        data-toggle="collapse" 
        data-target="#navbarNavMobile" 
        aria-controls="navbarNavMobile" 
        aria-expanded="false" 
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon collapse"></span>
        <img class="img-fluid img-navicon nav-open" alt="Navigation Open" src="/assets/images/menu-nav-icon.png">
        <img class="img-fluid img-navicon nav-close" alt="Navigation Close" src="/assets/images/menu-close-icon.png">
    </button>
    <div class="navbar-collapse px-3">
      <div class="d-flex flex-xl-row flex-column mt-2 justify-content-center ml-auto">
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/' . 'index.php'):?>active<?php endif;?>" href="/">Home <span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/about' || $_SERVER['REQUEST_URI'] == '/about' . 'index.php'):?>active<?php endif;?>" href="/about">About Us</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/services' || $_SERVER['REQUEST_URI'] == '/services' . 'index.php'):?>active<?php endif;?>" href="/services">Services</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/our-process' || $_SERVER['REQUEST_URI'] == '/our-process' . 'index.php'):?>active<?php endif;?>" href="/our-process">Our Process</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/projects' || $_SERVER['REQUEST_URI'] == '/projects' . 'index.php'):?>active<?php endif;?>" href="/projects">Projects</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/gallery' || $_SERVER['REQUEST_URI'] == '/gallery' . 'index.php'):?>active<?php endif;?>" href="/gallery">Gallery</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/news' || $_SERVER['REQUEST_URI'] == '/news' . 'index.php'):?>active<?php endif;?>" href="/news">News</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/referrals' || $_SERVER['REQUEST_URI'] == '/referrals' . 'index.php'):?>active<?php endif;?>" href="/referrals">Referrals</a>
        <a class="nav-item nav-link <?php if ($_SERVER['REQUEST_URI'] == '/contact' || $_SERVER['REQUEST_URI'] == '/contact' . 'index.php'):?>active<?php endif;?>" href="/contact">Contact Us</a>
      </div>
      <div class="col-12 col-lg-6 footer-branding-social-media py-2 d-block d-xl-none">
        <ul class="social-links mt-2 mb-0">
          <li>
            <a class="icons" href="#" title="Connect via Facebook" target="_blank">
              <i class="fa fa-facebook-f"></i>
            </a>
          </li>
          <li>
            <a class="icons" href="#" title="Connect via LinkedIn" target="_blank">
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

</div>
