<div class="row row-subscribe">

    <section class="subscribe col-12 cc p-0">

        <div class="container">
            <div class="row">

                <div class="col-12 col-lg-8">
                    <h3>Keep up to date with our latest news, updates and workshops.</h3>
                </div>

                <div class="col-12 col-lg-4">
                    {!! Form::open(['id'=>'subscribe', 'url' => '/signup']) !!}
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Email Address" id="subscribe" name="email" required="">
                            <div class="input-group-btn">
                                <button class="btn btn-default btn-subscribe" type="submit">Subscribe</button>
                            </div>
                        </div>
                    {!! Form::open() !!}
                </div>

            </div>
        </div>

    </section>

</div><!-- / .row-subscribe / -->