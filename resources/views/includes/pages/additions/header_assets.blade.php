    <link rel="icon" type="image/ico" href="/favicon.ico">
    
    <!-- Font-Loading -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&display=swap" rel="preload" as="style" onload="this.rel='stylesheet';" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" integrity="sha256-etrwgFLGpqD4oNAFW08ZH9Bzif5ByXK2lXNHKy7LQGo=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" integrity="sha256-PZLhE6wwMbg4AB3d35ZdBF9HD/dI/y4RazA3iRDurss=" crossorigin="anonymous" />

    <!--
    <link rel="stylesheet" href="/assets/css/sites/granddomaine.css" />
    <link rel="stylesheet" href="/assets/css/features/navigation.css" />
    <link rel="stylesheet" href="/assets/css/features/banner.css" />
    <link rel="stylesheet" href="/assets/css/features/sharemodal.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/wines.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/gallery.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/footer.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/contact.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/heritage.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/vineyards.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/news.css" media="print" onload="this.media='all';" />
    <link rel="stylesheet" href="/assets/css/features/distributions.css" media="print" onload="this.media='all';" />
    -->

    <title>Grand Domaine</title>

    <!-- <script async defer src="https://kit.fontawesome.com/f7c1d56625.js"></script> -->
    