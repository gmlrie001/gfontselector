@if($page->featured_image != null)
    <div class="row">
        <section class="site-header-hero">
            <div class="jumbotron {{$body_class}}">
                <picture>
                    <source media="(max-width:567px)" srcset="/{{$page->mobile_banner}}">
                    <img class="img-fluid" src="/{{$page->featured_image}}" alt="{{$page->title}}">
                </picture>
            </div>
        </section>
    </div> 
@endif