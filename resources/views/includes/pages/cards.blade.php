@extends( 'layouts.vault' )

@section( 'content' )

<form action="/vault/site_fonts/edit/1" method="POST">
<div id="googleFonts" class="frontend-font-selector px-3">

<section class="custom-container my-lg-3 my-2"><!-- container-fluid -->
  <div class="row">
    <div class="col-12 col-lg page-title">
      <h1>Google Fonts Selector</h1>
    </div>
    <div class="col-12 col-lg-4 d-flex flex-lg-row flex-column ml-auto">
      <h2>
        <small>Update Google Fonts</small>
      </h2>
      <a title="Update Google Fonts" class="cta btn px-lg-5 mx-lg-4 mx-3" href="/site_fonts/update">Update</a>
    </div>
  </div>
</section>

<div class="custom-container my-lg-3 my-0"><!-- container-fluid -->
  <section class="my-lg-3 my-2">
    <div class="row">
      <div class="col-12 col-lg-6 mb-lg-2 my-3">
        <form action="/site_fonts/search" method="POST">
          @csrf()
          <div class="d-block col-12 px-lg-0">
            <h4>Search:</h4>
          </div>
          <label for="font-search" class="input-group-append">
            <input name="query" id="font-search" type="search" class="form-control" value="" placeholder="Search font-family ..." style="border-radius:0;"><!-- onkeyup="live_search()"-->
            <button type="submit" style="all: unset;box-shadow: unset !important;">
              <i class="fa fa-search" style="line-height:28px;width:34px;background-color:#495057;color:#fefefe;border:1px solid #ced4da;text-align:center;"></i>
            </button>
          </label>
        </form>
      </div>
      <div class="col-12 col-lg-6 my-3 text-right">
        <div class="d-block col-12 px-lg-0">
          <h4>Filter/Ordering:</h4>
        </div>
        <select id="ordering" class="form-control ordering-filter" data-base-href="site_fonts/orderBy">
          <option value="NULL">Select Ordering...</option>
          <option value="family/asc">Font Family Name ASCending</option>
          <option value="family/desc">Font Family Name DESCending</option>
          <option value="lastModified/asc">Last Modified ASCending</option>
          <option value="lastModified/desc">Last Modified DESCending</option>
          <option value="version/asc">Version ASCending</option>
          <option value="version/desc">Version DESCending</option>
        </select>
      </div>
    </div>
  </section>
</div>

<div class="col d-block d-lg-none">
  <button class="mobile-modal-activate">Show Filters</button>
</div>

<section class="vault-google-fonts-grid">
  <div class="custom-container filters-container my-lg-3 my-0 d-none d-lg-block">
    <div class="row mx-lg-3">
      <div class="col-12 mt-lg-0 my-3 px-lg-0 px-3">
        <form action="/site_fonts/filter" method="POST">
          @csrf()
          <div class="d-block col-12 px-lg-0 px-3">
            <h4>Category:</h4>
          </div>
          <label for="font-category" class="w-100" style="width: 100% !important;">
            <select name="category" id="font-category" class="form-control border-0">
              <option value="NULL">Filter by Category...</option>
              @forelse($categories as $key=>$category)
              <option value="{{ $category }}">{{ ucwords( $category ) }}</option>
              @empty
              @endforelse
            </select>
          </label>
          <div class="form-group my-lg-3 my-2">
            <div class="d-block col-12 px-lg-0 px-3">
              <h4>Variants:</h4>
            </div>
            {{--
            @forelse($variants as $key=>$variant)
            <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
              <input name="variants[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $variant }}">
              <span hidden>{{ ucwords( $variant ) }}</span>
            </label>
            @empty
            @endforelse
            --}}
            <select name="variants[]" id="font-variant" class="form-control" style="border-radius:0;" multiple>
            <!-- onchange="selected()"-->
              @forelse($variants as $key=>$variant)
              <option value="{{ $variant }}" style="font-weight:125%;line-height:1.618;">{{ ucwords( $variant ) }}</option>
              @empty
              @endforelse
            </select>
          </div>
          <div class="form-group my-lg-3 my-2">
            <div class="d-block col-12 px-lg-0 px-3">
              <h4>Subsets:</h4>
            </div>
            {{--
            @forelse($subsets as $key=>$subset)
            <label class="col-auto d-flex flex-row mx-0 mb-lg-1 px-lg-0 px-3 hidden">
              <input name="subsets[]" type="checkbox" class="form-control mr-lg-3 mr-2 w-auto hidden" value="{{ $subset }}">
              <span hidden>{{ ucwords( $subset ) }}</span>
            </label>
            @empty
            @endforelse
            --}}
            <select name="subsets[]" id="font-subset" class="form-control" style="border-radius:0;" multiple>
            <!-- onchange="selected()"-->
              @forelse($subsets as $key=>$subset)
              <option value="{{ $subset }}" style="font-weight:125%;line-height:1.618;">{{ ucwords( $subset ) }}</option>
              @empty
              @endforelse
            </select>
          </div>
          <div class="form-row" style="display:flex;justify-content:space-evenly;flex-wrap:wrap;">
            <button class="col-11 col-lg-11" type="submit">Filter</button>
            <button class="col-5 col-lg-5" type="reset">Reset</button>
            <button class="col-5 col-lg-5" type="button" onclick="window.location.href='/site_fonts';">Clear All</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="custom-container fonts-container my-lg-3 my-4">
    <section class="row cardcols my-lg-3 my-2 mx-lg-3 mx-auto" id="myUL">
      @forelse( $font_db as $key=>$font )
      <article class="col-lg-auto col-12 google_fonts card pb-lg-0 @if ( $key > 11 ) hidden @endif" data-font-id="{{ $font->id }}" data-family="{{ str_replace( ' ', '+', $font->family ) }}" data-category="{{ $font->category }}" data-version="{{ $font->version }}" data-variants="{{ implode(",", $font->variants) }}">
        <section class="card-body category-{{ $font->category }}" style="font-family:'Roboto',sans-serif;border:1px solid #eaeaea;padding:10px 25px;border-radius:1rem;">
          <div class="card-title mb-lg-0 d-flex flex-lg-column flex-row flex-wrap justify-content-between ellipsis">
            <div class="row">
              <div class="col-12 col-lg font-title">
                <h3 class="card-title mb-0">{{ ucwords( $font->family ) }}</h3>
                <h5>Font has {{ count( $font->variants ) ?? 0 }} variants & {{ count($font->subsets) ?? 0 }} subsets</h5>
              </div>
              <div class="d-none collapse hidden" hidden>
                @isset($font->version)
                <div class="col-12 col-lg-auto font-version d-none collapse hidden" hidden>
                  <div class="text-center versioned" style="width:25px;height:25px;padding:0;border:1px solid #000;border-radius:50%;line-height:25px;">
                    <span style="font-family:fantasy!important;font-weight:300!important;font-size:13px!important;">{{ $font->version }}</span>
                  </div>
                </div>
                @endisset
                @isset($font->subsets)
                <div class="col-12 col-lg-auto font-subsets d-none collapse hidden" hidden>
                  <span class="text-right-lg">
                    <abbr class="d-none d-lg-block" title="{{ ucwords( implode( ', ', $font->subsets ) ) }}" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(10/1000));">Subsets</abbr>
                    <span class="d-block d-lg-none" style="font-family:fantasy!important;font-weight:300!important;letter-spacing:calc(1em*(40/1000));">{{ ucwords( implode( ', ', $font->subsets ) ) }}</span>
                  </span>
                </div>
                @endisset
              </div>
            </div>
          </div>

          @if( null != $font->variants && count( $font->variants ) > 0 )
          <select class="order-last mb-lg-2 mb-3" onchange="font_variant( this, this.value );">
            <option value="NULL">Select Variant</option>
            @foreach( $font->variants as $key=>$variant )
            <option value="{{ $variant }}">{{ ucwords( $variant ) }}</option>
            @endforeach
          </select>
          @endif

          <section class="card-text font-text-example ellipsis" style="font-size:1rem;font-family:{{ $font->family }},{{ $font->category }};font-display:swap;">
            <div class="wrapper" style="padding:10px 0;">
              <p style="font-size:inherit;font-family:inherit;">The quick brown fox jumps over the lazy dog.</p>
            </div>
          </section>

          <div class="d-flex flex-row flex-wrap">
            <div class="col-auto mx-lg-3">
              <label for="title_font" class="d-flex">
                <input id="title_font" class="form-control col-lg-auto col" type="radio" name="title_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span>Title</span>
              </label>
            </div>
            <div class="col-auto mx-lg-3">
              <label for="body_font" class="d-flex">
                <input id="body_font" class="form-control col-lg-auto col" type="radio" name="body_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span>Body</span>
              </label>
            </div>
            <div class="col-auto mx-lg-3">
              <label for="cta_font" class="d-flex">
                <input id="cta_font" class="form-control col-lg-auto col" type="radio" name="cta_font" value="{{ str_replace( ' ', '+', $font->family ) }}">
                <span>CTA</span>
              </label>
            </div>
          </div>
          
        </section>
      </article>
      @empty
      <div class="px-5">
        <h2>No results found!</h2>
        <a class="clear-filters" href="/parse_google_fonts_file">
          <span class="mr-3">&times;</span> Clear All Filters
        </a>
      </div>
      @endforelse
      @if ( null != $font_db->links() )
      <div class="container-fluid my-lg-3 my-5" style="overflow:hidden;margin-left:1rem;margin-right:1rem;">
        <div class="row">
          <?php echo($font_db->links()); ?>
        </div>
      </div>
      @endif
    </section>

  </div>
  <div class="d-none collapse hidden" hidden>
    {!! Honeypot::generate( 'my_name', 'my_time' ) !!}
  </div>
  <input class="btn btn-success btn-lg btn-block edit-btn" type="submit" value="update" style="margin-bottom:10rem;margin-left:1rem;margin-right:1rem;">
</form>
</section>

<section id="filter-modal" class="collapse d-lg-none">
  <div id="dismiss-modal" class="">
    <span class="close-modal">&times;</span>
  </div>
</section>

</div>
@endsection

@push( 'pageScripts' )
<script>
var font_loader = (function(d=document, t='link'){
  var e = document.querySelector( '.google_fonts:nth-of-type( {{ $key+1 }} )' );
  var b = e.getBoundingClientRect();
  if( b.top > 0 && b.height > 0 ) {
    var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
        gfonts_font_sel = "<?php echo(str_replace(' ', '+', $font->family)); ?>",
        gfonts_font_var = <?php echo('["' . implode('", "', $font->variants) . '"]'); ?>;

    var url_build, g, s;
        url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var.join(',');

    try {
      g = d.createElement(t);
      g.rel = "stylesheet";
      g.href = url_build + '&display=swap';
      s = d.getElementsByTagName(t)[0];
      s.parentNode.insertBefore(g, s);
    } catch( err ) {
      console.clear();
      console.warn( '\r\n' + err + '\r\n' );
    }
  }
});
</script>
<script>
var ordering = document.querySelector('#ordering');
    ordering.addEventListener( 'input', ordering_link, false );

function ordering_link(event) {
  if ( event == undefined || event == null || typeof( event ) != 'object' ) return;

  var evt = event;
  evt.preventDefault();

  var h;
  h = evt.target.getAttribute('data-base-href');

  var v, i, s, t;
  v = evt.target.value;
  i = evt.target.selectedIndex;
  s = evt.target.children[i];
  t = s.innerText;

  var tmp, url;
  tmp = [ '/', h, '/', encodeURI( v ) ];
  url = tmp.join('');

  delete( evt );
  delete( i ); delete( s ); delete( t );
  delete( tmp );

  setTimeout( ()=>{ window.location.href = url; }, 100 );

  return;
}

document.addEventListener(
  'click',
  (evt)=>{
    evt.stopPropagation();
    console.log( evt.target.classList.contains('close-modal') );
    if ( evt.target.classList.contains('close-modal') ) {
      var p = evt.target.parentNode.closest('#filter-modal'),
          r = evt.target.parentNode.nextElementSibling;
      p.classList.remove('show');
      r.parentNode.removeChild(r);
      return;
    } else if ( evt.target.classList.contains('mobile-modal-activate') ) {
      var m = document.querySelector('#filter-modal'),
          f = document.querySelector('.filters-container'),
          c = f.cloneNode(true);
      m.appendChild( c );
      c.classList.remove( 'd-none' );
      c.classList.add('center-in-modal');
      m.classList.add('show');
      return;
    }
  },
  false
);
</script>
<script id="debounce">
const debounce = (func, delay) => {
  let inDebounce;

  return function() {
    const context = this;
    const args = arguments;

    clearTimeout(inDebounce);

    inDebounce = setTimeout( 
      () => func.apply(context, args),
      delay
    );
  }
}
/**
 * USAGE:
 debounceBtn.addEventListener('click', debounce(function() {
  console.info('Hey! It is', new Date().toUTCString());
 }, 3000));
;
 */
</script>
<script id="throttle">
const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;

  return function() {
    const context = this;
    const args = arguments;

    if (!lastRan) {
      func.apply(context, args)
      lastRan = Date.now()
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout( 
        () => {
          if ((Date.now() - lastRan) >= limit) {
            func.apply(context, args)
            lastRan = Date.now()
          }
        },
        limit - (Date.now() - lastRan)
      )
    }
  }
}
/**
 * USAGE:
 throttleBtn.addEventListener('click', throttle(function() {
  return console.log('Hey! It is', new Date().toUTCString());
 }, 1000));
 */
</script>
<script>
var i  = 0;
var o  = 11;
var m  = 1;
var n  = 1;
var es = [].slice.call( document.querySelectorAll( '.card.hidden' ) );
var d  = document;
var t  = 'link';
window.addEventListener( 'scroll', throttle( function() {
  var chk = Math.floor( document.querySelector( '.cardcols' ).clientHeight / 10 ) * 10;
  var offset = window.scrollY + window.innerHeight;
  var height = document.documentElement.offsetHeight;

  if ( offset >= height - 150 ) {
    if( es.length == 0 ) {
      window.removeEventListener( 'scroll', (evt)=>{}, false );
    }
    es.slice(i, o).forEach( ( e ) => {
      e.classList.remove('hidden');
      var font2load = e.children[0].style.fontFamily
                      .split(',')[0]
                      .toString()
                      .replace( /\s+?/ig, '+' )
                      .replace( /\"/ig, '' );
      var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
          gfonts_font_sel = font2load,
          gfonts_font_var = []; //<?php //echo('["' . implode('", "', $font->variants) . '"]');?>;

      var url_build, g, s;
          url_build = gfonts_url_base + '=' + gfonts_font_sel;// + ':' + gfonts_font_var.join(',');

      try {
        g = d.createElement(t),
        g.rel = "stylesheet";
        g.href = url_build + '&display=swap';
        s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s);
      } catch( err ) {
        console.clear();
        console.warn( '\r\n' + err + '\r\n' );
      }
    });
    i += 3 * m + 1;
    o += i + 4;
    m++;
    n += 0.25;
  }
}, 500 ), false);
</script>
<script>
  function live_search() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input  = document.getElementById('font-search');
    filter = input.value.toUpperCase();
    ul     = document.getElementById("myUL");
    li     = ul.getElementsByTagName('article');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i];//.getElementsByTagName("a")[0];
      txtValue = a.getAttribute('data-font-family');//a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display         = "";
        li[i].style.backgroundColor = "#1a1a1a";
        li[i].style.color           = "#fefefe";
        li[i].style.borderColor     = "#fefefe";
        li[i].querySelector( '.cta' ).style.borderColor = "#fefefe";
      } else {
        li[i].style.display         = "none";
      }
    }
  }
  function selected() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input  = document.getElementById('font-category');
    filter = input.value.toUpperCase();
    ul     = document.getElementById("myUL");
    li     = ul.getElementsByTagName('article');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i];//.getElementsByTagName("a")[0];
      txtValue = a.getAttribute('data-category');//a.textContent || a.innerText;
      console.log( txtValue.toUpperCase() );
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display         = "";
        li[i].style.backgroundColor = "#1a1a1a";
        li[i].style.color           = "#fefefe";
        li[i].style.borderColor     = "#fefefe";
        li[i].querySelector( '.cta' ).style.borderColor = "#fefefe";
      } else {
        li[i].style.display         = "none";
      }
    }
  }
  function font_variant(elem, style) {
    var kindaStyle = style.match(/(\d{3}|\w+)/ig);
    var p = elem.parentNode.closest( '.card-body' );
        p.style.fontStyle  = 'unset';
        p.style.fontWeight = 'unset';
    kindaStyle.forEach( ( e ) => {
      if ( isNaN( parseInt( e ) ) ) {
        if (e == 'regular') {
          p.style.fontWeight = 'normal';
        } else {
          p.style.fontStyle  = e;
        }
      } else {
        p.style.fontWeight = parseInt( e );
      }
    });
  }
</script>
@endpush
