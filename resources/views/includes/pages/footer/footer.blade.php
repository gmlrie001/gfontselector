<footer class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-12 d-block d-lg-none mobileShow">
                    <div class="socials text-right">
                      @foreach( $socials as $key=>$social )
                        <a rel="noopener noreferrer" class="fab {{ $social->social_media_icon }}" target="_blank" href="{{ $social->social_media_url }}" title="{{ $social->title }}"></a>
                      @endforeach
                    </div>
                </div>
                <div class="col-12 col-lg-7 nav-links">
                    <div>
                        <p>{{ str_replace( "\r\n", " ", strip_tags( $site_settings->contact_map_address ) ) }}</p>
                        <a href="/privacy">Privacy Policy</a>
                    </div>
                    <p>{{ str_replace( "\r\n", " ", strip_tags( $site_settings->copyright ) ) }}</p>
                </div>
                <div class="col-12 col-lg-5 text-right">
                    <div class="socials text-right">
                      @foreach( $socials as $key=>$social )
                        <a rel="noopener noreferrer" class="fab {{ $social->social_media_icon }}" target="_blank" href="{{ $social->social_media_url }}" title="{{ $social->title }}"></a>
                      @endforeach
                    </div>
                    <a rel="noopener noreferrer" class="" href="https://monzamedia.com" target="_blank">website design :: monzamedia</a>
                </div>
            </div>
        </div>
    </div>
</footer>
