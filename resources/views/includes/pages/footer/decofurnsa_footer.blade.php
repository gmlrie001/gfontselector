<footer class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3">
          <h3 role="heading">Customer Service</h3>
          <ul>
            <li><a href="/delivery" target="_self">Delivery</a></li>
            <li><a href="/returns" target="_self">Returns</a></li>
            <li><a href="/payment-credit-options" target="_self">Payment &amp; credit options</a></li>
            <li><a href="/terms-conditions" target="_self">Terms &amp; conditions</a></li>
            <li><a href="/promos" target="_self">Promotions Ts+Cs</a></li>
            <li><a href="/faqs">FAQ's</a></li>
          </ul>
        </div>
        <div class="col-12 col-lg-3">
          <h3 role="heading">How to shop</h3>
          <ul>
            <li><a href="/furniture-care-guide" target="_self">Furniture care guide</a></li>
            <li><a href="/product-materials-explained" target="_self">Product materials explained</a></li>
          </ul>
        </div>
        <div class="col-12 col-lg-3">
          <h3 role="heading">About Us</h3>
          <ul>
            <li><a href="/about" target="_self">About Decofurn</a></li>
            <li><a href="/contact-us" target="_self">Contact us</a></li>
            <li><a href="/store-locator">Store Locator</a></li>
            <li><a href="/directions">Directions</a></li>
          </ul>
        </div>
        <div class="col-12 col-lg-3 d-none d-lg-block">
          <h3 role="heading">Social Media</h3>
          <a style="background-color: rgb(66, 103, 178); color: rgb(255, 255, 255);" target="_blank"
            href="https://www.facebook.com/decofurnsa" class="fa fa-facebook" alt="DecofurnSA on Facebook"
            title="DecofurnSA on Facebook"></a>
          <a style="background-color: rgb(0, 0, 0); color: rgb(255, 255, 255);" target="_blank"
            href="http://www.instagram.com/decofurnsa" class="fa fa-instagram" alt="Instagram" title="Instagram"></a>
        </div>
      </div>
    </div>
  </div>
</footer>

<div class="container-fluid footer-socials d-block d-lg-none">
  <div class="row">
    <div class="col-12">
      <a style="background-color: rgb(66, 103, 178); color: rgb(255, 255, 255);" target="_blank"
        href="https://www.facebook.com/decofurnsa" class="fa fa-facebook" alt="DecofurnSA on Facebook"
        title="DecofurnSA on Facebook"></a>
      <a style="background-color: rgb(0, 0, 0); color: rgb(255, 255, 255);" target="_blank"
        href="http://www.instagram.com/decofurnsa" class="fa fa-instagram" alt="Instagram" title="Instagram"></a>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="container copyright">
      <div class="row icons">
        <div class="col-12 col-lg-4 logos">
          <a href="/">
            <img class="img-fluid" src="/assets/images/footer_logo/footer-logo.svg">
          </a>
          <img class="img-fluid" src="/assets/images/ssl_logo/ssl_logo.png">
        </div>
        <div class="col-12 col-lg-8 options">
          <a href="" title="PayU">
            <img class="img-fluid" src="/assets/images/link_image/payu.png" alt="PayU">
          </a>
          <a href="htttp://mobicred.co.za" title="Mobicred">
            <img class="img-fluid" src="/assets/images/link_image/mobicred1.png" alt="Mobicred">
          </a>
          <a href="" title="decofurn">
            <img class="img-fluid" src="/assets/images/link_image/decofurn.png" alt="decofurn">
          </a>
          <a href="" title="Ozow - Ipay">
            <img class="img-fluid" src="/assets/images/link_image/ozow.png" alt="Ozow - Ipay">
          </a>
          <a href="" title="EFT">
            <img class="img-fluid" src="/assets/images/link_image/eft1.png" alt="EFT">
          </a>
          <a href="" title="AMEX">
            <img class="img-fluid" src="/assets/images/link_image/american-express.png" alt="AMEX">
          </a>
          <a href="https://www.mastercard.co.za/en-za.html" title="Mastercard">
            <img class="img-fluid" src="/assets/images/link_image/mastercard.png" alt="Mastercard">
          </a>
          <a href="" title="visa">
            <img class="img-fluid" src="/assets/images/link_image/Visa.png" alt="visa">
          </a>
          <span>We accept:</span>
          <img class="img-fluid" src="/assets/images/ssl_logo/ssl_logo.png">
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <p>© Copyright <?php echo( date('Y') ); ?> - Decofurn Furniture. All rights reserved. Prices and products displayed on this site may
            vary from time to time. Decofurn Furniture shall in no way be held responsible for any errors or
            misstatements emanating from this website.</p>
        </div>
      </div>
    </div>
  </div>
</div>
