@extends( 'welcome' )

@section( 'title', $site_settings->site_name . " :: " . $page->title )

@push( 'templateHeader' )
<!-- Bootstrap CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"
  media="all">

<!-- <link href="{{ asset('/assets/css/vault/fonts.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/styles.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/vendor.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/header_sidebar.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/dashboard.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/tabs.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/forms.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/functions.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/listing.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/mass_operation.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/css/vault/pagination.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css"> -->

<style></style>
<link href="{{ asset('/assets/_vault/templating/_/css/styles.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<style></style>

<link href="{{ asset('/assets/_vault/templating/_/css/header/_header-variables.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/_vault/templating/_/css/header/header.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
<link href="{{ asset('/assets/_vault/templating/_/css/header/navigation/navigation.css') }}" rel="stylesheet"
  onload="this.media='all';this.removeAttribute('onload');return;" media="print" type="text/css">
@endpush

@section( 'header' )
<header class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="row">
        <a class="col-12 col-md-4 col-lg-3 logo" href="/"
          title="@isset( $site_settings->site_name ) {{ $site_settings->site_name }} @endisset">
          <figure>
            <source srcset="/assets/images/mobile_logo/white-logo.svg" media="max-width:991px">
            <img class="img-fluid" src="/assets/images/logo/blue-logo.svg"
              alt="@isset( $site_settings->site_name ) {{ $site_settings->site_name }} @endisset">
          </figure>
          <img class="img-fluid d-block d-lg-none" src=""
            alt="@isset( $site_settings->site_name ) {{ $site_settings->site_name }} @endisset">
        </a>
        <div class="col-12 col-md-8 col-lg-9 header-block">
          <div class="row simple-nav">
            <div class="col-12">
              <a class="share-open" href="#">
                <img class="img-fluid" src="/assets/images/template/share-icon.svg" />
              </a>
              @php
              if(Session::has('dont_show_cart')){
                $cart_total = 0;
              }
              @endphp
              @if($cart_total > 0)
              <a class="cart-open active" href="/cart/view">
                {{$cart_total}} ITEMS
                <img class="img-fluid" src="/assets/images/template/header/cart-icon-active.svg" />
              </a>
              @else
              <a class="cart-open" href="/cart/view">
                {{$cart_total}} ITEMS
                <img class="img-fluid" src="/assets/images/template/cart-icon-item.svg" />
              </a>
              @endif
              @if(Session::has('user') && Session::get('user.id') != null)
              <a class="login-link" href="/logout">LOGOUT</a>
              @if(Session::get('user.name') != "")
              <a class="login-link profile-name-text" href="/profile">
                <img class="img-fluid" src="/assets/images/template/account-icon.svg"
                  style="margin-right: 10px; width: 15px;" />
                {{(Session::get('user.name'))}}
              </a>
              @else
              <a class="login-link profile-name-text" href="/profile">
                <img class="img-fluid" src="/assets/images/template/account-icon.svg"
                  style="margin-right: 10px; width: 15px;" />
                MY ACCOUNT
              </a>
              @endif
              @else
              <a class="login-link" href="/my-account">REGISTER</a>
              <a class="login-link" href="/my-account">LOGIN</a>
              @endif
            </div>

            @if($cart_total > 0)
            @php $cart_product->price = 0; @endphp

            @if(Session::has('show_cart') && Session::get('show_cart') == true)
            <div class="col-12 cart-dropdown active">
              @else
              <div class="col-12 cart-dropdown">
                @endif
                <div>
                  @php $cartTot = 0; @endphp
                  @foreach($cart_products as $cart_product)
                  <div class="row">
                    <div class="col-3">
                      @php
                      $variant = \App\Models\ProductVariant::where('product_id', $cart_product->product_id)->where('filters', $cart_product->filters)->first();
                      if($variant != null){
                        if($variant->variant != null && sizeof($variant->variant->displayGalleries)){
                          $image = $variant->variant->product_image;
                        }else{
                          $image = $cart_product->product->product_image;
                        }
                      }else{
                        $image = $cart_product->product->product_image;
                      }
                      @endphp
                      <img class="img-fluid" src="/{{$image}}" />
                    </div>
                    <div class="col-9">
                      <h2>
                        {{$cart_product->product->title}}
                        <a href="/cart/delete/dropdown/{{$cart_product->id}}" class="delete-cart-dropdown">x</a>
                      </h2>
                      <h4>
                        <span>QTY: {{$cart_product->quantity}}</span>
                        <span class="float-right">R {{$cart_product->price}}</span>
                      </h4>
                    </div>
                  </div>
                  @php $cartTot += ($cart_product->price * $cart_product->quantity); @endphp
                  @endforeach
                </div>
                <h3>TOTAL: <span class="float-right">R {{$cartTot}}</span></h3>
                <a href="/cart/view">View Cart</a>
              </div>
              @endif
            </div>

            @if(sizeof($cart_products) > 4)
            <script>
              $(".cart-dropdown > div").mCustomScrollbar();
            </script>
            @endif

            <div class="row quick-links">
              <div class="col-12">
                @forelse($quick_links as $quick_link)
                <a href="{{$quick_link->link}}">
                  <strong>{{$quick_link->title}}</strong>
                  <span>{{$quick_link->description}}</span>
                </a>
                @empty
                @endforelse
              </div>
            </div>
          </div>
          <div class="col-12 mobile-controller">
            <a class="nav-open" href="#">
              <img class="img-fluid" src="/assets/images/template/header/navicon.svg" />
            </a>
            <a class="search-open" href="#">
              <img class="img-fluid" src="/assets/images/template/header/search-white.svg" />
            </a>
            <a class="share-open" href="#">
              <img class="img-responsive" src="/assets/images/template/share.svg" />
            </a>
          </div>
          <div class="col-12 position-relative no-min-height">
            <form class="col-12 search-form-mobile" method="GET" action="/search">
              <label for="search">
                <input id="search" type="search" class="form-control search-form" placeholder="Search products..." required>
              </label>
              <button type="submit">Search</button>
            </form>
          </div>
        </div>
      </div>
    </div>
</header>

<nav class="container-fluid">
  <div class="row">
    <div class="container position-relative">
        <div class="row position-relative">
        <div class="col-12 col-lg-9 navigation">
            <a href="/furniture-products" class="mego-open">Shop Now</a>
            <a href="/furniture-products/sale">On Sale</a>
            <a href="/specials" target="_self">FLYER SPECIALS</a>
            <a href="/store-locator" target="_self">STORE LOCATOR</a>
            <a href="/inspiration" target="_self">INSPIRATION</a>
        </div>
        <form method="GET" action="https://decofurnsa.co.za/search" accept-charset="UTF-8"
            class="col-12 col-lg-3 search-form" novalidate="">
            <input class="form-control search-form" placeholder="Search here..." name="search" type="text">
            <button type="submit">
            <img class="img-responsive" src="/assets/images/template/header/search-icon.svg">
            </button>
        </form>
        </div>
    </div>
  </div>
</nav>
@endsection
