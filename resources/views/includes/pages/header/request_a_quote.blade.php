<!---QUOTE  ---->
<div class="quoteOverlay"></div>

<div class="container enquireModal">
  <div class="row">

    <div class="col-12 col-lg-8 mx-auto quoteContain">

      <div class="row">

        <div class="col-12">
          <a class="modalClose ml-auto mobileCustomPadding" href="javascript:void(0);">
            <img class="img-fluid float-right" src="/backup_html/images/assets/closeModal.svg" alt="navicon">
          </a>
        </div>

        <div class="col-12 enquireForm">

          <h2>Request a quote</h2>
  
          <form action="{{ route( 'request_a_quote' ) }}" method="POST">
	    {{ csrf_field() }}

            <div class="form-row">

              <div class="form-group col-12 col-sm-6">
                <label for="firstnameVal">First Name *</label>
                <input id="firstnameVal" name="firstnameVal" type="text" class="form-control" placeholder="What's your first name" required>
              </div>

              <div class="form-group col-12 col-sm-6">
                <label for="lastnameVal">Last Name *</label>
                <input id="lastnameVal" name="lastnameVal" type="text" class="form-control" placeholder="What's your last name" required>
              </div>

              <div class="form-group col-12 col-sm-6">
                <label for="emailVal">Contact Email *</label>
                <input id="emailVal" name="emailVal" type="email" class="form-control" placeholder="What's your email?" required>
              </div>

              <div class="form-group col-12 col-sm-6">
                <label for="numberVal">Contact number</label>
                <input id="numberVal" name="numberVal" type="text" class="form-control" placeholder="What's your number" required data-parsley-type="digits">
              </div>

              <div class="form-group col-12">
                <label for="webtypeVal">Type of Website</label>
                <select id="webtypeVal" name="webtypeVal" class="form-control" placeholder="Type of website" required>
                  <option>Type of website</option>
                  <option value="brochure">Brochure</option>
                  <option value="blogger">Blogger</option>
                  <option value="partialecommerce">Partial E-Commerce</option><!-- No Cart/Checkout -->
                  <option value="smallecommerce">Small E-Commerce</option><!-- Less than 500 products -->
                  <option value="fullecommerce">Full E-Commerce + Support</option><!-- Fully Supported -->
                </select>
              </div>

              <div class="form-group col-12">
                <label for="messageVal">Message</label>
                <textarea id="messageVal" name="messageVal" id="message" class="form-control" placeholder="" rows="3"></textarea>
              </div>

	      {!! Honeypot::generate( 'my_time', 'my_name' ) !!}

              <div class="form-group col-12">
                <button class="btn btnForm float-left" type="submit">Send</button>
              </div>

            </div>
          </form>

        </div>

      </div>

    </div>
  </div>    

</div>
