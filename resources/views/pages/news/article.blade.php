@extends( 'layouts.page' )

@section( 'title', $site_settings->site_name . " :: " . $page->title  )

@section( 'content' )
<style>
  /*'/_html-backup/assets/images/backgrounds/vineyards.jpg'*/
  body {
  @if( $news_settings->full_background_image )
    background: url( /{{$news_settings->full_background_image}} ) center center no-repeat;
  @endif
    min-height: 100vh;
    background-attachment: fixed;
  }
</style>

@isset( $site_settings->logo )
<div class="container-fluid pt-2 pb-5 d-none d-lg-block">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 px-0">
          <a href="/">
            <img class="img-fluid" src="/{{ ltrim( $site_settings->logo, '/' ) }}" />
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endisset

@isset( $article )
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 white-bg-block vineyard-block article-block">
          <div class="row">
            <div class="col-12 col-lg-6">

              @php $date_form = explode( ' ', date( 'd M y', strtotime( $article->published_date ) ) ); @endphp
              <span class="date-block">
                {{ $date_form[0] }} {{ $date_form[1] }}<i>{{ $date_form[2] }}</i>
              </span>
  
            @if( $article->galleries && count( $article->galleries ) > 0 )
              <div class="main-slider">
              @foreach( $article->galleries as $k=>$gallery )
                <img class="img-fluid" src="/{{ ltrim( $gallery->gallery_image, '/' ) }}" alt="{{ $gallery->title }}">
                @php $navs[$k]['img'] = $gallery->gallery_image; $navs[$k]['title'] = $gallery->title; @endphp
              @endforeach
              </div>
              @if ( $navs && count( $navs ) > 0 )
              <div class="nav-slider">
              @foreach( $navs as $key=>$nav )
                <img class="img-fluid" src="/{{ ltrim( $nav['img'], '/' ) }}" alt="{{ $nav['title'] }}">
              @endforeach
              @php unset( $navs ); @endphp
              </div>
              @endif
            </div>
            @endif
  
            <div class="col-12 col-lg-6">
              
            @if( $article->title )
              <h3>{{ $article->title }}</h3>
            @endif
  
              <div class="socials text-left mb-4">

                <span>Share:</span>
  
              @if( $article_sharing && count( $article_sharing ) > 0 )
                @foreach( $article_sharing as $key=>$sharing )
                <a rel="noopener noreferrer" class="@if( $key === 'email' ) fas fa-envelope @else fab {{ $sharing[1] }} @endif" title="Share to {{ ucfirst( $key ) }}" target="_blank" href="{{ $sharing[0] }}"></a>
                @endforeach
              @endif

              @if( $navigate_back )
                <a href="{{ ltrim( $navigate_back, '/' ) }}" class="fas fa-caret-left back"></a>
              @else
                <a href="" class="fas fa-caret-left back" onclick="window.history.go(-1);"></a>
              @endif

            </div>
              @if( $article->description )
                {!! $article->description !!}
              @endif
              @if( $article->articles && count( $article->articles ) > 0 )
                @forelse( $article->articles as $key=>$subarticle )
                  {!! $subarticle->description !!}
                @empty
                @endforelse
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@php unset( $date_form, $key, $gallery, $nav, $sharing ); @endphp
@endisset

@endsection
