@extends( 'layouts.page' )

@section( 'title', $site_settings->site_name . " :: " . $page->title  )

@section( 'content' )
<div class="container-fluid py-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <h1 class="col-12 text-center bottom-border-display">Contact Us<span></span></h1>
      </div>
    </div>
  </div>
</div>

@isset($address)
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="row">
        @foreach( $address as $key=>$contact )
        <div class="col-12 col-lg-5">
          <div class="contact-block">
          @if( $contact->title )
            <h3>{{ $contact->title }}</h3>
          @endif
          <style></style>
          @if( $contact->address )
            <a href="javascript:void(0);" class="" title="No-Ops">
              <img class="img-fluid" src="/_html-backup/assets/images/template/map-icon.png" />
              {!! $contact->address !!}
            </a>
          @endif
          @if( $contact->email )
            <a rel="noopener noreferrer" title="Email Directly" class="" href="mailto:{{ $contact->email }}?subject=Website%20Contact%20Direct-Email">
              <img class="img-fluid" src="/_html-backup/assets/images/template/mail-icon.png" />
              <p>{{ $contact->email }}</p>
            </a>
          @endif
          </div>
        </div>
        @endforeach
        <div class="col-12 col-lg-3">
            <h3>Social Media</h3>
            <div class="socials text-left">
            @foreach( $socials as $key=>$social )
              <a rel="noopener noreferrer" title="Share to {{ $social->title }}" class="fab {{ $social->social_media_icon }}" target="_blank" href="{{ $social->social_media_url }}"></a>
            @endforeach
            </div>
        </div>

        <div class="col-12 col-lg-4">
          <form class="contact-form" action="{{ route( 'contact_enquiry' ) }}" method="POST">
            {{ csrf_field() }}
            <fieldset>
              <legend>
                <h3>Get In Touch</h3>
              </legend>
              <label for="name">
                <input type="text" name="name" id="name" placeholder="Full name*" required>
              </label>
              <label for="email">
                <input type="email" name="email" id="email" placeholder="Email*" required>
              </label>
              <label for="mobile">
                <input type="text" name="mobile" id="mobile" placeholder="Telephone*" required>
              </label>
            </fieldset>
            <fieldset>
              <label for="message">
                <textarea name="message" id="message" rows="5" placeholder="Message*" required></textarea>
              </label>
            </fieldset>
            {!! Honeypot::generate('my_name', 'my_time') !!}
            <button type="submit">SEND</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endisset

@isset( $contact_settings )
<div class="container-fluid mt-5">
  <div class="row">
    <a rel="noopener noreferrer" class="" title="Contact Map of Address" href="javascript:void(0);" target="_blank">
    @if( $contact_settings->map_image )
      <img class="img-fluid d-none d-lg-block" src="/{{ ltrim( $contact_settings->map_image, '/' ) }}" />
    @endif
    @if( $contact_settings->map_image_mobile )
      <img class="img-fluid d-block d-lg-none" src="/{{ ltrim( $contact_settings->map_image_mobile, '/' ) }}" />
    @endif
    </a>
  </div>
</div>
@endisset

@endsection
