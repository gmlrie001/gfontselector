<!DOCTYPE html>

<html lang="en" dir="ltr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

    <!-- Title and SEO -->
  @if ( Request::is( '/' ) )
    <title>{{ $site_settings->site_name }}</title>
  @else
    <title>{{ $site_settings->site_name }} :: {{ $seo['seo_title'] }}</title>
  @endif

  <!-- Basic SEO Meta Tags -->
    <meta name="keywords" content="{{ $seo['seo_keywords'] }}" />
    <meta name="description" content="{{ strip_tags( $seo['seo_description'] ) }}" />
    <meta name="author" content="Monza Media <http://www.monzamedia.com>" />

  <!-- Meta THEME COLOR -->
    <meta name="theme-color" content="#c1d100"><!-- Site Theme-Color -->
    <meta name="image" content="{{ $seo['image'] }}">

  <!-- Schema.org for Google -->
    <meta itemprop="name" content="{{ $seo['title'] }}">
    <meta itemprop="description" content="{{ strip_tags( $seo['seo_description'] ) }}">

  <!-- Twitter -->
    <meta name="twitter:card" content="{{ strip_tags( $seo['seo_description'] ) }}">
    <meta name="twitter:title" content="{{ $seo['title'] }}">
    <meta name="twitter:description" content="{{ strip_tags( $seo['seo_description'] ) }}">
    <meta name="twitter:site" content="{{ '@'.$site_settings->site_name }}">
    <meta name="twitter:creator" content="{{ '@'.$site_settings->site_name }}">
    <meta name="twitter:image:src" content="{{ $seo['image'] }}">

  <!-- Open Graph general (Facebook, Pinterest & Google+) -->
<!--
    <meta name="og:title" content="{{ $seo['title'] }}">
    <meta name="og:description" content="{{ strip_tags( $seo['seo_description'] ) }}">
    <meta name="og:image" content="{{ $seo['image'] }}">
    <meta name="og:url" content="{{ $seo['url'] }}">
    <meta name="og:site_name" content="{{ $site_settings->site_name }}">
    <meta name="og:locale" content="EN">
    <meta name="fb:app_id" content="1316646895143006">
-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--
    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/css/media-queries.css">
    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/fonts/style.css">
-->

    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/vendor/font-awesome/font-awesome-4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/vendor/slick-slider/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/vendor/slick-slider/slick-theme.css">

    <!-- JQuery-UI CSS -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <!-- Slick-Slider CSS -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">

    <!-- Selectric CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/vendor/selectric/selectric.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" type="text/css" href="/assets/vendor/magnific_popup/magnific-popup.css">

    <!-- Bespoke Site Look and Layout CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/assets'; ?>/css/styles.css">

    <!-- JQuery -->
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <!--<script async src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <!-- Load Website Google Font: Nunito & Open Sans :: Single API Call | Separate Calls -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                //families: [ 'Nunito:300,400,600,700,800', 'Open+Sans:400,600' ]
                families: [ 'Nunito:300,400,600,700,800' ]
            }
        });
    </script>
    <script src="/assets/js/components/obsfucate_mailto.js"></script>
    <!-- -->

  </head>

  @php ob_flush(); @endphp

  <body>

    @include( 'includes.pages.header.natural.bootstrap4' )

    @if( Request::is( '/' ) )
      @include( 'includes.pages.page_banner.natural.bootstrap4' )
    @endif

    <main role="main">
      @include( 'includes.pages.sidebar.natural.social_sidebar' )
      @yield( 'content' )
    </main>

    @include( 'includes.pages.footer.natural.bootstrap4' )

    @include( 'includes.pages.modals.natural.share_modal' )

<!-- FRAMEWORKS -->
  <!-- JQuery-UI FRAMEWORK CSS & JS -->
   <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>-->
  <!-- Bootstrap 4 FRAMEWORK along with POPPER -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- PLUGINS -->
  <!-- Slick Slider: SLIDE ANYTHING Plugin -->
    <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <!-- Open Codes - from Google for Address Tagging/Coding -->
    <!--script src="//cdn.jsdelivr.net/openlocationcode/latest/openlocationcode.js"></script-->
  <!-- Magnific Popup -->
    <script src="/assets/vendor/magnific_popup/jquery.magnific-popup.min.js"></script>

    <script src="/assets/vendor/selectric/jquery.selectric.min.js"></script>
  <!-- Custom Site Script -->
    <script src="/assets/js/scripts.js"></script>

    <style>
      .page-banner-hero > * {
        max-height: 438px;
        overflow: hidden;
      }
      @media only screen and (max-width: 568px) {
        .page-banner-hero > * {
          max-height: 300px;
        }
      }
    </style>
  </body>

</html>
