<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title and SEO -->
    <title>{{$site_settings->site_name}} :: {{$page->seo_title}}</title>
    <meta name="keywords" content="{{$page->seo_keywords}}" />
    <meta name="description" content="{{$page->seo_description}}" />
    <meta name="author" content="Monza Media <http://www.monzamedia.com>" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap -->
    <link href="{{ asset( 'assets/css/bootstrap.min.css' ) }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/js/sites/fortuna/pages/vendor/slick/slick/slick.css' ) }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets//js/sites/fortuna/pages/vendor/slick/slick/slick-theme.css' ) }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets//js/sites/fortuna/pages/vendor/Magnific-Popup-master/dist/magnific-popup.css' ) }}"/>
    {{--<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/js/sites/fortuna/pages/vendor/selectric/src/selectric.css' ) }}"/>--}}

    <link type="text/css" href="{{ asset( 'assets/css/styles.css' ) }}" rel="stylesheet">
    <!-- Client Updates/Changes - JAN 2019 -->
    <link type="text/css" href="{{ asset( 'assets/css/fix-styles-jan2019.css' ) }}" rel="stylesheet">

    <!-- Fonts -->
    <link type="text/css" href="{{ asset( 'assets/css/fonts/fonts.css' ) }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset( 'assets/css/font-awesome-4.7.0/css/font-awesome.min.css' ) }}">

    <style>
      header.fixer-styling {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 120px;
        z-index: 9020 !important;
      }
      /* RESPSONSIVE Tablet Landscape and Screensizes <= 1024 */
      @media only screen and (max-width: 1024px) {
        header.fixer-styling {
          height: 100px;
        }
      }
    </style>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

    <script type="text/javascript" src="{{ asset( 'assets/js/sites/fortuna/pages/vendor/Magnific-Popup-master/dist/jquery.magnific-popup.min.js' ) }}"></script>

  </head>
  <body>

    <header class="fixer-styling">

      @include('includes.pages.header.fortuna.bootstrap4')

    </header>

    <main role="main">

      {{--
      @if( Request::is( '/' ) )
        @include( 'includes.pages.page_banner.fortuna.bootstrap4' )
      @endif
      --}}

      @include('includes.pages.messages.fortuna')

      @yield('content')

    </main>


    <footer class="container footerContain mobileCustomPadding">

      @include('includes.pages.footer.fortuna.bootstrap4')

    </footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset( 'assets/js/sites/fortuna/bootstrap.min.js' ) }}"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="{{ asset( 'assets/js/sites/fortuna/pages/vendor/slick/slick/slick.min.js' ) }}"></script>

    <!-- JQuery Selectric -->
    {{--<script type="text/javascript" src="{{ asset( 'assets/js/sites/fortuna/pages/vendor/selectric/src/jquery.selectric.js' ) }}"></script>--}}
    <!-- GMaps -->
    {{--<script type="text/javascript" src="{{ asset( 'assets/js/sites/fortuna/pages/vendor/gmap/maps.js' ) }}"></script>--}}

    <!-- Sticky Header -->
    <script type="text/javascript" src="{{ asset( 'assets/js/sites/fortuna/pages/vendor/sticky/stickyHeader.js' ) }}"></script>
    <!-- Masonary -->
    <script src="{{ asset( 'assets/js/sites/fortuna/masonry.js' ) }}"></script>
    <!-- Custom Site Script -->
    <script src="{{ asset( 'assets/js/sites/fortuna/scripts.js' ) }}"></script>

  </body>

</html>
