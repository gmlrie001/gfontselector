<!doctype html>

<html lang="en" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

  <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>@yield( 'title' )</title>
    <!-- <link rel="icon" type="image/ico" href="/favicon.ico"> -->
    <meta name="theme-color" content="#004737">
    <meta name="background-color" content="#ffffff">
      
  <!-- BOOTSTRAP v4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- Site Styles -->
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/_trendytrees/landing/landing.css">
    <!-- <link rel="stylesheet" type="text/css" href="/assets/css/general.css"> -->

  <!-- Font Awesome 5.8.1 SHIM via CDN or LOCAL -->
    <link rel="stylesheet" type="text/css" href="//use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" type="text/css" href="//use.fontawesome.com/releases/v5.8.1/css/v4-shims.css">

  <!-- Slick Slider CSS Theme -->
    <!-- <link rel="stylesheet" href="/assets/vendor/slick-slider/slick.css" type="text/css"> -->
    <!-- <link rel="stylesheet" href="/assets/vendor/slick-slider/slick-theme.css" type="text/css"> -->

    <style>
      /*
      * {
        outline: 1px solid red;
      }
      */
      html,
      body {
        max-width: 100%;
        min-height: 100%;
      }
      body.landing-page {
        overflow: hidden;
        padding: 0 1rem 0;
      }
      header.container-fluid {
        top: 0;
        z-index: 9190;
      }
      .center-center-noflex {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
      }
      .shadow-1 {
        box-shadow: 1px 5px 16px -3px rgba(4, 4, 4, 0.6);
        background-blend-mode: multiply;
      }
      @media only screen and (max-width: 1024px) {

        .icon-title,
        .icon-title * {
          top: -2.5%;
        }

        main.landing-page {
          margin-top: -2.5rem;
          margin-bottom: 5rem;
        }

        .options {
          max-height: 500px;
        }
        .option {
          position: relative;
          max-height: 500px;
        }

      }
      @media only screen and (max-width: 992px) {
        main.landing-page {
          padding: 0 1rem 0;
          background-image: none !important;
        }

        .icon-title,
        .icon-title * {
          top: -5%;
        }

        main.landing-page {
          margin-top: 0; 
          margin-bottom: 3rem;
        }

        .options {
          max-height: 450px;
        }
        .option {
          position: relative;
          max-height: 450px;
        }

      }

      @media only screen and (max-width: 768px) {
        header.container-fluid {
          top:7.5%;
        }
        
        main.landing-page {
          height: calc(3 * (375px + 3rem));
        }

        .option .icon-title,
        .option .icon-title * {
          left: 50%;
        }

        .option:first-child .icon-title,
        .option:first-child .icon-title *,
        .option:last-child .icon-title,
        .option:last-child .icon-title * {
          left: 0;
        }

        .options {
          max-width: 100%;
          max-height: unset;
        }
        .option img.img-fluid {
          height: 100%;
          object-fit: cover;
        }
        .option {
          max-height: 400px;
        }
       
       .landing-page header {
         top: 0;
         left: 0;
         bottom: 0;
         right: 0;
         max-width: 100%;
         max-height: 250px;
         height:259px;
         margin: 0 auto;
       }
      }
    </style>

  <!-- CORDEL FONT to be REPLACED by NUNITO SANS -->
    <!-- JQuery -->
    <script src="//code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <!-- Load Website Google Font: Nunito & Open Sans :: Single API Call | Separate Calls -->
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
      WebFont.load({
        google: {
          families: [
            'Muli:300,400,500,600,700,800',
            'Raleway:300,400,500,600,700',
            'Alex+Brush:300,400'
          ]
        }
      });
    </script>

  </head>

  <body class="landing-page">

	@yield( 'content' )

  </body>

</html>
