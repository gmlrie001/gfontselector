<!DOCTYPE html>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->


<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
		<link rel="shortcut icon" href="/assets/images/vault/logo/favicon.ico" />

		<!-- SEO ? -->
		<title>Vault - Monza Media</title>

    <meta name="description" content="Locked" />
    <meta name="keywords" content="Locked" />
    <meta name="author" content="Monza Media <http://www.monzamedia.com>" />
    <meta name="robots" content="noindex, nofollow">
	
		@include('includes.vault.styleincludes')

		<!-- Front-End Styles -->
		<link href="{{ asset('/assets/css/vault/fonts.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/styles.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/vendor.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/header_sidebar.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/dashboard.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/tabs.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/forms.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/functions.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/listing.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/mass_operation.css') }}" rel="stylesheet" />
		<link href="{{ asset('/assets/css/vault/pagination.css') }}" rel="stylesheet" />

		<!--Scripts-->
		<!--Vendor Scripts-->
		@include('includes.vault.scriptIncludes')
		
		<style id="notifier">
			[hidden], .hidden {
				display: none !important;
			}
			.slick-track,
			a.col-xs-12.padding-0.slick-slide.slick-current.slick-active {
				width: 100% !important;;
			}
			.header-alert-circle.notifications-count {
				padding: 5px 10px;
				margin-bottom: 15px;
				border-radius: 50%;
				background-color: red;
				vertical-align: super;
				font-size: 11px;
				font-weight: 700;
				color: #ffffff;
				text-align: center;
			}
			.font-selected.toast {
				max-width:calc( 85vw / 3 - 2.5rem );
				padding:1rem 2.5rem;
				background-color:#fff;
				transition:all 0.2s;
				width:100%;
				outline:1px solid rgba(10,10,10,0.25);
				outline-offset:-1px;
				position:fixed;
				bottom:-5%;
				box-shadow: 0 -2px 6px -1px rgba(10,10,10,0.15);
			}
			.font-selected.toast:hover{
				bottom:0;
			}
			.clearfix:after {
				display: table;
				content: "";
				float: none;
				clear: both;
			}
			.float-left {
  			float: left !important;
			}
			.float-right {
  			float: right !important;
			}
			#selected-title_font {
				left: calc( ( 15vw + 1.25rem ) + 0 * ( 85vw / 3 ) - 0.5rem );
			}
			#selected-body_font {
				left: calc( ( 15vw + 1.25rem ) + 1 * ( 85vw / 3 ) - 0.5rem );
			}
			#selected-cta_font {
				left: calc( ( 15vw + 1.25rem ) + 2 * ( 85vw / 3 ) - 0.5rem );
			}
			section#myUL {
  			margin-bottom: 5rem;
			}
			@media only screen and (max-width: 992px) {
				.font-selected.toast {
					max-width: 100%;
					overflow-x: hidden;
					max-height: 50px;
				}
				.font-selected.toast h2 {
					font-size: 95%;
					font-weight: 700;
				}
				#selected-title_font {
					left: unset;
					bottom: calc( 2 * 50px );
				}
				#selected-body_font {
					left: unset;
					bottom: calc( 1 * 50px );
				}
				#selected-cta_font {
					left: unset;
					bottom: calc( 0 * 50px );
				}
				section#myUL {
  				margin-bottom: 3.5rem;
				}
			}
		</style>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/vault/modules/google-font-selector.css') }}" media="all">
		<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/vault/modules/bs4_classes.css') }}" media="all">

		@stack( 'vaultPageStyles' )

	</head>
	
	@php ob_flush(); @endphp

	<body>

		<div class="container-fluid header padding-0">
			@include('includes.vault.header')
		</div>
	
		<div class="vault-content">

			<div class="side-nav">
				@include('includes.vault.nav')
			</div>

			@yield('content')
		</div>
	
		@include('includes.vault.messages')
	
		<!--Front-End Scripts-->
		<script src="{{ asset('/assets/js/vault/scripts.js') }}" type="text/javascript"></script>
		<!-- <script src="{{ asset('/assets/js/vault/modules/google-fonts-selector.js') }}" type="text/javascript"></script> -->
		<script>
		var font_loader = function(elem, d=document, t='link', family, variants){
			// return console.log(family, variants);
			var e = elem;
			var b = e.getBoundingClientRect();
			if( b.top > 0 && b.height > 0 ) {
				var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
						gfonts_font_sel = family,
						gfonts_font_var = variants;

				var url_build, g, s;
						url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var; //.join(',');

				try {
					var fnc = new Function( 'return ' + 'function(t) { return document.createElement(t); }')();
					g = fnc(t);
					g.rel = "stylesheet";
					g.href = url_build + '&display=swap';
					s = d.getElementsByTagName(t)[0];
					s.parentNode.insertBefore(g, s);
					return console.log( [gfonts_font_sel, gfonts_font_var] );
				} catch( err ) {
					console.clear(); console.warn( '\r\n' + err + '\r\n' );
				}
			};
			return;
		};
		var nonHidden = [].slice.call( document.querySelectorAll('article.google_fonts:not(.hidden)') )
		nonHidden.forEach( el => {
			// var el = l; nonHidden.shift(); console.log( nonHidden );
			var f = el.dataset.family,
					c = el.dataset.category,
					v = el.dataset.variants;
			font_loader( el, document, 'link', f, v);
		} );
		var formData = new FormData();
		var inputs_title = [].slice.call( document.querySelectorAll('input[type=radio][name=title_font]') );
		input_listeners( inputs_title, 'change' );
		var inputs_body  = [].slice.call( document.querySelectorAll('input[type=radio][name=body_font]') );
		input_listeners( inputs_body, 'change' );
		var inputs_cta   = [].slice.call( document.querySelectorAll('input[type=radio][name=cta_font]') );
		input_listeners( inputs_cta, 'change' );

		function input_listeners(arr, evt) {
			arr.map( el => {
				el.addEventListener( evt, (evnt)=>{
					var elm = evnt.srcElement;
					evnt.stopPropagation();

					if( ! evnt.srcElement.checked ) return;

					if ( ! formData.has(evnt.srcElement.name) ) {
						formData.append(evnt.srcElement.name, evnt.srcElement.value);
					} else {
						formData.set(evnt.srcElement.name, evnt.srcElement.value);
					}

					var key   = evnt.srcElement.name;
					var value = evnt.srcElement.value;
					storeSession(key, value);
					console.log( fetchStored( key ) );
					fontSelected(key, value);
					var dict  = {
						'title_font': null,
						'body_font': null,
						'cta_font': null,
					}
					autoSave( key, value );
				}, false)
			})
		}

		function storeSession(key, value) {
			var sess = window.sessionStorage || null;
			return ( sess && sess != null ) ? sess.setItem(key, value) : null
		}

		function fetchStored(key) {
			var sess = window.sessionStorage || null;
			return ( sess != null && sess.key(key) != null ) ? sess.getItem(key) : null;
		}

		function removeStored(key) {
			var sess = window.sessionStorage || null;
			if (sess != null && sess.key(key) != null) {
				console.log( sess.key( key ) );
				sess.removeItem(key);
				autoSave(key)
				return !0;
			}
			return;
		}

		function deSelectFont(element) {
			try {
				var del = element.parentNode.closest('.toast'),
						key = del.dataset.fontClass,
				parentOfDel = del.parentNode;
				parentOfDel.removeChild( del );
				var selected = document.querySelector( 'input[name*="'+ key +'"]:checked' );
				if ( selected != null || typeof( selected ) != undefined ) selected.checked = ( removeStored( key ) ) ? false : true;
				try {
					uncheck(key);
				} catch( err ) {
					console.error("\r\n" + err + "\r\n");
				}
			} catch( err ) {
				console.error("\r\n" + err + "\r\n");
			}
			return;
		}

		/* RF VSCode: f-12 */
		/*
		function uncheck(iname) {
			try {
				var selected = document.querySelector( 'input[name*="'+ iname +'"]:checked' );
					if ( selected != null ) selected.checked = ( removeStored( iname ) ) ? false : true;
			} catch( err ) {
				console.error("\r\n" + err + "\r\n");
			}
			return;
		}
		*/

		function fontSelected(name, value, active=false) {
			var toasterMarkup = `
			<section id="selected-%name%" data-font-class="%name%" data-font="%value%" class="font-selected toast bring-to-front clearfix">
				<div class="float-right" style="cursor:pointer"><span class="remove-font" onclick="deSelectFont(this.parentNode);">&times;</span></div>
				<div class="wrapper" style="display:flex;flex-direction:row">
					<div class="toast-header clearfix">
						<h2>
							<span class="bigger" style="font-style:italic;text-decoration:none;font-weight:300;">%name%</span> 
						</h2>
					</div>
					<div class="toast-body">
						<p>
							<strong>%value%</strong>
						</p>
					</div>
				</div>
			</section>
			`;
			toasterMarkup = toasterMarkup
											.replace( /%name%/ig, name )
											.replace( /%value%/ig, value );

			return document.querySelector('.vault-content').appendChild(
				document.createRange().createContextualFragment(
					toasterMarkup
				)
			) || null;
		}

		function autoSave(name=null, value=null) {
			var k = name || null,
					v = value || null;

			if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
				var xhr = new XMLHttpRequest();
			} else if (window.ActiveXObject) { // IE 6 and older
    		var xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}

			postData = new FormData;
			addFormData( postData, {'_token': "{{ csrf_token() }}"} );

			addFormData( postData, {'id': 1});
			addFormData( postData, {'status': 'PUBLISHED'});
			addFormData( postData, {'status_date': ''});

			addFormData( postData, {'my_name': ''});

			nm = document.querySelector('input[name=my_time]').value;
			addFormData( postData, {'my_time': '"'+nm+'"'});

			if ( k != null && typeof( k ) != null ) {
				if( v != null ) {
					postData.append( k, v.replace( ' ', '+' ) );
				} else {
					postData.append( k, v );
				}
			}

			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					console.log( this );
					console.log( this.responseText );
				}
			}

			xhr.open("POST", document.location.href, true);
			// xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.setRequestHeader('X-CSRF-TOKEN', "{{ csrf_token() }}");
			xhr.send( postData );
		}

		function addFormData(form, pair={}) {
			Object.keys(pair).forEach( k => {
				postData.append(k, pair[k]);
			})

		}

		document.addEventListener( 'DOMContentLoaded', function() {
			if( document.location.pathname.search('fonts') != -1 ) {
				if ( window.sessionStorage && window.sessionStorage.length > 0 ) {
					Object.keys(sessionStorage).forEach( (k)=>{
						fontSelected( k, sessionStorage[k], true );
						// ( function() {
							try {
								var seltd = document.querySelector( 'input#' + k + '[value="' + sessionStorage[k] + '"]' ) || null;
								if ( ! seltd.checked ) seltd.checked = true;
								return;
							} catch( err ) { /* */ }
						// } )();
					} )
				}
			}
		} );
		</script>

	</body>
</html>