<!DOCTYPE html>

<html lang="en" dir="ltr">

  <head>
  <!-- Meta, Title and Basic SEO -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Global CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css" media="print" onload="this.media='all';">
  <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/elegant_font/css/style.css" media="print" onload="this.media='all';">

    <title>Article Layouts - Inline Text and Image</title>

  <!-- Theme CSS -->
    <!-- <link id="theme-style" rel="stylesheet" type="text/css" href="/assets/css/styles.css"> -->
  <!-- Style CSS
    <link id="style-style" rel="stylesheet" type="text/css" href="/assets/css/style.css"> -->
  
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Includes styles, remainder of the SEO meta tags and eager loading scripts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;family=Raleway:300,400,600,700,800&amp;display=swap">

    <!-- Font Awesome 5.8.1 SHIM via CDN or LOCAL -->
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" media="all" onload="this.media='all';">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css" media="all" onload="this.media='all';">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    @stack( 'pageStyles' )

  </head>

  @php ob_flush(); @endphp

  <body>

    <div class="page-container">
      @yield('content')
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script async="" defer="" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script async="" defer="" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>

</html>
