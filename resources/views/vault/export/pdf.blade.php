<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="viewport" content="width=device-width">
        <style>
            table {  
                color: #333;
                font-family: Helvetica, Arial, sans-serif;
                font-size: 11px;
                width: 640px; 
                border-collapse: 
                collapse; border-spacing: 0; 
            }
            
            td, th {  
                border: 1px solid transparent; /* No more visible border */
                height: 30px;
            }
            
            th {  
                background: #DFDFDF;  /* Darken header a bit */
                font-weight: bold;
            }
            
            td {  
                background: #FAFAFA;
                text-align: center;
            }
            
            /* Cells in even rows (2,4,6...) are one color */        
            tr:nth-child(even) td { background: #F1F1F1; }   
            
            /* Cells in odd rows (1,3,5...) are another (excludes header cells)  */        
            tr:nth-child(odd) td { background: #FEFEFE; }  
        </style>
	</head>
	<body>
        <table>
            <thead>
                <tr>
                    @foreach ($columns as $col)
                        <th>{{ucfirst(str_replace('_',' ',$col))}}</th>        
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < sizeof($data); $i++)
                <tr>        
                    @foreach ($data[$i] as $res)
                        <td>{{$res}}</td>        
                    @endforeach        
                </tr>
                @endfor 
            </tbody>   
        </table>
	</body>
</html>
