@extends('layouts.basic')

@section('content')
<div class="container" style="max-height: 650px;margin:auto;position:absolute;top:0;left:0;right:0;bottom:0;">
	<div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3">
	    <div class="col-xs-12 box-shadow-block">
		<div class="col-xs-12 vault-logo">
		    <img src="{{ url('/').'/assets/images/vault/logo/vault-logo-white.svg' }}" class="img-responsive center-block" />
		</div>
		{!! Form::open(array('class' => 'col-xs-12 login-form')) !!}
		    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 form-group padding-0">
			{!! Form::email('email', null, array('placeholder' => 'EMAIL ADDRESS', 'class' => 'form-control', 'required' => '', 'autofocus' => '', 'autocapitalization' => 'off')) !!}
			<div class="label label-danger">{{ $errors->first('email') }}</div>
		    </div>
		    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 form-group padding-0">
			{!! Form::password('password', array('placeholder' => 'PASSWORD', 'class' => 'form-control', 'required' => '')) !!}
			<div class="label label-danger">{{ $errors->first('password') }}</div>
		    </div>
		    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 form-group padding-0">
			{!! Honeypot::generate('my_name', 'my_time') !!}
			{!! Form::submit('Login', array('class' => 'btn btn-success btn-lg btn-block')) !!}
		    </div>
		{!! Form::close() !!}
	    </div>
	</div>
	<div class="col-xs-12 text-center powered-by">
	    <p class="col-xs-12">Powered by</p>
	    <a class="col-xs-12" href="http://www.monzamedia.com/" target="_blank">
		<img src="{{ url('/').'/assets/images/vault/template/monza.png' }}" class="img-responsive center-block" />
	    </a>
	</div>
</div>
@endsection