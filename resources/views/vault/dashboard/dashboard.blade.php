@extends('layouts.vault')

@push( 'vaultPageStyles' )
<style>
.dashboard_main {
    background-image: url('/assets/images/vault/template/dashboard.jpg');
}
@media only screen and (max-width: 992px) {
    .dashboard_main {
        height: 93vh;
        background-image: url('/assets/images/vault/template/dashboard_mobile.jpg');
        background-size: cover !important;
        background-position: center center !important;
    }
}
</style>
@endpush

@section('content')
    <div class="dashboard_main drop-zone" style="">
        <div class="notify col-xs-12 col-md-3 pull-right" draggable="true">
            @if(sizeof($notifications))
                <div class="notification-slider">
                    <h1>Notifications 
                        <span class="minimize"><img src="/assets/images/vault/template/minimize.svg" class="img-responsive" /></span>
                        <span class="maximize"><img src="/assets/images/vault/template/maximize.svg" class="img-responsive" /></span>
                    </h1>
                    <div>
                    @foreach($notifications as $notification)
                        <a href="{{$notification->note_link}}" class="col-xs-12 padding-0" target="_blank">
                            @switch( $notification->priority )
                                @case("urgent")
                                    <em class="urgent">Urgent</em>
                                    @break
                                @case("important")
                                    <em class="important">Important</em>
                                    @break
                                @case("general")
                                    <em class="notice">Notice</em>
                                    @break
                                @case("announcement")
                                    <em class="annoucement">Annoucement</em>
                                    @break
                            @endswitch
                            @if($notification->note_image !== null && $notification->note_image !== "")
                                <img src="http://notify.monzamedia.com/{{$notification->note_image}}" class="img-responsive hidden-xs hidden-sm" />
                                @if($notification->note_image_mobile !== null && $notification->note_image_mobile !== "")
                                    <img src="http://notify.monzamedia.com/{{$notification->note_image_mobile}}" class="img-responsive hidden-md hidden-lg" />
                                @endif
                            @endif
                            <h2>{{$notification->note_title}}</h2>
                            <p>{{ str_limit(strip_tags($notification->note_description), 125) }}</p>
                            @if($notification->note_link != null && $notification->note_link != "")
                                <span>read more...</span>
                            @endif
                        </a>
                    @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        $(".notification-slider > div").slick({
            arrows: false, 
            accessibility: false, 
            autoplay: true, 
            autoplaySpeed: 2500, 
            arrows: false, 
            dots: true, 
            pauseOnFocus: false, 
            pauseOnHover: false, 
            pauseOnDotsHover: false, 
            speed: 500
        });

        $(".notification-slider .minimize").click(function(){
            $(this).fadeOut(0);
            $(".notification-slider .maximize").fadeIn(0);

            $(".notification-slider > div").slideToggle(500);

            $(".notification-slider").toggleClass('mini');
        });

        
        $(".notification-slider .maximize").click(function(){
            $(this).fadeOut(0);
            $(".notification-slider .minimize").fadeIn(0);

            $(".notification-slider > div").slideToggle(500);

            $('.notification-slider > div')[0].slick.refresh()
            $(".notification-slider").toggleClass('mini');
        });
    </script>
@endsection
