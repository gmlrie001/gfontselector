<header class="" data role="header">

  <div class="branding" role="branding">
    <figure class="branding-logo">
      <source srcset="..." media="max-width: 575px">
      <source srcset="..." media="max-width: 767px">
      <source srcset="..." media="max-width: 1023px">
      <img class="img-fluid site-logo" src="" alt="{{ ucwords( $site_settings->site_name ) }} Logo">
    </figure>
  </div>

  <div class="main-nav" role="navigation"></div>

  <div class="social-media" role="navigation"></div>

  <div class="main-ecommerce-nav">
    <div class="main-nav" role="navigation"></div>
    <div class="main-nav" role="navigation"></div>
    <div class="main-nav" role="navigation"></div>
  </div>

</header>




<!-- Decofurn Ecommerce Header -->
<div class="sticky-header stick-me not-sticking" style="position: relative; top: 0px;">
  <header class="container-fluid">
    <div class="row">
        <div class="container">
           <div class="row">
                <a class="col-12 col-md-4 col-lg-3 logo" href="/" title="Decofurn Furniture">
                    <img class="img-fluid d-none d-lg-block" src="/assets/images/logo/blue-logo.svg" alt="Decofurn Furniture">
                    <img class="img-fluid d-block d-lg-none" src="/assets/images/mobile_logo/white-logo.svg" alt="Decofurn Furniture">
                </a>
                <div class="col-12 col-md-8 col-lg-9 header-block">
                    <div class="row simple-nav">
                        <div class="col-12">
                            <a class="share-open" href="#">
                                <img class="img-responsive" src="/assets/images/template/share-icon.svg">
                            </a>
                            <a class="cart-open" href="/cart/view">
                                0 ITEMS
                                <img class="img-responsive" src="/assets/images/template/cart-icon-item.svg">
                            </a>
                            <a class="login-link" href="/my-account">REGISTER</a>
                            <a class="login-link" href="/my-account">LOGIN</a>
                        </div>
                      </div>
                      <div class="row quick-links">
                        <div class="col-12">
                            <a href="tel:087 740 1800">
                                <strong>NATIONAL HOTLINE</strong>
                                <span>087 740 1800</span>
                            </a>
                            <a href="/store-locator">
                                <strong>STORE LOCATOR</strong>
                                <span>click here to find a store</span>
                            </a>
                            <a href="/decofurn-credit-application">
                                <strong>OPEN YOUR ACCOUNT TODAY</strong>
                                <span>apply for Decofurn Credit</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobile-controller">
                    <a class="nav-open" href="#">
                        <img class="img-responsive" src="/assets/images/template/header/navicon.svg">
                    </a>
                    <a class="search-open" href="#">
                        <img class="img-responsive" src="/assets/images/template/header/search-white.svg">
                    </a>
                    <a class="share-open" href="#">
                        <img class="img-responsive" src="/assets/images/template/share.svg">
                    </a>
                </div>
                <div class="col-12 position-relative no-min-height">
                    <form method="GET" action="https://decofurnsa.co.za/search" accept-charset="UTF-8" class="col-12 search-form-mobile" novalidate="">
                        <input class="form-control" placeholder="Search for products..." name="search" type="text">
                        <button type="submit">
                            Search
                        </button>
                    </form>
                </div>
           </div>
        </div>
    </div>
  </header>

  <div class="mobile-nav-overlay"></div>
  <div class="mobile-nav">
    <h3>Menu <span>X</span></h3>
    <div class="first-nav">
        <a href="/furniture-products" class="open-level-sub open-level-2" data-id="#shop-nav">Shop <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
        <a href="/furniture-products/sale">On Sale</a>
        <a href="/specials" target="_self">FLYER SPECIALS</a>
        <a href="/store-locator" target="_self">STORE LOCATOR</a>
        <a href="/inspiration" target="_self">INSPIRATION</a>
        <a href="/my-account">Register</a>
        <a href="/my-account">Login</a>
        <div class="soc-links">
          <a style="background-color: rgb(66, 103, 178); color: rgb(255, 255, 255);" target="_blank" href="https://www.facebook.com/decofurnsa" class="fa fa-facebook" alt="DecofurnSA on Facebook" title="DecofurnSA on Facebook"></a>
          <a style="background-color: rgb(0, 0, 0); color: rgb(255, 255, 255);" target="_blank" href="http://www.instagram.com/decofurnsa" class="fa fa-instagram" alt="Instagram" title="Instagram"></a>
        </div>
        <div class="level" id="shop-nav">
          <a href="/furniture-products" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> Shop Now</a>
          <a href="/furniture-products/bedroom" class="open-level-sub" data-id="#category_1">BEDROOM <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/living-room-lounge" class="open-level-sub" data-id="#category_2">LIVING ROOM / LOUNGE <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/dining-room-kitchen-bar" class="open-level-sub" data-id="#category_3">DINING ROOM / KITCHEN / BAR <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/patio-outdoor" class="open-level-sub" data-id="#category_4">PATIO / OUTDOOR <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/office" class="open-level-sub" data-id="#category_5">OFFICE <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/home-decor" class="open-level-sub" data-id="#category_6">HOME DECOR <img class="img-responsive" src="/assets/images/template/header/forward-white.svg"></a>
          <a href="/furniture-products/whats-new">WHAT'S NEW</a>
        </div>
        
        <div class="level" id="category_1">
          <a href="/furniture-products/bedroom" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> BEDROOM</a>
          <a href="/furniture-products/bedroom/beds">Beds </a>
          <a href="/furniture-products/bedroom/headboards">Headboards </a>
          <a href="/furniture-products/bedroom/bedroom-sets">Bedroom Sets </a>
          <a href="/furniture-products/bedroom/childrens">Children’s </a>
          <a href="/furniture-products/bedroom/mattresses">Mattresses </a>
          <a href="/furniture-products/bedroom/duvets-pillows-mattress-protectors">Duvets &amp; Pillows &amp; Mattress Protectors </a>
          <a href="/furniture-products/bedroom/pedestals-bedside-tables">Pedestals / Bedside Tables </a>
          <a href="/furniture-products/bedroom/cupboards-robes">Cupboards / Robes </a>
          <a href="/furniture-products/bedroom/chest-of-drawers-mirrors">Chest of Drawers &amp; Mirrors </a>
          <a href="/furniture-products/bedroom/storage-boxes-ottomans">Storage Boxes &amp; Ottomans </a>
          <a href="/furniture-products/bedroom/occasional-chairs-tub-chairs">Occasional Chairs &amp; Tub Chairs </a>
          <a href="/furniture-products/bedroom/sleeper-couches">Sleeper couches </a>
          <a href="/furniture-products/bedroom/lamps-hanging-lights">Lamps / Hanging Lights </a>
        </div>

        <div class="level" id="category_2">
          <a href="/furniture-products/living-room-lounge" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> LIVING ROOM / LOUNGE</a>
          <a href="/furniture-products/living-room-lounge/couches">Couches </a>
          <a href="/furniture-products/living-room-lounge/recliners">Recliners </a>
          <a href="/furniture-products/living-room-lounge/lounge-sets">Lounge Sets </a>
          <a href="/furniture-products/living-room-lounge/occasional-chairs-tub-chairs">Occasional Chairs &amp; Tub Chairs </a>
          <a href="/furniture-products/living-room-lounge/coffee-tables-side-tables">Coffee Tables &amp; Side tables </a>
          <a href="/furniture-products/living-room-lounge/tv-plasma-units">TV / Plasma Units </a>
          <a href="/furniture-products/living-room-lounge/bookcases-multi-unit">Bookcases &amp; Multi Unit </a>
          <a href="/furniture-products/living-room-lounge/screens">Screens </a>
          <a href="/furniture-products/living-room-lounge/sleeper-couches">Sleeper couches </a>
          <a href="/furniture-products/living-room-lounge/rugs">Rugs </a>
          <a href="/furniture-products/living-room-lounge/lamps-hanging-lights">Lamps / Hanging Lights </a>
        </div>

        <div class="level" id="category_3">
          <a href="/furniture-products/dining-room-kitchen-bar" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> DINING ROOM / KITCHEN / BAR</a>
          <a href="/furniture-products/dining-room-kitchen-bar/dining-tables">Dining Tables </a>
          <a href="/furniture-products/dining-room-kitchen-bar/dining-chairs-benches">Dining Chairs &amp; Benches </a>
          <a href="/furniture-products/dining-room-kitchen-bar/dining-room-sets">Dining Room Sets </a>
          <a href="/furniture-products/dining-room-kitchen-bar/sideboards-servers">Sideboards &amp; Servers </a>
          <a href="/furniture-products/dining-room-kitchen-bar/barstools-kitchen-stools">Barstools &amp; Kitchen Stools </a>
        </div>

        <div class="level" id="category_4">
          <a href="/furniture-products/patio-outdoor" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> PATIO / OUTDOOR</a>
          <a href="/furniture-products/patio-outdoor/patio-couches">Patio Couches </a>
          <a href="/furniture-products/patio-outdoor/patio-occasional-chairs">Patio Occasional Chairs </a>
          <a href="/furniture-products/patio-outdoor/outdoor-solid-teak">Outdoor Solid Teak </a>
          <a href="/furniture-products/patio-outdoor/aluminium-outdoor">Aluminium Outdoor </a>
          <a href="/furniture-products/patio-outdoor/outdoor-sets">Outdoor Sets </a>
          <a href="/furniture-products/patio-outdoor/patio-accessories-side-tables">Patio Accessories &amp; Side Tables </a>
          <a href="/furniture-products/patio-outdoor/patio-corner-units">Patio Corner Units </a>
          <a href="/furniture-products/patio-outdoor/outdoorpatio-sunloungers">Outdoor/Patio Sunloungers </a>
        </div>

        <div class="level" id="category_5">
          <a href="/furniture-products/office" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> OFFICE</a>
          <a href="/furniture-products/office/office-chairs">Office Chairs </a>
          <a href="/furniture-products/office/office-desks">Office Desks </a>
          <a href="/furniture-products/office/bookcases-multi-units">Bookcases &amp; Multi Units </a>
        </div>

        <div class="level" id="category_6">
          <a href="/furniture-products/home-decor" class="close-level"><img class="img-responsive" src="/assets/images/template/header/back-grey.svg"> HOME DECOR</a>
          <a href="/furniture-products/home-decor/buddhas">Buddhas </a>
          <a href="/furniture-products/home-decor/baskets">Baskets </a>
          <a href="/furniture-products/home-decor/pots">Pots </a>
          <a href="/furniture-products/home-decor/paintings">Paintings </a>
          <a href="/furniture-products/home-decor/lamps-hanging-lights">Lamps / Hanging Lights </a>
        </div>
      </div>
    </div>

    <nav class="container-fluid">
      <div class="container position-relative">
          <div class="row position-relative">
              <div class="col-12 col-lg-9 navigation">
                  <a href="/furniture-products" class="mego-open">Shop Now</a>
                  <a href="/furniture-products/sale">On Sale</a>
                                      <a href="/specials" target="_self">FLYER SPECIALS</a>
                                      <a href="/store-locator" target="_self">STORE LOCATOR</a>
                                      <a href="/inspiration" target="_self">INSPIRATION</a>
                              </div>
              <form method="GET" action="https://decofurnsa.co.za/search" accept-charset="UTF-8" class="col-12 col-lg-3 search-form" novalidate="">
                  <input class="form-control" placeholder="Search here..." name="search" type="text">
                  <button type="submit">
                      <img class="img-responsive" src="/assets/images/template/header/search-icon.svg">
                  </button>
              </form>
          </div>
          <div class="mega-menu">
              <div class="row ">
                  <div class="col-12 col-lg-9">
                                              <div class="row">
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/bedroom">BEDROOM</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/beds">Beds</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/headboards">Headboards</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/bedroom-sets">Bedroom Sets</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/childrens">Children’s</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/mattresses">Mattresses</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/duvets-pillows-mattress-protectors">Duvets &amp; Pillows &amp; Mattress Protectors</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/pedestals-bedside-tables">Pedestals / Bedside Tables</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/cupboards-robes">Cupboards / Robes</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/chest-of-drawers-mirrors">Chest of Drawers &amp; Mirrors</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/storage-boxes-ottomans">Storage Boxes &amp; Ottomans</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/occasional-chairs-tub-chairs">Occasional Chairs &amp; Tub Chairs</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/sleeper-couches">Sleeper couches</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/bedroom/lamps-hanging-lights">Lamps / Hanging Lights</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/living-room-lounge">LIVING ROOM / LOUNGE</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/couches">Couches</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/recliners">Recliners</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/lounge-sets">Lounge Sets</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/occasional-chairs-tub-chairs">Occasional Chairs &amp; Tub Chairs</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/coffee-tables-side-tables">Coffee Tables &amp; Side tables</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/tv-plasma-units">TV / Plasma Units</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/bookcases-multi-unit">Bookcases &amp; Multi Unit</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/screens">Screens</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/sleeper-couches">Sleeper couches</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/rugs">Rugs</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/living-room-lounge/lamps-hanging-lights">Lamps / Hanging Lights</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/dining-room-kitchen-bar">DINING ROOM / KITCHEN / BAR</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/dining-room-kitchen-bar/dining-tables">Dining Tables</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/dining-room-kitchen-bar/dining-chairs-benches">Dining Chairs &amp; Benches</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/dining-room-kitchen-bar/dining-room-sets">Dining Room Sets</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/dining-room-kitchen-bar/sideboards-servers">Sideboards &amp; Servers</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/dining-room-kitchen-bar/barstools-kitchen-stools">Barstools &amp; Kitchen Stools</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                      </div>
                                              <div class="row">
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/patio-outdoor">PATIO / OUTDOOR</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/patio-couches">Patio Couches</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/patio-occasional-chairs">Patio Occasional Chairs</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/outdoor-solid-teak">Outdoor Solid Teak</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/aluminium-outdoor">Aluminium Outdoor</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/outdoor-sets">Outdoor Sets</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/patio-accessories-side-tables">Patio Accessories &amp; Side Tables</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/patio-corner-units">Patio Corner Units</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/patio-outdoor/outdoorpatio-sunloungers">Outdoor/Patio Sunloungers</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/office">OFFICE</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/office/office-chairs">Office Chairs</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/office/office-desks">Office Desks</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/office/bookcases-multi-units">Bookcases &amp; Multi Units</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/home-decor">HOME DECOR</a></h2>
                                                                              <ul>
                                                                                              <li>
                                                      <a href="/furniture-products/home-decor/buddhas">Buddhas</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/home-decor/baskets">Baskets</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/home-decor/pots">Pots</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/home-decor/paintings">Paintings</a>
                                                  </li>
                                                                                              <li>
                                                      <a href="/furniture-products/home-decor/lamps-hanging-lights">Lamps / Hanging Lights</a>
                                                  </li>
                                                                                      </ul>
                                                                      </div>
                                                      </div>
                                              <div class="row">
                                                              <div class="col-12 col-lg-4">
                                      <h2><a href="/furniture-products/whats-new">WHAT'S NEW</a></h2>
                                                                      </div>
                                                      </div>
                                      </div>
                  <div class="col-12 col-lg-3 images">
                  </div>
              </div>
          </div>
      </div>
  </nav>

  <div class="container-fluid d-block d-lg-none fixed-bottom-nav">
    <div class="row">
        <a class="col-3" href="/">
            <img class="img-fluid" src="/assets/images/template/home.svg">
            <span>Home</span>
        </a>
        <a class="col-3" href="/furniture-products/sale">
            <img class="img-fluid" src="/assets/images/template/sale.svg">
            <span>On Sale</span>
        </a>
        <a class="col-3" href="/profile">
            <img class="img-fluid" src="/assets/images/template/profile.svg">
            <span>My Account</span>
        </a>
        <a class="col-3" href="/cart/view">
            <img class="img-fluid" src="/assets/images/template/cart-m.svg">
            <span>Cart</span>
        </a>
      </div>
    </div>
</div>
