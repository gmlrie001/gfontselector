try {
  var ordering = document.querySelector('#ordering');
  ordering.addEventListener('input', ordering_link, false);
} catch (err) {
  console.warn("\r\nWarning: " + err + "\r\n");
}

function ordering_link(event) {
  if (event == undefined || event == null || typeof (event) != 'object') return;

  var evt = event;
  evt.preventDefault();

  var h;
  h = evt.target.getAttribute('data-base-href');

  var v, i, s, t;
  v = evt.target.value;
  i = evt.target.selectedIndex;
  s = evt.target.children[i];
  t = s.innerText;

  var tmp, url;
  tmp = ['/', h, '/', encodeURI(v)];
  url = tmp.join('');

  delete(evt);
  delete(i);
  delete(s);
  delete(t);
  delete(tmp);

  setTimeout(() => {
    window.location.href = url;
  }, 100);

  return;
}

document.addEventListener(
  'click',
  (event) => {
    event.stopPropagation();
    modal( event );
  },
  false
);


function modal(evt) {
  if ( evt == null || evt == undefined || 
      (typeof( evt ) != 'object' && (typeof( evt ) == undefined ||typeof( evt.type ) !== 'click' ) )
  ) return;

  if (evt.target.classList.contains('close-modal')) {
    var p = evt.target.parentNode.closest('#filter-modal'),
        r = evt.target.parentNode.nextElementSibling;

    p.classList.remove('show');
    r.parentNode.removeChild(r);

    return;
  } else if (evt.target.id === 'filter-modal' ) {
    var p = evt.target;
        r = document.querySelector('#dismiss-modal').nextElementSibling;

    p.classList.remove('show');
    r.parentNode.removeChild(r);

    return;
  } else if (evt.target.classList.contains('mobile-modal-activate')) {
    var m = document.querySelector('#filter-modal'),
        f = document.querySelector('.filters-container'),

    c = f.cloneNode(true);
    m.appendChild(c);

    c.classList.remove('d-none');
    c.classList.add('center-in-modal');
    m.classList.add('show');
    return;
  }
}


const debounce = (func, delay) => {
  let inDebounce;

  return function () {
    const context = this;
    const args = arguments;

    clearTimeout(inDebounce);

    inDebounce = setTimeout(
      () => func.apply(context, args),
      delay
    );
  }
}
/**
 * USAGE:
 debounceBtn.addEventListener('click', debounce(function() {
  console.info('Hey! It is', new Date().toUTCString());
 }, 3000));
;
 */

const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;

  return function () {
    const context = this;
    const args = arguments;

    if (!lastRan) {
      func.apply(context, args)
      lastRan = Date.now()
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout(
        () => {
          if ((Date.now() - lastRan) >= limit) {
            func.apply(context, args)
            lastRan = Date.now()
          }
        },
        limit - (Date.now() - lastRan)
      )
    }
  }
}
/**
 * USAGE:
 throttleBtn.addEventListener('click', throttle(function() {
  return console.log('Hey! It is', new Date().toUTCString());
 }, 1000));
 */

var i = 0;
var o = 11;
var m = 1;
var n = 1;
var es = [].slice.call(document.querySelectorAll('.card.hidden'));
var d = document;
var t = 'link';
window.addEventListener('scroll', throttle(function () {
  var chk = Math.floor(document.querySelector('.cardcols').clientHeight / 10) * 10;
  var offset = window.scrollY + window.innerHeight;
  var height = document.documentElement.offsetHeight;

  if (offset >= height - 200) {
    if (es.slice(i, o).length == 0) {
      window.removeEventListener('scroll', (evt) => {
        evt.preventDefault();
        return;
      }, false);
    }
    es.slice(i, o).forEach((e) => {
      e.classList.remove('hidden');
      // console.log(e);
      var font2load = e.dataset.family,
        variants = e.dataset.variants;
      /* e.children[0].style.fontFamily
        .split(',')[0]
        .toString()
        .replace(/\s+?/ig, '+')
        .replace(/\"/ig, '');*/
      var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
        gfonts_font_sel = font2load,
        gfonts_font_var = variants; //<?php //echo('["' . implode('", "', $font->variants) . '"]');?>;

      var url_build, g, s;
      url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var; //.join(',');

      try {
        g = d.createElement(t),
          g.rel = "stylesheet";
        g.href = url_build + '&display=swap';
        s = d.getElementsByTagName(t)[0];
        s.parentNode.insertBefore(g, s);
      } catch (err) {
        console.clear();
        console.warn('\r\n' + err + '\r\n');
      }
    });
    i += 3 * m + 1;
    o += i + 4;
    m++;
    n += 0.25;
  }
}, 50), false);

function live_search() {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById('font-search');
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('article');
  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    a = li[i]; //.getElementsByTagName("a")[0];
    txtValue = a.getAttribute('data-font-family'); //a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
      li[i].style.backgroundColor = "#1a1a1a";
      li[i].style.color = "#fefefe";
      li[i].style.borderColor = "#fefefe";
      li[i].querySelector('.cta').style.borderColor = "#fefefe";
    } else {
      li[i].style.display = "none";
    }
  }
}
//font-category
function generic(idSel) {
  // Declare variables
  var input, filter, ul, li, a, i, txtValue;

  input = document.getElementById(idSel);
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName('article');

  var n = li.length;
  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < n; i++) {

    a = li[i]; //.getElementsByTagName("a")[0];
    txtValue = a.getAttribute('data-category'); //a.textContent || a.innerText;
    
    console.log(txtValue.toUpperCase());
    
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
      li[i].style.backgroundColor = "#1a1a1a";
      li[i].style.color = "#fefefe";
      li[i].style.borderColor = "#fefefe";
      li[i].querySelector('.cta').style.borderColor = "#fefefe";
    } else {
      li[i].style.display = "none";
    }
  }

}

function font_variant(elem, style) {
  var kindaStyle = style.match(/(\d{3}|\w+)/ig);
  var p = elem.parentNode.closest('.card-body');
  p.style.fontStyle = 'unset';
  p.style.fontWeight = 'unset';
  kindaStyle.forEach((e) => {
    if (isNaN(parseInt(e))) {
      if (e == 'regular') {
        p.style.fontWeight = 'normal';
      } else {
        p.style.fontStyle = e;
      }
    } else {
      p.style.fontWeight = parseInt(e);
    }
  });
}

// ;var font_loader = function(elem, d=document, t='link', family, variants){
//   // return console.log(family, variants);
//   var e = elem;
//   var b = e.getBoundingClientRect();
//   if( b.top > 0 && b.height > 0 ) {
//     var gfonts_url_base = 'http://fonts.googleapis.com/css?family',
//         gfonts_font_sel = family,
//         gfonts_font_var = variants;

//     var url_build, g, s;
//         url_build = gfonts_url_base + '=' + gfonts_font_sel + ':' + gfonts_font_var; //.join(',');

//     try {
//       var fnc = new Function( 'return ' + 'function(t) { return document.createElement(t); }')();
//       g = fnc(t);
//       g.rel = "stylesheet";
//       g.href = url_build + '&display=swap';
//       s = d.getElementsByTagName(t)[0];
//       s.parentNode.insertBefore(g, s);
//       // return console.log( [gfonts_font_sel, gfonts_font_var] );
//     } catch( err ) {
//       console.clear(); console.warn( '\r\n' + err + '\r\n' );
//     }
//   };
//   return;
// };
// var nonHidden = [].slice.call( document.querySelectorAll('article.google_fonts:not(.hidden)') )
// nonHidden.forEach( el => {
//   // var el = l; nonHidden.shift(); console.log( nonHidden );
//   var f = el.dataset.family,
//       c = el.dataset.category,
//       v = el.dataset.variants;
//   font_loader( el, document, 'link', f, v);
// } );
// var formData = new FormData();
// var inputs_title = [].slice.call( document.querySelectorAll('input[type=radio][name=title_font]') );
// input_listeners( inputs_title, 'change' );
// var inputs_body  = [].slice.call( document.querySelectorAll('input[type=radio][name=body_font]') );
// input_listeners( inputs_body, 'change' );
// var inputs_cta   = [].slice.call( document.querySelectorAll('input[type=radio][name=cta_font]') );
// input_listeners( inputs_cta, 'change' );

// function input_listeners(arr, evt) {
//   arr.map( el => {
//     el.addEventListener( evt, (evnt)=>{
//       var elm = evnt.srcElement;

//       // console.log( evnt );
//       // console.log( evnt.srcElement.checked );
//       evnt.stopPropagation();

//       if( ! evnt.srcElement.checked ) return;

//       console.log( formData.has(evnt.srcElement.name) );
//       if ( ! formData.has(evnt.srcElement.name) ) {
//         formData.append(evnt.srcElement.name, evnt.srcElement.value);
//       } else {
//         formData.set(evnt.srcElement.name, evnt.srcElement.value);
//       }
//       console.log( formData.get(evnt.srcElement.name) );
//       var key   = evnt.srcElement.name;
//       var value = evnt.srcElement.value;
//       storeSession(key, value);
//       console.log( fetchStored( key ) );
//       fontSelected(key, value);
//       var dict  = {
//         'title_font': null,
//         'body_font': null,
//         'cta_font': null,
//       }
//       autoSave( key, value );
//     }, false)
//   })
// }

// function storeSession(key, value) {
//   var sess = window.sessionStorage || null;
//   return ( sess && sess != null ) ? sess.setItem(key, value) : null
// }

// function fetchStored(key) {
//   var sess = window.sessionStorage || null;
//   return ( sess != null && sess.key(key) != null ) ? sess.getItem(key) : null;
// }

// function removeStored(key) {
//   var sess = window.sessionStorage || null;
//   if (sess != null && sess.key(key) != null) {
//     sess.removeItem(key);
//     autoSave(key)
//     return !0;
//   }
//   return !1;
// }

// function deSelectFont(event) {
//   try {
//     var element = event.srcElement;
//     var del = element.parentNode.closest('.toast'), 
//         key = del.dataset.fontClass;
//     // var selected = document.querySelector( 'input' + '[type=radio]:checked' + '[value=' + key + ']' );
//     // console.log( selected );
//     // return;
//     // selected.checked = ( removeStored(key) ) ? false : true;
//     // del.parentNode.removeChild( del );
//   } catch( err ) {
//     // console.warn( "\r\n" + err + "\r\n" );
//   } finally {
//     // return;
//   }

// }

// function fontSelected(name, value, active=false) {
//   var toasterMarkup = `
//   <section id="selected-%name%" data-font-class="%name%" class="font-selected toast">
//     <div class="float-right" style="cursor:pointer">
//       <span class="remove-font" onclick="deSelectFont(this);">
//         &times;
//       </span>
//     </div>
//     <div class="wrapper" style="display:flex;flex-direction:row">
//       <div class="toast-header clearfix">
//         <h2>
//           <span class="bigger" style="font-style:italic;text-decoration:none;font-weight:300;">%name%</span> 
//         </h2>
//       </div>
//       <div class="toast-body">
//         <p>
//           <strong>%value%</strong>
//         </p>
//       </div>
//     </div>
//   </section>
//   `;
//   toasterMarkup = toasterMarkup
//                   .replace( /%name%/ig, name )
//                   .replace( /%value%/ig, value );

//   return document.querySelector('.vault-content').appendChild(
//     document.createRange().createContextualFragment(
//       toasterMarkup
//     )
//   ) || null;
// }

// function autoSave(name=null, value=null) {
//   var k = name || null,
//       v = value || null;
//   if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
//     var xhr = new XMLHttpRequest();
//   } else if (window.ActiveXObject) { // IE 6 and older
//     var xhr = new ActiveXObject("Microsoft.XMLHTTP");
//   }

//   // console.log( k, v );
//   // return;

//   postData = new FormData;
//   addFormData( postData, {'_token': "{{ csrf_token() }}"} );
//   addFormData( postData, {'id': 1});
//   addFormData( postData, {'status': 'PUBLISHED'});
//   addFormData( postData, {'status_date': ''});
//   addFormData( postData, {'my_name': ''});
//   nm = document.querySelector('input[name=my_time]').value;
//   addFormData( postData, {'my_time': '"'+nm+'"'});

//   console.log( document.location.href );
//   if ( k != null && typeof( k ) != null ) {
//     if( v != null ) {
//       postData.append( k, v.replace( ' ', '+' ) );
//     } else {
//       postData.append( k, v );
//     }
//     console.log( k );
//     console.log( v );
//   }
//   console.log( postData );
//   // return;

//   xhr.onreadystatechange = function() {
//     if (this.readyState == 4 && this.status == 200) {
//       console.log( this );
//       console.log( this.responseText );
//     }
//   }

//   xhr.open("POST", document.location.href, true);
//   // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//   xhr.setRequestHeader('X-CSRF-TOKEN', "{{ csrf_token() }}");
//   xhr.send( postData );
// }

// function addFormData(form, pair={}) {
//   Object.keys(pair).forEach( k => {
//     postData.append(k, pair[k]);
//   })

// }

// document.addEventListener( 'DOMContentLoaded', function() {
//   if( document.location.pathname.search('fonts') != -1 ) {
//     if ( window.sessionStorage && window.sessionStorage.length > 0 ) {
//       Object.keys(sessionStorage).forEach( (k)=>{
//         fontSelected( k, sessionStorage[k], true );
//         ( function() {
//           try {
//             var seltd = document.querySelector( 'input#' + k + '[value="' + sessionStorage[k] + '"]' ) || null;
//             if ( ! seltd.checked ) seltd.checked = true;
//             return;
//           } catch( err ) { /* */ }
//         } )();
//       } )
//     }
//   }
// } );
