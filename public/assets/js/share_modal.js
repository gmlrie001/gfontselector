$(document).ready(function(){
    $(".share-nav").click(function () {
        $(".share-modal").fadeIn(500);
        $(".share-overlay").fadeIn(500);
    });

    $(".share-close").click(function () {
        $(".share-modal").fadeOut(500);
        $(".share-overlay").fadeOut(500);
    });
});