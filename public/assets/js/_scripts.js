var _debug = false;

function social_media_slider() {
  var modal = document.querySelector(".icon-bar");
  var btn = document.querySelector(".page-social-icon");

  try {
    // Close by clicking on opening element
    btn.onclick = () => {
      modal.classList.toggle("active");
      btn.children[0].classList.toggle("color-inversion");
    }
  } catch (err) {
    /* Error */
  }

  try {
    // Close by clicking anywhere else except opening or opened element
    window.onclick = (event) => {
      event.stopPropagation();
      if (event.target == modal) {
        if (modal.classList.contains("active")) modal.classList.toggle("active");
        if (btn.children[0].classList.contains("color-inversion")) btn.children[0].classList.toggle("color-inversion");
      }
    }
  } catch (err) {
    /* Error */
  }

}

var load_debugger = function (relpathtocss = '') {

  // Properties
  this.stylesheet_location = relpathtocss;
  this.link_css = null;
  // load_debugger.prototype.getHead = () => {
  //   this.sectionHead = document.head;

  //   return this.sectionHead;
  // }
}

load_debugger.prototype.headCSSLink = () => {
  this.link_css = document.createElement('link');
  this.link_css.type = 'type/css';
  this.link_css.rel = 'stylesheet';
  this.link_css.id = 'css_debugging';
  console.log(this.link_css);
  console.log(this.stylesheet_location);
  // return this.link_css;
}
load_debugger.prototype.setCSSLink = () => {
  this.link_css.href = this.stylesheet_location;
  console.log(this.link_css);
  // return this.link_css;
}
load_debugger.prototype.appendCSSToHead = () => {
  this.sectionHead = document.head;
  console.log(this.link_css, this.stylesheet_location);
  this.sectionHead.appendChild(this.link_css);
}
load_debugger.prototype.prependCSSToHead = () => {}

function switchon_debugging() {
  // l = new load_debugger();
  // l.stylesheet_location = '/assets/css/outline_debugger.css';
  // l.headCSSLink();
  // l.appendCSSToHead();
  // n = document.querySelector('#css_debugging')
  // n.href = l.stylesheet_location
  // switch_debugging();
  var giftofspeed = document.createElement('link');
  giftofspeed.id = 'css_debugging';
  giftofspeed.rel = 'stylesheet';
  giftofspeed.href = '/assets/css/outline_debugger.css';
  giftofspeed.type = 'text/css';

  var godefer = document.getElementsByTagName('link')[0];
  godefer.parentNode.insertBefore(giftofspeed, godefer);

  debug_mode = 'ON';
}

function switchoff_debugging() {
  var giftofspeed = document.querySelector('#css_debugging');
  giftofspeed.parentNode.removeChild(giftofspeed);

  debug_mode = 'OFF';
}

function update_element() {
  var nv, err;
  try {
    nv = document.querySelector(".sticky-header");
    if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
      if (!nv.classList.contains("is-sticky")) nv.classList.add("is-sticky");
    } else {
      if (nv.classList.contains("is-sticky")) nv.classList.remove("is-sticky");
    }
  } catch (err) {
    /* Log Error/Warning/Exception */
    /*console.warn("\r\n" + err + "\r\n");*/
  }
}

document.addEventListener("DOMContentLoaded", () => {
  if (_debug) console.log("\r\n" + "DOM Content Load Complete" + "\r\n");

  /* Add drop shadow to sticky navigation after scroll exit header region */
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    update_element();
  }

  /* Scroll Event Listener */
  window.onscroll = function () {
    update_element()
  };

  debug_mode = 'OFF';

  // social_media_slider();
  share_modal();

  reveal_articles(0);
  check_article_load_more();
  $(".btn-loadmore").on('click', function (e) {
    e.preventDefault();
    reveal_articles(6);
    check_article_load_more();
  });
  (function () {
    check_article_load_more();
  })();

  $('.client-ticker-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    speed: 5000,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 0,
    cssEase: 'linear',
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.spinner').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })

  $('.product-image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.product-image-slider-nav'
  });
  $('.product-image-slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.product-image-slider',
    arrows: false
    // centerMode: true,
    // centerPadding: '30px',
    // focusOnSelect: true
  });

  // DESKTOP
  try {
    $('.popup-gallery').magnificPopup({
      type: 'image',
      gallery: {
        enabled: true
      }
    });
  } catch (err) {
    /*Error*/
  }
  // MOBILE
  try {
    $('.popup-gallery-mobile').magnificPopup({
      type: 'image',
      gallery: {
        enabled: true
      }
    });
  } catch (err) {
    /*Error*/
  }


});

function set_debugging(selector) {
  var a, e;
  a = [].slice.call(document.querySelectorAll(selector));
  a.map(e => {
    e.classList.add('debug-mode');
  })
  return;
}

function remove_debugging() {
  var a, e,
    s = 'debug-mode',
    selector = '.' + s;
  a = [].slice.call(document.querySelectorAll(selector));
  a.map(e => {
    e.classList.remove(s);
  })
  return;
}

function debug_quick_show(sel, timer = 5) {
  var timer_ms = timer * 1000;
  set_debugging(sel);
  setTimeout(() => {
    remove_debugging();
  }, timer_ms);
  return;
}

function share_modal() {
  try {
    share = document.querySelector('.share-overlay');
    close = document.querySelector('.share-close');
    close.addEventListener('click', (e) => {
      if (share.classList.contains('show')) share.classList.remove('show');
    }, false);
    window.addEventListener('click', (e) => {
      if (e.target == share) {
        if (share.classList.contains('show')) share.classList.remove('show');
      }
    }, false);
  } catch (err) {}
}

function reveal_articles(n = 3) {

  var articles_to_reveal = $(".hidden").slice(0, n),
    article_revealed;

  console.log(articles_to_reveal)
  /* Reveal 6 Articles by sliding them down */
  // articles_to_reveal.slideDown();
  articles_to_reveal.removeClass('hidden');

  /* Remove hidden class so load more disappears as expected */
  // article_revealed = [].slice.call(articles_to_reveal);
  // article_revealed.map(article => {
  //   article.classList.remove('hidden')
  // });

  return;

}

function check_article_load_more() {

  if ($(".hidden").length == 0) {
    $(".btn-loadmore").addClass('hidden'); //fadeOut('slow');
  }

  return;

}