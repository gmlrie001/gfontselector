<?php namespace App\Http\Middleware;

use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

use App\Models\PageView;

use Closure;
use Config;

class RecordVisitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pageView = new PageView();
        $location = \Location::get();
        $pageView->visitor_ip = $request->getClientIp();
        $pageView->page_url = $request->path();
        $pageView->countryCode = $location->countryCode;
        $pageView->regionName = $location->regionName;
        $pageView->cityName = $location->cityName;
        $pageView->zipCode = $location->zipCode;
        
        dd($pageView);

        $pageView->save();
        
        return $next($request);
    }
}
