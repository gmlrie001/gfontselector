<?php namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get("id");
        return [
        'email' => 'required|email|unique:users,email,'.$id,
        'name' => 'required',
        'my_name'   => 'honeypot',
        'my_time'   => 'required|honeytime:5'
    ];
    }
}
