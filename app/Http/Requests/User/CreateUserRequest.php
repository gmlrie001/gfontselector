<?php namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'email' => 'required|email|unique:users',
        'password' => 'required|min:8',
        'name' => 'required',
        'user_group_id' => 'required|numeric',
        'user_group' => 'required',
        'my_name'   => 'honeypot',
        'my_time'   => 'required|honeytime:5'
    ];
    }
}
