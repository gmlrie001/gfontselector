<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_name' => 'required|string',
            // 'site_domain' => 'filled|string',
            // 'logo' => 'dimensions:min_width=280,min_height=95',
            // 'mobile_logo' => 'dimensions:min_width=120,min_height=40',
            // 'footer_logo' => 'required',
            'copyright' => 'required|string',
            // 'tax_rate' => 'filled|numeric',
            'my_name'   => 'honeypot',
            'my_time'   => 'required|honeytime:0',
            // 'seo_keywords' => 'required|string',
            // 'seo_title' => 'required|string',
            // 'seo_description' => 'required|string',
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'site_name.required' => 'Site Name must not be blank.',
            'site_name.string' => 'Site Name must be text.',
            // 'site_domain.filled' => 'Site Domain must not be blank.',
            // 'site_domain.string' => 'Site Domain must be text.',
            // 'logo.required' => 'Logo is required',
            // 'mobile_logo.required' => 'Mobile Logo is required',
            // 'logo.dimensions' => 'Logo dimensions should be 280px by 95px',
            // 'mobile_logo.dimensions' => 'Mobile Logo dimensions should be 120px by 40px',
            // 'footer_logo.required' => 'Footer Logo is required',
            'copyright.required' => 'Copyright must not be blank.',
            'copyright.string' => 'Copyright must be text.',
            // 'tax_rate.filled' => 'Tax Rate must not be blank.',
            // 'tax_rate.numeric' => 'Tax Rate must be a number.',
            'my_time.required' => '',
            'my_name.honeypot'  => '',
            'my_time.honeytime'  => '',
            // 'seo_keywords' => 'required|string',
            // 'seo_title' => 'required|string',
            // 'seo_description' => 'required|string',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            // 'email' => 'trim|lowercase',
            'site_name' => 'trim|capitalize|escape'
        ];
    }
}
