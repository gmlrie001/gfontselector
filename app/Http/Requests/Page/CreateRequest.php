<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\ImplicitRule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'           => 'required|string',
            'description'     => 'nullable|string',
            'seo_title'       => 'required|string',
            'seo_description' => 'required|string',
            'seo_keywords'    => 'required|regex:/^(.+,?.+)?$/isgU',
            'my_name'         => 'honeypot',
            'my_time'         => 'required|honeytime:0'
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'           => 'Title must not be blank.',
            'title.string'             => 'Title must be text.',
            'seo_title.required'       => 'SEO Title must not be blank.',
            'seo_title.string'         => 'SEO Title must be text.',
            'seo_description.required' => 'SEO Description must not be blank.',
            'seo_description.string'   => 'SEO Description must be text.',
            'seo_keywords.required'    => 'SEO Keywords must not be blank.',
            'seo_keywords.regex'       => 'SEO Keywords must a comma separated list.',
            'my_time.required'         => '',
            'my_name.honeypot'         => '',
            'my_time.honeytime'        => ''
        ];
    }
}
