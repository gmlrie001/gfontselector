<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Model Includes */

/* Request Includes */

class OrderFieldController extends VaultController
{
    public function __invoke($direction, $field)
    {
        Session::put('order.field', $field);

        Session::put('order.direction', $direction);
        
        return Redirect::back();
    }
}
