<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

use TomLingham\Searchy\Facades\Searchy;

use Illuminate\Http\Request;

class SearchController extends VaultController {

    
    public function search($tableName, Request $request)
    {
        $model = '\\App\\Models\\' . studly_case(\str_singular($tableName));
        $searchValue = Input::get('search');
        $columns = Schema::getColumnListing($tableName);

        $search_results = $model::hydrate(
            Searchy::$tableName($columns)
            ->query($searchValue)->having('relevance', '>', 1)
            ->get()->toArray()
        );

        if (sizeof($search_results)) {
            Session::put([
                'search_results' => $search_results,
                'tableName'      => $tableName
            ]);
            return Redirect::back()->with('search_results', $search_results)->with('tableName', $tableName);
        } else {
            return Redirect::back()->with('error', 'No results found');
        }
    }

    public function searchTrash($tableName, Request $request)
    {
        $model = '\\App\\Models\\' . studly_case(\str_singular($tableName));
        $searchValue = Input::get('search');
        $columns = Schema::getColumnListing($tableName);
        $search_results = $model::hydrate(Searchy::$tableName($columns)
                                ->onlyTrashed()->query($searchValue)->having('relevance', '>', 1)->get()->toArray());
    
        if (sizeof($search_results)) {
            Session::put([
                'search_results' => $search_results,
                'tableName'      => $tableName
            ]);
            return Redirect::back()->with('search_results', $search_results)->with('tableName', $tableName);
        } else {
            return Redirect::back()->with('error', 'No results found');
        }
    }


}