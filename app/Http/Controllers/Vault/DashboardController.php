<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/* Model Includes */

/* Request Includes */

class DashboardController extends VaultController {

    public function index(){
	    $this->data['slides'] = DB::connection('monza_external')->table('vault_banners')->select('title', 'slide_link', 'slide_image')->where('active', 1)->orderBy('order', 'asc')->get();
    
        $url = substr(strstr(URL::to('/'), '//'), 2);

        $url = "http://notify.monzamedia.com/get/notifications/".$url;
        //dd( $url );
        //  Initiate curl
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
        // Execute
        $result=curl_exec($ch);
        // Closing
        curl_close($ch);

        $notifications = json_decode($result);
        // Will dump a beauty json :3
        
        $this->data['notifications'] = collect($notifications->data);

        return view('vault.dashboard.dashboard', $this->data);
    }
}