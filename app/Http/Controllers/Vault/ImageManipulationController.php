<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ImageManipulationController extends VaultController {
    
    public function tempImageUpload(){
        $imagePath = "assets/images/temp/";
        $imagePath_post = "/assets/images/temp/";

        if(!file_exists($imagePath)){
            mkdir($imagePath, 0777, true);
        }
            
        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);        
           
        //Check write Access to Directory
    
        if(!is_writable($imagePath)){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t upload File; no write Access'
            );
            
            print json_encode($response);
            
            return;
        }
        
        if(in_array($extension, $allowedExts)){
            if($_FILES["img"]["error"] > 0){
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );
            }else{
                $filename = $_FILES["img"]["tmp_name"];
                $new_filename = $temp[0]."_".rand().'.'.$extension;
                list($width, $height) = getimagesize( $filename );
    
                move_uploaded_file($filename,  $imagePath.$new_filename); //$_FILES["img"]["name"]
    
                $response = array(
                    "status" => 'success',
                    "url" => $imagePath_post.$new_filename,
                    "width" => $width,
                    "height" => $height,  
                );
            }
        }else{
            $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
        }
          
        print json_encode($response);
            
    }
       
    
    public function imageCrop($dir){     
        
        $imgUrl = ltrim($_POST['imgUrl'], '/');            
             
        $img_info = explode(".", basename($imgUrl));
        
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        // offsets
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        // crop box
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        // rotation angle
        $angle = $_POST['rotation'];

	    $jpeg_quality = 100;

	    $imagePath = "assets/images/{$dir}"."/";
	
	    if(!file_exists($imagePath)){
	        mkdir($imagePath, 0777, true);
	    }
        
        $imagePath = $imagePath.$img_info[0].'_';
        $output_filename = $imagePath.date('d-m-y');

	    $type = '.'.$img_info[1];

        if(!is_writable(dirname($output_filename.$type))){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t write cropped File'
            );
        }else{
            $img = Image::make($imgUrl);
            
            $img->encode($img_info[1], 100);
            
            $img->resize($imgW, $imgH);
                    
            $img->crop($cropW, $cropH, $imgX1, $imgY1);
            
            $img->save($output_filename.$type);
            
            $response = Array(
                "status" => 'success',
                "url" => $output_filename.$type
            );
        }
	
	    print json_encode($response);
    }

    public function imageUpload($dir, Request $request) {
        if($request->file('image') !== null){
			if($request->file('image')->isValid()){
                $imageName = $request->file('image')->getClientOriginalName();
                
                Input::file('image')->move("assets/images/".$dir."/",Input::file('image')->getClientOriginalName());
        
                $location = "assets/images/".$dir."/".$imageName;
			}else{
                return Response::json(array(
                    'location' => '', 
                    'success' => false
                ));
            }
		}else{
            return Response::json(array(
                'location' => '', 
                'success' => false
            ));
        }

        return Response::json(array(
            'location' => $location, 
            'success' => true
        ));
    }

    public function removeImage($table, $field, $id){
        DB::table($table)->where('id', $id)->update([$field => '']);
		
		return Redirect::back()->with('message', 'Successfully removed '.studly_case($field));
    }
}