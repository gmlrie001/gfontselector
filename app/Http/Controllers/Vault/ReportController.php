<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

use App\Models\PageView;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends VaultController {
    public function pageViews(){
        // $page_views = PageView::select('page_url', DB::raw('count(visitor) visits'),DB::raw('YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day'))->groupBy('page_url', 'year', 'month', 'day')->get();
        $dates = PageView::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day'))->groupBy('year', 'month', 'day')->get();

        // $position = \Location::get('197.229.3.22');

        $this->data['pages'] = PageView::select('page_url')->groupBy('page_url')->pluck('page_url', 'page_url');
        $this->data['countries'] = PageView::select('countryCode')->groupBy('countryCode')->pluck('countryCode', 'countryCode');
        
        $this->data['all_visits'] = PageView::count();
        $this->data['distinct_visits'] = sizeof(PageView::select('visitor_ip')->groupBy('visitor_ip')->get());
        $this->data['distinct_countries'] = sizeof(PageView::select('countryCode')->groupBy('countryCode')->get());
        
        $data = \Lava::DataTable();

        $data->addDateColumn('Date')->addNumberColumn('Visits');
        foreach($dates as $date){
            $rowData = [$date->year."-".$date->month."-".$date->day];
            $rowData[] = PageView::whereDate('created_at', '=', date($date->year."-".$date->month."-".$date->day))->count();
            $data->addRow($rowData);
        }
        
        \Lava::AreaChart('Views', $data, [
            'title' => 'Page Views Overview',
            'animation' => [
                'startup' => true,
                'easing' => 'inAndOut'
            ],
            'colors' => ['blue', '#F4C1D8']
        ], 'views-div');

        return view('vault.reports.page_views', $this->data);
    }

    public function filterPageViews(Request $request){
        $filter = $request->filter;
        
        $dates = explode(' - ', $request->date_range);
        
        $pages = $request->pages;
        
        $locations = $request->locations;
        
        // $page_views = PageView::select('page_url', DB::raw('count(visitor) visits'),DB::raw('YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day'))->groupBy('page_url', 'year', 'month', 'day')->get();
        $dateList = PageView::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day'))->whereBetween('created_at', [new Carbon($dates[0]), new Carbon($dates[1])])->groupBy('year', 'month', 'day')->get();
        
        if(sizeof($pages)){
            $pageList = PageView::select('page_url')->whereIn('page_url', $pages)->whereBetween('created_at', [new Carbon($dates[0]), new Carbon($dates[1])])->groupBy('page_url')->get();
            $this->data['show_pages'] = true;

            
            $pageData = \Lava::DataTable();
            $pageData->addDateColumn('Date');
            foreach($pageList as $pg){
                $pageData->addNumberColumn($pg->page_url);
            }

            foreach($dateList as $dt){
                $pageRowData = [$dt->year."-".$dt->month."-".$dt->day];
                foreach($pageList as $pg){
                    if($filter == 0){
                        $pageRowData[] = PageView::where('page_url', $pg->page_url)->whereDate('created_at', '=', date($dt->year."-".$dt->month."-".$dt->day))->count();
                    }else{
                        $pageRowData[] = sizeof(PageView::select('page_url')->where('page_url', $pg->page_url)->whereDate('created_at', '=', date($dt->year."-".$dt->month."-".$dt->day))->groupBy('page_url')->get());
                    }
                }
                $pageData->addRow($pageRowData);
            }
            
            \Lava::ColumnChart('PageViews', $pageData, [
                'title' => 'Page Views',
                'animation' => [
                    'startup' => true,
                    'easing' => 'inAndOut'
                ],
                'colors' => ['blue', '#F4C1D8']
            ], 'pages-div');
        }else{
            $this->data['show_pages'] = false;
        }
        
        if(sizeof($locations)){
            $locationList = PageView::select('countryCode')->whereIn('countryCode', $locations)->whereBetween('created_at', [new Carbon($dates[0]), new Carbon($dates[1])])->groupBy('countryCode')->get();
            $this->data['show_locations'] = true;

            
            $locationData = \Lava::DataTable();
            $locationData->addDateColumn('Date');
            foreach($locationList as $loc){
                $locationData->addNumberColumn($loc->countryCode);
            }

            foreach($dateList as $dt){
                $locRowData = [$dt->year."-".$dt->month."-".$dt->day];
                foreach($locationList as $loc){
                    if($filter == 0){
                        $locRowData[] = PageView::where('countryCode', $loc->countryCode)->whereDate('created_at', '=', date($dt->year."-".$dt->month."-".$dt->day))->count();
                    }else{
                        $locRowData[] = sizeof(PageView::select('countryCode')->where('countryCode', $loc->countryCode)->whereDate('created_at', '=', date($dt->year."-".$dt->month."-".$dt->day))->groupBy('countryCode')->get());
                    }
                }
                $locationData->addRow($locRowData);
            }
            
            \Lava::ColumnChart('LocViews', $locationData, [
                'title' => 'Location Views',
                'animation' => [
                    'startup' => true,
                    'easing' => 'inAndOut'
                ],
                'colors' => ['blue', '#F4C1D8']
            ], 'locs-div');
        }else{
            $this->data['show_locations'] = false;
        }

        $data = \Lava::DataTable();

        $data->addDateColumn('Date')->addNumberColumn('Visits');
        foreach($dateList as $date){
            $rowData = [$date->year."-".$date->month."-".$date->day];
            $rowData[] = PageView::whereDate('created_at', '=', date($date->year."-".$date->month."-".$date->day))->count();
            $data->addRow($rowData);
        }
        
        \Lava::AreaChart('Views', $data, [
            'title' => 'Page Views Overview',
            'animation' => [
                'startup' => true,
                'easing' => 'inAndOut'
            ],
            'colors' => ['blue', '#F4C1D8']
        ], 'views-div');

        $this->data['pages'] = PageView::select('page_url')->groupBy('page_url')->pluck('page_url', 'page_url');
        $this->data['countries'] = PageView::select('countryCode')->groupBy('countryCode')->pluck('countryCode', 'countryCode');
        
        $this->data['all_visits'] = PageView::count();
        $this->data['distinct_visits'] = sizeof(PageView::select('visitor_ip')->groupBy('visitor_ip')->get());
        $this->data['distinct_countries'] = sizeof(PageView::select('countryCode')->groupBy('countryCode')->get());
        
        return view('vault.reports.page_views_post', $this->data);
    }
}