<?php namespace App\Http\Controllers\Vault;

/* Base Controller Include */
use App\Http\Controllers\Services\VaultController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Exports\TableExport;
use App\Exports\TableTrashExport;
use App\Exports\TableSectionExport;
use App\Exports\TableTrashSectionExport;
use PDF;

/* Model Includes */

/* Request Includes */

class ExportController extends VaultController {
	
    public function createPdf($tableName) {
        $data = DB::table($tableName)->where('deleted_at', null)->get();
        $columns = Schema::getColumnListing($tableName);
        
        $pdf = PDF::loadView('vault.export.pdf', array('data' => $data, 'columns' => $columns));
        return $pdf->setPaper('a4', 'landscape')->download($tableName.'.pdf');
    }

    public function createTrashPdf($tableName) {
        $data = DB::table($tableName)->where('deleted_at', '!=', null)->get();
        $columns = Schema::getColumnListing($tableName);
        
        $pdf = PDF::loadView('vault.export.pdf', array('data' => $data, 'columns' => $columns));
        return $pdf->setPaper('a4', 'landscape')->download($tableName.'.pdf');
    }

	public function createExcel($tableName) {
		return (new TableExport($tableName))->download($tableName.'.xlsx');                             
    }

	public function createTrashExcel($tableName) {
		return (new TableTrashExport($tableName))->download($tableName.'.xlsx');                             
    }

    public function createSelectedPdf($tableName) {
        $selectedItems = explode(',', Input::get('ids'));
        $data = DB::table($tableName)->whereIn('id', $selectedItems)->get();
        
        $columns = Schema::getColumnListing($tableName);
        
        $pdf = PDF::loadView('vault.export.pdf', array('data' => $data, 'columns' => $columns));
        return $pdf->setPaper('a4', 'landscape')->download($tableName.'.pdf');
    }

	public function createSelectedExcel($tableName) {
        $selectedItems = explode(',', Input::get('ids'));
		return (new TableSectionExport($tableName, $selectedItems))->download($tableName.'.xlsx');                             
    }

    public function createTrashSelectedPdf($tableName) {
        $selectedItems = explode(',', Input::get('ids'));
        $data = DB::table($tableName)->whereIn('id', $selectedItems)->get();
        
        $columns = Schema::getColumnListing($tableName);
        
        $pdf = PDF::loadView('vault.export.pdf', array('data' => $data, 'columns' => $columns));
        return $pdf->setPaper('a4', 'landscape')->download($tableName.'.pdf');
    }

	public function createTrashSelectedExcel($tableName) {
        $selectedItems = explode(',', Input::get('ids'));
		return (new TableTrashSectionExport($tableName, $selectedItems))->download($tableName.'.xlsx');                             
    }
}