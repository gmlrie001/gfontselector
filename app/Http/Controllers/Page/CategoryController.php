<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

/* Illuminate Support */
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

use App\Repositories\Repository;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\Category;
use App\Models\Collection as Product;
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;

/* Custom Helper Functions */
use App\Helpers\Strings\StringHelper;
use App\Helpers\DateTime\DateHelper;

/* Custom Traits For Vault Models */
use App\Traits\SEO\SEOsTraits;
use App\Helpers\MiscHelpers;

class CategoryController extends PageController
{
    use SEOsTraits;
    private $settings;

    public function __construct()
    {
        parent::__construct();

        $this->data['page'] = Page::find(6);
    }

    public function __invoke()
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners']  = $banners->get();
                
        $collections = ( new Repository(new Category) )->all();
        $this->data['collections'] = Category::whereCategoryId(null)->status()->orderBy('order', 'asc')->get();

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);

        // SETUP this section\'s SEO information.
        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        $this->data['page']->title = 'Collections';

        unset($sm, $banners, $this->data['share_links'], $faqs);

        if (! Session::has('collection_go_back_url')) {
            Session::put('collection_go_back_url', URL::full());
        } else {
            Session::forget('collection_go_back_url');
        }
        Session::put('collection_go_back_url', URL::full());

        return view('pages.collections.collections', $this->data);
    }


    public function category($category_url_title)
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners']  = $banners->get();
                
        $this->data['category'] = Category::whereUrlTitle($category_url_title)->first();
        $this->data['category']->load('children', 'categories', 'collections');
        // $collections = Product::where('category_id', $this->data['category']->id)->status()->orderBy('order', 'asc')->get();
        foreach ($this->data['category']->collections as $key=>$col) {
            $tmp = Product::findOrFail($col->collection_id);
            $this->data['category']->collections[$key]->listing_image = $tmp->listing_image;
            $this->data['category']->collections[$key]->url_title = $tmp->url_title;
            $collections[$key] = $this->data['category']->collections[$key];
        }
        $this->data['collections'] = $collections;

        // SETUP this section\'s SEO information.
        $this->data['seo'] = $this->setup_SEO($this->data['page']);
        $this->data['page']->title = (null !== $this->data['category']) ? $this->data['category']->title : null;

        if (! Session::has('collection_go_back_url')) {
            Session::forget('collection_go_back_url');
        }
        Session::put('collection_go_back_url', URL::full());

        return view('pages.collections.category', $this->data);
    }
}
