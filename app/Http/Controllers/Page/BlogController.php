<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/* Illuminate Support */
use Illuminate\Support\Collection;

use App\Repositories\Repository;

/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;
use App\Models\Blog;

/* Custom Helper Functions */
use App\Helpers\Strings\StringHelper;
use App\Helpers\DateTime\DateHelper;

/* Custom Traits For Vault Models */
use App\Traits\SEO\SEOsTraits;
use App\Helpers\MiscHelpers;

class BlogController extends PageController
{
    use SEOsTraits;
    private $settings;

    public function __construct()
    {
        parent::__construct();

        $this->data['page'] = Page::find(7);
    }

    public function __invoke()
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $sm = ['facebook', 'twitter', 'pinterest', 'whatsapp', 'email'];
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners']  = $banners->get();
        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);

        $blogs = ( new Repository(new Blog) )->all();
        $this->data['blogs'] = $blogs;

        $this->data['page']->title = 'Blog';
        // SETUP this section\'s SEO information.
        $this->data['seo'] = $this->setup_SEO($this->data['page']);

        unset($sm, $tmp, $banners, $blogs, $this->data['share_links']);

        // dd($this->data, get_defined_vars());

        return view('pages.blogs.listing', $this->data);
    }

    public function view($url_title)
    {
        $sm = $this->data['share_links']->pluck('platform')->toArray();
        $this->data['item_sharing'] = collect(Parent::_socshr($this->data['page'], $sm));

        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);
        $this->data['banners']  = $banners->get();
        $banners = PageBanner::status()->where('page_id', $this->data['page']->id);

        $blog = Blog::status()->where('url_title', $url_title)->first();
        $this->data['blog'] = $blog->load('galleries');

        // SETUP this section\'s SEO information.
        $this->data['seo'] = $this->setup_SEO($this->data['blog']);

        $this->data['page']->seo_title = $blog->title;
        unset($sm, $banners, $this->data['share_links']);

        $sm  = $blog->share_links->sortBy('order')->pluck('platform')->toArray();
        $sm = ['facebook', 'twitter', 'pinterest', 'whatsapp', 'email'];
        $tmp = collect(parent::_socshr($this->data['blog'], $sm));
        $this->data['sharing_links'] = $tmp->reverse();

        unset($sm, $tmp, $blog);

        $locs = Blog::status()->orderBy('pubdate', 'desc')->get();

        $p    = ($this->data['blog']->order - 1 < 1) ? (count($locs) - 1): -1;
        $prev = Blog::status()->where('order', $this->data['blog']->order + $p)->first();
        $prev = Blog::status()->where('pubdate', '<', $this->data['blog']->pubdate)->first();

        $n    = ($this->data['blog']->order + 1 > count($locs)) ? -(count($locs) - 1): 1;
        $next = Blog::status()->where('order', $this->data['blog']->order + $n)->first();
        $next = Blog::status()->where('pubdate', '>', $this->data['blog']->pubdate)->first();

        $this->data['prev_article'] = $prev;
        $this->data['next_article'] = $next;

        unset($locs, $p, $prev, $n, $next);


        return view('pages.blogs.article', $this->data);
    }
}
