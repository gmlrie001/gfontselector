<?php namespace App\Http\Controllers\Page;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;
use App\Http\Controllers\Page\ContactController;

//use Mailchimp;
// use \DrewM\MailChimp\MailChimp;

/* Facade Includes */
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
/* HTTP Requests */
use Illuminate\Http\Request;

/* Model Includes */
// use App\Models\ContactSetting;
use App\Models\ContactEnquiry;
use App\Models\Page;
use App\Models\PageBanner;
use App\Models\PageBannerBlock;
use App\Models\Site;
use App\Models\Social;
use App\Models\ShareLink;

//use App\Http\Controllers\Page\ContactController;
use App\Http\Requests\Enquiry\CreateRequest;

/* Google ReCaptcha Helper/Trait */
// use App\Helpers\GoogleReCaptcha\GoogleRecaptchaHelper;
use App\Helpers\MiscHelpers;

class EnquiryController extends ContactController
{
    public $settings;
    public $site;
    public $subject;
    public $key;
    public $send_to;
    // protected $send_to;

    public function __construct()
    {
        parent::__construct();

        $this->site       = Site::first();
        $this->miscHelper = new MiscHelpers();

        $this->key      = '6Lchza4UAAAAACglaxBW5Z-8jsvAORBKSoQsyz7C';
        $this->send_to  = 'm.riedaa.gamieldien@gmail.com';
        $this->subject  = 'Website Enquiry';
    }
    
    public function enquiry(Request $request)
    {
        $siteLogo = $this->site->logo;
        $logoPath = $this->miscHelper->convert_to_base64($siteLogo);
        // $image    = "<img class='img-fluid' src='" . $logoPath . "' alt='Site Logo'>";
        $logoPath = $siteLogo;

        $send_to  = $this->send_to;
        $subject  = $this->subject;

        /* Communicate with Google ReCaptcha API servers to verify this ReCaptcha */
        // $this->google_recaptcha = new GoogleRecaptchaHelper();
        // $_gr = $this->google_recaptcha->googleRecaptchaServer($request->all(), [], $this->key);
        $_gr = true;

        if ($_gr) {
            $enquiry = $this->save_enquiry($request);
            Mail::send(
                'emails.brombacher.contact',
                ['enquiry' => $enquiry, 'subject' => $subject, 'logo' => $logoPath ],
                function ($m) use ($send_to, $subject) {
                    $m->to($send_to)->subject($subject);
                }
            );
            return Redirect::to(route('contact'))->with('message', 'Successfully sent your contact enquiry.');
        } else {
            return Redirect::to(route('contact'))->with('error', 'Message sending failed. Please try again or contact us by phone or sending us an email.<br>ERROR: Contact Enquiry Mail success _GR but mail::send error.');
        }
    }

    private function save_enquiry($request)
    {
        $enquire        = ContactEnquiry::create($request->all());
        $enquire->order = ContactEnquiry::count();
        $enquire->save();

        return $enquire;
    }

    private function mailer(...$opts)
    {
        $options = $opts[0];

        $enquiry  = $options['enquiry'];
        $logoPath = $options['logo'];
        $subject  = $options['subject'];
        $send_to  = $this->send_to;

        // try {
        /** DEBUG **/
      //   return view('emails.brombacher.enquiry', ['enquiry' => $enquiry, 'subject' => $subject, 'logo' => $logoPath]);
      // } catch( Exception $exception ) {
      //   // Log::error( 'ERROR: Contact Enquiry Mail::send()->error: ' . $exception->getMessage() . "\r\n" );
      //   return Redirect::back()->with( 'error', 'Message sending failed. Please try again or contact us by phone or sending us an email.' );
      // }
    }
}
