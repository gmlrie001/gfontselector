<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageBanner extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "page_id";
    public $parentTable = "pages";
    public $orderOptions = ['title'];
    public $relationships = ['page_banner_blocks' => 'blocks'];
    public $mainDropdownField = "";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'max_columns',
        'page_id',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        // ['max_columns', 'Max Columns', 'select_from_array', ['1' => '1 Column', '2' => '2 Columns', '3' => '3 Columns'], '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_row', 'Status'],
            ['page_id', '', 'parent', '', '', '', '', 'collapse', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', 'Status'],
    ];

    public function blocks()
    {
        return $this->hasMany(PageBannerBlock::class, 'banner_id')->where('status', 'PUBLISHED')
                    ->orWhere('status', 'SCHEDULED')->where('status_date', '<=', now())->orderBy('order', 'asc');
    }

    public function displayBlocks()
    {
        return $this->hasMany(PageBannerBlock::class, 'banner_id')->where('status', 'PUBLISHED')
                    ->orWhere('status', 'SCHEDULED')->where('status_date', '<=', now())->orderBy('order', 'asc');
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
