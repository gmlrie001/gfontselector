<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Social extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title', 'order'];
    public $relationships = [];
    public $mainDropdownField = "social_media_url";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'social_media_url',
        'social_media_icon',
        'social_media_icon_bg_colour',
        'social_media_icon_colour',
        'status',
        'status_date',
    ];
    
    public $fields = [
        //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
            ['title', 'Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['social_media_url', 'URL', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['open_parent', 'Icon Settings', ''],
                ['open_row', '', ''],
                    ['social_media_icon', 'Icon', 'icon_picker', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['close_row', '', ''],
                ['open_row', '', ''],
                    ['social_media_icon_bg_colour', 'Background Colour', 'colour_picker', '', '', '', '', 'col-xs-12 col-md-6', ''],
                    ['social_media_icon_colour', 'Icon Colour', 'colour_picker', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['close_row', '', ''],
            ['close_parent', 'Icon Settings', ''],
            ['open_row', 'Status'],
                ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', 'Status'],
    ];

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
