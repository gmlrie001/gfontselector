<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Config;

class Site extends Model
{
    // Site settings model.

    use SoftDeletes;

    public $orderable = false;
    public $orderField = "site_name";
    public $titleField = "site_name";
    public $statusField = "status";
    public $hasStatus = false;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['site_name', 'copyright'];
    public $relationships = [];
    public $mainDropdownField = "site_name";
    public $imageDropdownField = "logo";
    // public $locale_list;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_name',
        'copyright',

        'logo',
        'logo_mobile',
        'footer_logo',
        'footer_logo_mobile',

        'contact_map_address'

        // 'ssl_logo',
        // 'utility_logo',

        // 'footer_global_email',
        // 'footer_global_number',

        // 'disable_send_emails',

        // 'contact_page_deco_image',
        // 'contact_page_deco_mobile_image',

    ];

    public $fields = [
    // ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['open_parent', 'Site Information', ''],
            ['open_row', '', ''],
                ['site_name', 'Site Name', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'Site Information', ''],

        ['open_parent', 'Site Legal/Copyright', ''],
            ['open_row', '', ''],
                ['copyright', 'Copyright', 'wysiwyg', '', '', '', '', 'col-xs-12', ''],
            ['close_row', '', ''],
        ['close_parent', 'Site Legal/Copyright', ''],

        ['open_parent', 'Site Logo&apos;s', ''],
            ['open_row', '', ''],
                ['logo', 'Main Logo', 'image', '', '', '280', '95', 'col-xs-12 col-md-6 col-lg-6', 'can_remove'],
                ['logo_mobile', 'Main Mobile Logo', 'image', '', '', '120', '40', 'col-xs-12 col-md-6 col-lg-6', 'can_remove'],
            ['close_row', '', ''],
        ['close_parent', 'Site Logo&apos;s', ''],

        ['open_parent', 'Address For Site-Wide Usage', ''],
            ['open_row', '', ''],
                ['contact_map_address', 'Physical Address', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row', '', ''],
        ['close_parent', 'Address For Site-Wide Usage', ''],

        ['open_row', '', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '', ''],
    ];

    /**
    * Get the tracking codes for this site.
    */
    // public function siteTrackingCodes()
    // {
    //     return $this->hasMany('App\Models\SiteTrackingCode');
    // }

    private function _setLocale()
    {
        $this->locales_list    = Config::get('locales.locales_list');
        return ['set_user_prefered_locale', 'Set Locale', 'select_from_array', $this->locales_list, '', '', '', 'col-xs-12 col-md-6', ''];
    }
    private function _setTimezone()
    {
        $this->timezones_list  = Config::get('locales.timezones');
        return ['set_user_prefered_timezone', 'Set Timezone', 'select_from_array', $this->timezones_list, '', '', '', 'col-xs-12 col-md-6', ''];
    }
    private function _setDateFormat()
    {
        $this->dateformat_list = Config::get('locales.date_option_formats');
        return ['set_user_prefered_date_format', 'Set Date Format', 'select_from_array', $this->dateformat_list, '', '', '', 'col-xs-12 col-md-6', ''];
    }
    private function _setTimeFormat()
    {
        $this->timeformat_list = Config::get('locales.time_format_options');
        return ['set_user_prefered_time_format', 'Set Time Format', 'select_from_array', $this->timeformat_list, '', '', '', 'col-xs-12 col-md-6', ''];
    }
}
