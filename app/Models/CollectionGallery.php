<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class CollectionGallery extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "collection_id";
    public $parentTable = "collections";
    public $orderOptions = ['title', 'updated_at'];
    public $relationships = [];
    public $mainDropdownField = "title";
    public $imageDropdownField = "gallery_image";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'gallery_image',
        'gallery_video_link',
        'collection_id',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],

        // ['open_parent', 'Gallery Item Desciption', ''],
        //     ['open_row', '',''],
        //         ['description', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
        //     ['close_row', '',''],
        // ['close_parent', 'Gallery Item Desciption', ''],

        ['open_parent', 'Gallery Image and Video Link Options', ''],
            ['open_row', '',''],
                ['gallery_image', 'Listing', 'image', '', '', '445', '480', 'col-xs-12 col-md-6', 'can_remove'],
                // ['gallery_video_link', 'Item Video Link', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '',''],
        ['close_parent', 'Gallery Image and Video Link Options', ''],

        ['open_row', '',''],
          ['collection_id', '', 'parent', '', '', '', '', 'col-xs-12 col-md-6 d-none collapse hidden', ''],
          ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class, 'collection_id')->withDefault();
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
