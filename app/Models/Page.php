<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title', 'order'];
    public $relationships = [
        'page_banners' => 'banners',
    ];
    public $mainDropdownField = "description";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
        'status_date',
    ];

    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_parent', 'Description', ''],
            ['open_row','',''],
                    ['description', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row','',''],
        ['close_parent', 'Description', ''],

        ['open_parent', 'SEO Information', ''],
            ['open_row', '', ''],
                ['seo_title', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
            ['open_row', '', ''],
                ['seo_description', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['seo_keywords', 'SEO Keywords', 'tags', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'SEO Information', ''],

        ['open_row', 'Status'],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', 'Status'],
    ];

    public function banners()
    {
        return $this->hasMany(PageBanner::class);
    }
    
    public function displayBanners()
    {
        return $this->hasMany(PageBanner::class)->where('status', 'PUBLISHED')
                    ->orWhere('status', 'SCHEDULED')->where('status_date', '>', now())->orderBy('order', 'asc');
    }
    
    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
