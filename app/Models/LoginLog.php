<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip_address', 
        'user_id'
    ];

     /**
     * Get the user for this log.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
