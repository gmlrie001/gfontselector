<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "category_id";
    public $parentTable = "categories";
    public $orderOptions = ['title', 'updated_at'];
    public $relationships = [
        'categories' => 'categories',
        'categories' => 'children',
        'category_collections' => 'collections',
    ];
    public $mainDropdownField = "url_title";
    public $imageDropdownField = "image";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'url_title',
        'description',
        // 'image',
        'category_id',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['url_title', 'URL Title', 'url_title', '', '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_parent', 'Category Parent', ''],
            ['open_row', '',''],
                ['category_id', 'Parent Category', 'select', 'Category', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '',''],
        ['close_parent', 'Category Parent', ''],

        // ['open_parent', 'Category Image Options', ''],
        //     ['open_row', '',''],
        //         ['image', 'Category', 'image', '', '', '800', '600', 'col-xs-12 col-md-6', 'can_remove'],
        //     ['close_row', '',''],
        // ['close_parent', 'Category Image Options', ''],

        ['open_parent', 'SEO Information', ''],
            ['open_row', '', ''],
                ['seo_title', 'SEO Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
            ['open_row', '', ''],
                ['seo_description', 'SEO Description', 'textarea', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['seo_keywords', 'SEO Keywords', 'tags', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'SEO Information', ''],

        ['open_row', '',''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    public function categories()
    {
        return $this->hasMany(self::class, 'category_id')->orderBy('order', 'asc');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'category_id')->with('categories')->orderBy('order', 'asc');
    }

    public function collections()
    {
        return $this->hasMany(CategoryCollection::class, 'category_id')->orderBy('order', 'asc');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'category_id')->withDefault();
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
