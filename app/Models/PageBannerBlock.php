<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageBannerBlock extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "banner_id";
    public $parentTable = "page_banners";
    public $orderOptions = ['title', 'order'];
    public $relationships = [];
    public $mainDropdownField = "description";
    public $imageDropdownField = "banner_image";

    // public $dimensions_override = [
    //     'width'  => '1765px',
    //     'height' => '420px',
    // ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        // 'subtitle',
        'description',
        'column_count',
        'link',
        'link_target',
        'banner_image',
        'mobile_image',
        'banner_id',
        'status',
        'status_date',
    ];
    
    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['title', 'Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        // ['subtitle', 'Sub-Title', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        // ['column_count', 'Column Count', 'column_count', '', '', '', '', 'col-xs-12 col-md-6', ''],

        ['open_parent', 'Banner Image Description', ''],
            ['open_row', '', ''],
                ['description', 'Description', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row', '', ''],
        ['close_parent', 'Banner Image Description', ''],
    
        ['open_parent', 'Link Information', ''],
            ['open_row', '', ''],
                ['link', 'Link', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['link_target', 'Link Target', 'select_from_array', ['_blank' => 'New Tab', '_self' => 'Current Tab'], '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'Link Information', ''],

        ['open_parent', 'Image Options', ''],
            ['open_row', '', ''],
                ['banner_image', 'Banner Image', 'banner_image', '', '', '', '', 'col-xs-12 col-md-6', 'can_remove'],
                ['mobile_image', 'Mobile Image', 'banner_image_mobile', '', '', '', '', 'col-xs-12 col-md-6', 'can_remove'],
            ['close_row', '', ''],
        ['close_parent', 'Image Options', ''],

        ['open_row', 'Status'],
            ['banner_id', '', 'parent', '', '', '', '', 'col-xs-12 col-md-6 collapse hidden', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', 'Status'],
    ];

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '>', now());
    }

    public function scopeOrder($query, $column, $direction)
    {
        $column    = (null !== $column) ? $column : $this->$orderField;
        $direction = (null !== $direction) ? $direction : $this->$orderDirection;

        dd($query->orderBy($column, $direction));

        return $query->orderBy($column, $direction);
    }
}
