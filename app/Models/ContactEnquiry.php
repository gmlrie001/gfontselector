<?php namespace App\Models;

/* Facade Includes */
use Illuminate\Support\Facades\Session;

/* HTTP Requests */
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class ContactEnquiry extends Model
{
    use SoftDeletes;
    
    public $orderable = false;
    public $orderField = "created_at";
    public $titleField = "name";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "desc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['name', 'order', 'created_at'];
    public $relationships = [];
    public $mainDropdownField = "email";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'subject',
        'email',
        'fax',
        'message',
        'status',
        'status_date',
    ];

    /*
    public function owner() {
        return $this->belongsTo( 'App\Models\User', 'id', 'owner_id' );
    }
    */

    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
    ['subject', 'Enquiry Subject', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['open_parent', 'Contact Details', ''],
            [ 'open_row', '', '' ],
                ['name', 'Name', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['phone', 'Contact Number', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['email', 'Email', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['fax', 'Fax', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', '' ],
        ['close_parent', 'Contact Details', ''],

        ['open_parent', 'Message', ''],
            [ 'open_row', '', '' ],
                ['message', 'Message', 'textarea', '', '', '', '', 'col-xs-12', ''],
            ['close_row', '', '' ],
        ['close_parent', 'Message', ''],

        ['open_row', '', '' ],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '', '' ],
    ];

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')->where('status_date', '<=', now());
    }
}
