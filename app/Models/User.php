<?php

namespace App\Models;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $orderable = true;
    public $orderField = "order";
    public $titleField = "name";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['surname', 'name', 'email', 'order'];
    public $relationships = [
        // 'group_users' => 'groupUsers'
    ];
    public $mainDropdownField = "email";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'mobile',
        'user_group_id',
        'admin_type',
        'email',
        'password',
        'subscribed',
        'discount_type',
        'discount',
        'status',
        'status_date',
        'company',
        'specify',
        'reg_no',
        'vat_no',
        'phys_add',
        'post_add'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'active',
        'order',
    ];

    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['name', 'Name', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['surname', 'Surname', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['open_parent', 'Contact Details', ''],
            ['email', 'Email', 'email', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['mobile', 'Mobile', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_parent', 'Contact Details', ''],
        ['open_parent', 'Passwords', ''],
            ['password', 'Password', 'password', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['password_confirmation', 'Confirm Password', 'password', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_parent', 'Passwords', ''],
        // ['open_parent', 'Discount Details', ''],
        //     ['discount', 'Discount', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
        //     ['discount_type', 'Discount Type', 'select_from_array', ['0' => 'Percentage', '1' => 'Fixed Amount'], '', '', '', 'col-xs-12 col-md-6', ''],
        // ['close_parent', 'Discount Details', ''],
        ['open_parent', 'Settings', ''],
            ['open_row', '', ''],
                ['user_group_id', 'User Group', 'select', 'UserGroup', '', '', '', 'col-xs-12 col-md-6', ''],
                ['admin_type', 'Admin Type', 'admin_role', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
            // ['open_row', '', ''],
            //     ['subscribed', 'Subscribed?', 'select_from_array', ['0' => 'No', '1' => 'Yes'], '', '', '', 'col-xs-12 col-md-6', ''],
            // ['close_row', '', ''],
        ['close_parent', 'Settings', ''],
        ['open_row', '', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '', ''],
    ];

    public function setPasswordAttribute($pass)
    {
        if (Hash::needsRehash($pass)) {
            $this->attributes['password'] = Hash::make($pass);
        }
    }

    /**
     * Get the user group associated with the user.
     */
    // public function userGroups()
    // {
    //     return $this->belongsTo(UserGroup::class, 'user_group_id');
    // }

    /**
     * Get the user group associated with the user.
     */
    // public function groupUsers()
    // {
    //     return $this->hasMany(GroupUser::class, 'user_id');
    // }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
