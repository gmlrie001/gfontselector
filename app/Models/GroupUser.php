<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Config;

class GroupUser extends Model
{
    // Site settings model.

    use SoftDeletes;

    public $orderable = true;
    public $orderField = "user_id";
    public $titleField = "user_id";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "desc";
    public $parentOrder = "user_id";
    public $parentTable = "users";
    public $orderOptions = ['user_id', 'group_id'];
    public $relationships = [
      // 'users' => 'user',
      // 'user_groups' => 'groups',
    ];
    public $mainDropdownField = "group_id";
    public $imageDropdownField = "";

    public $locale_list;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'group_id',

        'status',
        'status_date',

    ];

    public $fields = [
    // ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['open_parent', 'User and UserGroups', ''],
            ['open_row', '', ''],
                ['group_id', 'User Groups', 'select', 'UserGroup', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '', ''],
        ['close_parent', 'User and UserGroups', ''],

        ['open_row', '', ''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['user_id', '', 'parent', '', '', '', '', 'col-xs-12 col-md-6 collapse hidden', ''],
        ['close_row', '', ''],
    ];

    /**
     * Get the user group associated with the user.
     */
    public function groups()
    {
        return $this->belongsTo(UserGroup::class, 'group_id');//->withDefault();
    }

    /**
     * Get the user group associated with the user.
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
