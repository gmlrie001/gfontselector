<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class ContactAddress extends Model
{
    use SoftDeletes;
    
    public $orderable = true;
    public $orderField = "order";
    public $titleField = "title";
    public $statusField = "status";
    public $hasStatus = true;
    public $orderDirection = "asc";
    public $parentOrder = "";
    public $parentTable = "";
    public $orderOptions = ['title'];
    public $relationships = [];
    public $mainDropdownField = "address";
    public $imageDropdownField = "";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'primary',
        'address',
        'postal_address',
        'phone',
        'fax',
        'mobile',
        'email',
        'map_image',
        'map_mobile_image',
        'latitude',
        'longitude',
        'contact_map_link',
        'status',
        'status_date',
    ];
    
    /**
     * Effort to ensure future-proof physical and postal addresses
     */

    /*
    street-address
    street-address-2
    street-address-3
    locality
    region
    postal-code
    country
    */


    public $fields = [
    //  ['field_name', 'label', 'field_type', 'options_model', 'options_relationship', 'width', 'height', 'container_class', 'can_remove'],
        ['open_row', '',''],
            ['title', 'Title', 'title', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],

        ['open_parent', 'Contact Details', ''],
            ['open_row', '',''],
                ['email', 'Email', 'email', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['phone', 'Phone', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['fax', 'Fax', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
                ['mobile', 'Mobile', 'text', '', '', '', '', 'col-xs-12 col-md-6', ''],
            ['close_row', '',''],
        ['close_parent', 'Contact Details', ''],

        ['open_parent', 'Physical Address Information', ''],
            ['open_row', '',''],
                ['address', 'Physical Address', 'wysiwyg', '', '', '', '', 'col-xs-12 col-md-12', ''],
            ['close_row', '',''],
        ['close_parent', 'Physical Address Information', ''],

        ['open_row', '',''],
            ['status', 'Status', 'status', '', '', '', '', 'col-xs-12 col-md-6', ''],
        ['close_row', '',''],
    ];

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')
                     ->where('status_date', '<=', now());
    }
}
