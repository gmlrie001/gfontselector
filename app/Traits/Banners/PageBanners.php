<?php namespace App\Traits\Banners;

use Illuminate\Database\Eloquent\Model;

// use App\Models\PageBanner;

trait PageBanners
{
    public function page_banners(Model $model, $attribute_id, $withs='')
    {
        // 'page_id'=>$this->data['page']->id
        $banners = $model::status()->has($withs)->where($attribute_id)->get();
        // dd($model, $attribute_id, $withs, $banners->loadMissing($withs));

        return $banners->load($withs);
    }
}
