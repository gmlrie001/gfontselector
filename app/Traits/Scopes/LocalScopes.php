<?php

namespace App\Traits\Scopes;

/* Eloquent ORM Model and Builder */
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

trait LocalScopes
{
    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus(Builder $query)
    {
        return $query->where('status', 'PUBLISHED')
                     ->orWhere('status', 'SCHEDULED')->where('status_date', '<=', now());
    }

    /**
     * Scope a query to only include certain status'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdering(Builder $query, $col, $dir)
    {
        try {
            $column    = ($col != null || $col == '') ? $col : $this->orderField;
            $direction = ($dir != null || $dir == '') ? $dir : $this->orderDirection;
        } catch (\Exception $error) {
            $logthis = $error->getMessage();
            return;
        }

        if ($this->orderable) {
            return $query->orderBy($column, $direction);
        }

        return;
    }
}
