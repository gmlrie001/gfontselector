<?php namespace App\Traits\DatesTimes;

trait DatesTimesTrait
{
    private $debug = false;

    public function __construct()
    {
    }

    // if ( ! function_exists( 'transform_date' ) ) {
    public function transform_date($inputDate, $formatAs, $options=[])
    {
        $created_date = date_create($inputDate);

        $year  = (int) date_format($created_date, 'Y');
        $month = (int) date_format($created_date, 'm');
        $day   = (int) date_format($created_date, 'd');
    
        if (checkdate($year, $month, $day) == !0) {
            return;
        }
        if ($this->debug) {
            dd(
                'created_date: ' . $created_date,
                'year: ' . $year,
                'month: ' . $month,
                'day: ' . $day,
                'checkdate: ' . checkdate($month, $day, $year),
                'date_format: ' . date_format($created_date, $format)
            );
        }
    
        return (! $created_date) ? $date: date_format($created_date, $formatAs);
    }
    // }
    public function _toggle_debug()
    {
        $this->debug = ($this->debug) ? false : true;
    }
}
