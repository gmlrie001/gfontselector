<?php namespace App\Helpers\GoogleReCaptcha;

/* Base Controller Include */
use App\Http\Controllers\Services\PageController;

/* Facade Includes */
//use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Log;

use App\Helpers\Curl\Curl;

class GoogleRecaptchaHelper
{
    private $secret_key   = '';
    protected $verify_url = 'https://www.google.com/recaptcha/api/siteverify';

    public function googleRecaptchaServer($request_all, $options=[], $secret=null)
    {
        // Google ReCaptcha Comms
        if (! is_null($secret) || $secret != '') {
            $this->secret_key = $secret;
        }

        // Extract the client ip and g-recaptcha-response if it exists
        $_ip = request()->getClientIp();
        $_gr = $request_all['g-recaptcha-response'];

        // Setup the url, number of POSTS and the POSTS DATA array
        $url = $this->verify_url;
        $post_data = [
            'secret'   => $this->secret_key,
            'remoteip' => $_ip,
            'response' => $_gr,
        ];

        // Default CURL options
        $defaults = array(
            CURLOPT_POST           => 1,
            CURLOPT_HEADER         => 0,
            CURLOPT_URL            => $url,
            CURLOPT_FRESH_CONNECT  => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE   => 1,
            CURLOPT_TIMEOUT        => 4,
            CURLOPT_POSTFIELDS     => http_build_query($post_data),
        );
        // Additional options
        $options  = array_merge($options, [
            CURLOPT_USERAGENT      => 'MonzamediaVault GoogleRecaptcha/1.2 (+' . $_SERVER['HTTP_REFERER'] . ') Laravel/v5.8',
        ]);

        // Initialize and set the options
        $ch = curl_init();
        // $ch = new Curl( $url, 'POST', $post_data );//
        curl_setopt_array($ch, ($defaults + $options));
        $result = curl_exec($ch);
        
        // POST Request
        if (! $result) {
            Log::error('ERROR: Google ReCaptcha CURL Error: ' . curl_error($ch) . "\r\n");
            trigger_error(curl_error($ch));
        }

        $ch_resp = [
            'response_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
            'content_type'  => curl_getinfo($ch, CURLINFO_CONTENT_TYPE),
        ];

        Log::info('INFO: Google ReCaptcha Success. Client IP: ' . $_ip . ' and the FROM Email set as: ' . $request_all['email'] . "\r\n");
        curl_close($ch);

        return $this->_handleGRResponse($result, $ch_resp)['success'];
    }

    // static public function client_verify($url, $data) {

    // }

    private function _handleGRResponse($result, $response)
    {
        if (($response['response_code'] !== 200 && ! $response['content_type'])) {
            return;
        }

        try {
            if (preg_match('/json/iU', $response['content_type'], $m)) {
                $handler = json_decode($result);
                $success = $handler->success;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            $handler = [];
            $success = false;
        }

        return [ 'success' => $success, 'handler' => $handler ];
    }
}
