<?php

namespace App\Helpers\DB_Backup;

class DatabaseBackup_Helper
{
    public $db_handle;
    public $fiohandle;

    private $host;
    private $user;
    private $pass;
    private $name;
    private $dump;

    private $tables;
    private $return;

    public function __construct($host, $user, $pass, $name, $dump='*')
    {
        if (!is_null($host) && !is_null($user) && !is_null($pass) && !is_null($name)) {
            $this->host = $host;
            $this->user = $user;
            $this->pass = $pass;
            $this->name = $name;
            $this->dump = $dump;
            $this->db_handle = $this->set_db_connection([$host,$user,$pass,$name]);
        } else {
            return;
        }
    }

    public function set_db_connection(...$opts)
    {
        $conn = new mysqli($opts[0], $opts[1], $opts[2], 'utf-8');
        $conn->select_db($opts[3]);

        return $conn;
    }

    /* backup the db OR just a table */
    public function backup_db_tables($tables = '*')
    {
        if (! $this->db_handle) {
            return;
        }
      
        //get all of the tables
        if ($tables == '*') {
            $tables = [];
            $result = $this->db_handle->query('SHOW TABLES');
            while ($row = $result->fetch_row()) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }
    
        //cycle through
        foreach ($tables as $table) {
            $result = $this->db_handle->query('SELECT * FROM '.$table);
            $num_fields = $result->num_fields();
        
            $return.= 'DROP TABLE '.$table.';';
            $row2 = $this->db_handle->fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
            $return.= "\n\n".$row2[1].";\n\n";
        
            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = $result->fetch_row()) {
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for ($j=0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return.= '"'.$row[$j].'"' ;
                        } else {
                            $return.= '""';
                        }
                        if ($j < ($num_fields-1)) {
                            $return.= ',';
                        }
                    }

                    $return.= ");\n";
                }
            }

            $return.="\n\n\n";
        }

        $this->db_output($tables, $return);
        return;
    }

    public function db_output($tbl=null, $rtn=null)
    {
        $tables = (!is_null($tbl)) ? $tbl : $this->tables;
        $return = (!is_null($rtn)) ? $rtn : $this->return;

        //save file
        $this->fiohandle = fopen('db-backup-'.time().'-'.(md5(implode(',', $tables))).'.sql', 'w+');

        fwrite($this->fiohandle, $return);

        fclose($this->fiohandle);
    }
}


$db_bu = new DatabaseBackup_Helper('localhost', 'root', '', 'matricdancecars_coza');
$db_bu->backup_db_tables('*');
