<?php namespace App\Helpers;

use App\Http\Controllers\Services\PageController;

use App\Models\BlogPost;
use App\Models\CarsPost;
use App\Models\Event;

use App\Traits\Strings\StringsTrait;

class MiscHelpers
{
    use StringsTrait;
    public $base64_base = ['data:image/', ';base64,'];
    public $image_array = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

    public function convert_to_base64($img_path=null)
    {
        if (is_null($img_path) || ! $img_path) {
            return;
        }

        if (\file_exists($img_path)) {
            $image_type = pathinfo($img_path)['extension'];
            if (in_array($image_type, $this->image_array)) {
                $image_b = $this->base64_base[0] . $image_type . $this->base64_base[1];
                $image_f = file_get_contents($img_path);
                $image64 = $image_b . base64_encode($image_f);

                return (string) $image64;
            }
 
            return !0;
        } else {
            return;
        }
    }

    private function _getSettings($modl)
    {
        $table_name = $this->_plural($this->_snake($modl));
        $model_name = $this->_studly($this->_singular($table_name));

        if (Schema::hasTable($table_name)) {
            $model_path = "App\Models\\" . $modl;
            $model = ( new $model_path );
            $eager_load = $model::where('status', 'PUBLISHED')->orWhere('status', 'SCHEDULED')->orderBy('order', 'asc');
            return $eager_load->first();
        }
        return;
    }

    private function _getRelationSettings($reln)
    {
        if ($this->settings->has($reln)) {
            return $this->settings->$reln;
        }
        return;
    }

    private function _convert_mdl_to_tbl_name($mdl)
    {
    }

    private function _snake($s)
    {
        return snake_case($s);
    }
    private function _studly($s)
    {
        return studly_case($s);
    }
    private function _plural($s)
    {
        return str_plural($s);
    }
    private function _singular($s)
    {
        return str_singular($s);
    }

    private function _get_current_classname()
    {
        return get_called_class();
    }

    private function _relpath($i, $capitalize=false)
    {
        $b = explode('\\', base_path());
        $i = explode('\\', $i);

        $d = $this->diff_array($b, $i);
        if ($capitalize) {
            $d = array_map('ucfirst', $d);
        }
        $e = implode('\\', $d);

        return $e;
    }

    private function diff_array($a, $b=[])
    {
        return (count($a) > count($b)) ? array_diff($a, $b) : array_diff($b, $a);
    }

    private function _get_basename($c)
    {
        return str_replace('Controller', '', last(explode('\\', $c)));
    }

    public function createAddressLink($addresses)
    {
        foreach ($addresses as $key=>$address) {
            if ($address->address && ($address->map_image || $address->map_mobile_image)) /* && $address->id === $default_contact ) */ {
        $addresses[$key]->google_map_link  = $this->_setupGMapLink($addresses[$key]->address);
        $check = (
            ($address->latitude != '' ||  !is_null($address->latitude)) &&
          ($address->latitude != '' || !is_null($address->longitude))
        );

        if ($check) {
            $addresses[$key]->google_map_link .= '/@' . $address->latitude . ',' . $address->longitude;
        }

        $this->data['map_link'] = $addresses[$key]->google_map_link;
        unset($check);
      }
        }

        return $addresses;
    }

    private function _setupGMapLink($cnt)
    {
        $init_link = 'https://www.google.co.za/maps';

        if (null !== $cnt) {
            $prep_addr = ( string ) strip_tags($cnt);
            $prep_addr = str_replace('&nbsp;', '', $prep_addr);
            $prep_addr = str_replace('<br />', '', $prep_addr);
            $prep_addr = str_replace(',', '', $prep_addr);
            $prep_addr = explode("\r\n", $prep_addr);
            $prep_addr = array_values(array_filter(
                $prep_addr,
                function ($a) {
                    return (null !== $a || ! empty($a)) ? $a : null;
                }
            ));
            $prep_addr = array_map(
                function ($a) {
                    return str_replace(' ', '+', $a);
                },
                $prep_addr
            );

            $prep_addr = implode(",+", $prep_addr);

            return $init_link . '?q=' . $prep_addr;
        }

        return;
    }

    /**
     * Function for setting a Session key, value entry - for testing popup alerts.
     *
     * @param $key Session key
     * @param $value Session key's value
     *
     * @return
     *
     */
    public function test_session_message($key, $value = 'This is test message!')
    {
        if (! isset($key) || null == $key) {
            return;
        }
 
        if (isset($key) && ($key !== null || $key == '')) {
            if (! Session::has($key)) {
                Session::put($key, $value);
            }
        }
 
        return;
    }

    /**
       * Short description
       *
       * @param
       * @return
       */
    public function blog_listing($settings)
    {
        $this->blog_settings = $settings;

        $lazyload = BlogPost::status();
        $lazyload = $lazyload->orderBy('published_date', 'desc');
        $news     = $lazyload->paginate($this->blog_settings->items_pp);

        return $this->listingTransforms($news, $this->blog_settings);
    }

    /**
       * Short description
       *
       * @param
       * @return
       */
    public function generic_listing($mdl, $settings=null)
    {
        $this->blog_settings = $settings;
        $pp = 12;
        $model_path = "App\Models\\" . $mdl;
        $model = ( new $model_path );

        $lazyload = $model::status();
        $lazyload = $lazyload->orderBy('published_date', 'desc');
        $news     = $lazyload->paginate($pp);
        // dd($model, $news, get_defined_vars());
        
        return $this->listingTransforms($news, $this->blog_settings);
    }



    /**
       * Short description
       *
       * @param
       * @return
       */
    public function cars_listing($settings)
    {
        $this->cars_settings = $settings;

        $lazyload = CarsPost::status();
        $lazyload = $lazyload->orderBy('published_date', 'desc');
        $cars     = $lazyload->paginate($this->cars_settings->items_pp);

        return $this->listingTransforms($cars, $this->cars_settings);
    }

    /**
     * Short description
     *
     * @param
     * @return
     */
    public function event_view($event_title, $settings)
    {
        $this->article = Event::status()->where('url_title', $event_title)->first();
        $this->prev = $this->previous(new Event(), $event_title, $this->article->event_date, 'event_date');
        $this->next = $this->next(new Event(), $event_title, $this->article->event_date, 'event_date');

        return collect([
          'article'  => $this->article,
          'previous' => $this->prev,
          'next'     => $this->next
      ]);
    }

    /**
     * Short description
     *
     * @param
     * @return
     */
    public function generic_view($mdl, $event_title, $settings=null)
    {
        $model_path = "App\Models\\" . $mdl;
        $model = ( new $model_path );
        
        $this->article = $model::status()->where('url_title', $event_title)->first();
        
        $this->prev = $this->previous($model, $event_title, $this->article->event_date, 'event_date');
        $this->next = $this->next($model, $event_title, $this->article->event_date, 'event_date');

        return collect([
          'article'  => $this->article,
          'previous' => $this->prev,
          'next'     => $this->next
      ]);
    }


    /**
     * Short description
     *
     * @param
     * @return
     */
    public function blog_view($blog_title, $settings)
    {
        $this->article = BlogPost::status()->where('url_title', $blog_title)->first();
        $this->prev = $this->previous(new BlogPost(), $blog_title, $this->article->published_date, 'published_date');
        $this->next = $this->next(new BlogPost(), $blog_title, $this->article->published_date, 'published_date');

        return collect([
          'article'  => $this->article,
          'previous' => $this->prev,
          'next'     => $this->next
      ]);
    }

    /**
     * Short description
     *
     * @param
     * @return
     */
    public function cars_view($cars_title, $settings)
    {
        $this->article = CarsPost::status()->where('url_title', $cars_title)->first();

        return collect([
          'article'  => $this->article,
      ]);
    }

    // public function blog_filter(...$opts)
    // {
    //     $this->blog_settings = $opts[2];
    //     $news = BlogPost::status()
    //             ->whereYear('published_date', '=', $opts[0])
    //             ->whereMonth('published_date', '=', $opts[1])
    //             ->orderBy('published_date', 'desc')
    //                 ->paginate($this->blog_settings->items_pp);

    //     return $this->listingTransforms($news, $this->blog_settings);
    // }

    /**
     * Short description
     *
     * @param
     * @return
     */
    private function previous($model, $title, $pubdate, $date_string='published_date')
    {
        return $model::status()->where('url_title', '!=', $title)
                    ->where($date_string, '<', $pubdate)->orderBy($date_string, 'desc')->first();
    }

    /**
     * Short description
     *
     * @param
     * @return
     */
    private function next($model, $title, $pubdate, $date_string='published_date')
    {
        return $model::status()->where('url_title', '!=', $title)
                    ->where($date_string, '>', $pubdate)->orderBy($date_string, 'desc')->first();
    }

    /**
     * Short description
     *
     * @param
     * @return
     */
    public function listingTransforms($posts, $settings=null)
    {
        // $title_words_limit = (null != $settings->title_strlim && $settings->title_strlim > 0);
        // $title_limit = (! $title_words_limit) ? $settings->title_strlim: 10;
        $title_limit = $title_words_limit = 10;
        $description_limit = $word_limit = 21;

        // $word_limit = (null != $settings->description_strlim && $settings->description_strlim > 0);
        // $description_limit = (! $word_limit) ? $settings->description_strlim: 21;

        foreach ($posts as $key=>$post_item) {
            if ($title_words_limit) {
                $posts[$key]->title = $this->truncate_by_words(
                    $this->clean_string($post_item->title),
                    $title_limit
                );
            }
            if ($word_limit) {
                $posts[$key]->description = $this->truncate_by_words(
                    $this->clean_string($post_item->description),
                    $description_limit
                );
            }
        }

        return $posts;
    }
}
