<?php

namespace App\Helpers\GoogleFonts;

class GoogleFontsHelper
{
    public $font_json_path;// = 'assets/json/google_fonts.json';
    public $is_public;
    public $modified;
    public $needs_api_call = false;
    public $api_base_url = 'https://www.googleapis.com/webfonts/v1/webfonts';
    public $api_key;

    public function __construct(
        $path='assets/json/test.json',
        $apiKey='',
        $public=true
    ) {
        $this->font_json_path = ( isset( $path ) ) ? $path : null;
        $this->set_api_key($apiKey);

        $this->is_public = $public;
        if ($this->is_public) {
            $this->font_json_path = public_path( ltrim($this->font_json_path, '/' ) );
        } else {
            $this->font_json_path = '/' . ltrim( $this->font_json_path, '/' );
        }

        $this->needs_api_call = $this->font_file_exists();

        // dd($this, $this->font_json_path, $this->font_json_path, file_exists( $this->font_json_path ) );

        return $this;
    }

/* Magic Methods */
    public function __get($property)
    {
         if (property_exists($this, $property)) {  
            return $this->$property;  
         }  
    }

    public function __set($property, $value)  
    {  
        if (property_exists($this, $property)) {  
             $this->$property = $value;  
        }  
    }

    public function __isset($property)  
    {  
        return isset($this->$property);  
    }

    public function __unset($property)  
    {  
        unset($this->$property);  
    }  

    public function __toString()  
    {  
        return $this->title . '<br/>' . $this->description;  
    }

    public function __sleep()  
    {  
        return;  
    }  
 
    public function __call($method, $parameters)  
    {  
       if (method_exists($this, $method))  
       {  
           return call_user_func_array(array($this, $method), $parameters);  
       }  
    }  

    public static function __callStatic($method, $parameters)
    {
        if(method_exists(__CLASS__, $method))
        {
              return self::$method(...$parameters);
        }
    }


/* Magic Methods */
    
    public function get_localPath()
    {
        return $this->font_json_path;
    }

    public function set_localPath($pth)
    {
        $this->font_json_path = public_path( $pth );

        return $this;
    }

    public function font_file_exists()
    {
        return file_exists($this->font_json_path);
    }

    public function get_contents()
    {
        return ($this->font_file_exists()) ? file_get_contents($this->font_json_path) : null;
    }


    public function fetch_contents($url)
    {
        if (! $this->validate_url($url) ) { return; }

        try {
            return file_get_contents($url);
        } catch( Exception $e ) {
            return;
        }
    }

    public function validate_url($url) : bool
    {
        return ( filter_var( $url, FILTER_VALIDATE_URL ) );
    }

    public function last_modified()
    {
        $stats = stat($this->font_json_path);
        $this->modified = ($stats['mtime']) ? $stats['mtime'] : null;

        return $this;
    }

    public function get_api_key()
    {
        return $this->api_key;
    }

    public function set_api_key($key)
    {
        $this->api_key = $key;

        return $this;
    }

    public function fetch_from_api()
    {
        $font_file_path = public_path( '/assets/json/test.json' );
        $build_api_url  = [
            'base'  => $this->api_base_url,
            'query' => [ 'key' => $this->api_key, 'sort' => 'alpha' ],
        ];

        $api_url  = $build_api_url['base'] . '?' . http_build_query($build_api_url['query']);
        $fonts    = $this->fetch_contents( $api_url );
        // ( $this->font_file_exists() ) ? $this->get_contents($api_url) : null;
        $fontData = $fonts;

        try {
            if (! file_exists($font_file_path)) {
                $fhandle = fopen($font_file_path, 'w');
                fwrite($fhandle, $fontData);
                fclose($fhandle);
            }
        } catch (\Exception $error) {
            $log = $error;
            return;
        }

        unset($api_url, $fonts, $fontData);

        echo("\r\nRetrieved Updated Google Fonts from the API.\r\n");

        return $this;
    }

    private function writeJSON($data)
    {
        if ($data == null) {
            return;
        }
    }
}
