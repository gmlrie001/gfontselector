<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Repositories\Interfaces\RepositoryInterface;

class Repository implements RepositoryInterface
{
    /**
     * Model property on class instances
     */
    protected $model;

    /**
     * Constructor to bind model to repo
     */
    public function __construct(Model $model)
    {
        $this->model = new $model;
    }

    /**
     * Get a collection of ALL the posts/articles/items within a section
     */
    public function all_withUnPublished()
    {
        return $this->model->all();
    }

    /**
     * Get a collection of ALL the posts/articles/items within a section
     */
    public function all()
    {
        return ($this->model->hasStatus) ? $this->model->status()->get() : $this->model->get();
    }

    /**
     * Get a collection of ALL the posts/articles/items within a section
     */
    public function firstOne()
    {
        return $this->all()->first();
    }

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function showByUsing($column, $value)
    {
        // return $this->model->findUrlTitle($url_title);
        return $this->model->where($column, $value)->first();
    }

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function show($id)
    {
        // return $this->model->findUrlTitle($url_title);
        return $this->model->findOrFail($id);
    }

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function showByUrlTitle($url_title)
    {
        return $this->model->whereUrlTitle($url_title)->firstOrFail();
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with()
    {
        return $this->model->with(array_values($this->model->relationships));
    }
}
