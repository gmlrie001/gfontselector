<?php namespace App\Repositories\Interfaces;

interface RepositoryInterface
{
    /**
     * Get a collection of ALL the posts/articles/items within a section
     */
    public function all_withUnPublished();

    /**
     * Get a collection of ALL the posts/articles/items within a section
     */
    public function all();

    /**
     * Get the first posts/articles/items within a section
     */
    public function firstOne();

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function showByUsing($column, $value);

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function show($id);

    /**
     * Get the specific posts/articles/items using the url_title.
     */
    public function showByUrlTitle($url_title);
}
